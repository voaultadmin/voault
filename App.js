import React, { Component } from 'react';
import { Image, StyleSheet, YellowBox } from 'react-native';
import { Provider } from 'react-redux';
import { createStackNavigator } from 'react-navigation';
import { fromLeft, fromRight, fromTop, fromBottom, zoomIn, zoomOut, flipY } from 'react-navigation-transitions'
import { Button } from 'react-native-elements';


import icon from './images/topright.png';
import icon1 from './images/topleft.png';
import store from './stateStore/store';
import Login from './components/Login';
import RecoverCreds from './components/RecoverCreds';
import Signup from './components/Signup';
import Signup2 from './components/Signup2';
import SmsCode from './components/SmsCode';
import VIntent from './components/VIntent';
import Instructions from './components/Account_meta/Instructions.js'
import Menu from './components/Menu';
import Details from './components/Details';
import Keys from './components/Details_meta/Keys';
import Gmaps from './components/Details_meta/Gmaps';
import Payment from './components/Details_meta/Payment';
import Guide from './components/Details_meta/Guide';
import Info from './components/Details_meta/Info';
import ViTrade from './components/Details_meta/ViTrade';
import ViStorage from './components/Details_meta/ViStorage';
import LiveCams from './components/Details_meta/LiveCams';
import Controls from './components/Controls';
import Account from './components/Account';
import Profile from './components/Account_meta/Profile';
import Edit from './components/Account_meta/Edit';
import Help from './components/Account_meta/Help';
import Legal from './components/Account_meta/Legal';
import { myColors, socket } from './constants/constants';


import ignoreWarnings from 'react-native-ignore-warnings';
ignoreWarnings('Setting a timer');
ignoreWarnings('Unrecognized WebSocket');


const handleCustomTransition = ({ scenes }) => {
const prevScene = scenes[scenes.length - 2];
const nextScene = scenes[scenes.length - 1];

// Custom transitions go there
if (prevScene
  && prevScene.route.routeName === 'Menu'
  && nextScene.route.routeName === 'Account') {
  return fromLeft();
}
if (prevScene
  && prevScene.route.routeName === 'Menu'
  && nextScene.route.routeName === 'Details') {
  return fromRight(600);
}
else if (prevScene
  && prevScene.route.routeName === 'Login'
  && nextScene.route.routeName === 'Menu') {
  return fromTop(600);
}
else if (prevScene
  && prevScene.route.routeName === 'Details'
  && nextScene.route.routeName === 'Keys') {
  return fromBottom(400);
}
else if (prevScene
  && prevScene.route.routeName === 'Details'
  && nextScene.route.routeName === 'Guide') {
  return fromBottom(400);
}
else if (prevScene
  && prevScene.route.routeName === 'Details'
  && nextScene.route.routeName === 'Gmaps') {
  return fromBottom(400);
}
else if (prevScene
  && prevScene.route.routeName === 'Details'
  && nextScene.route.routeName === 'Info') {
  return fromBottom(400);
}
else if (prevScene
  && prevScene.route.routeName === 'Details'
  && nextScene.route.routeName === 'LiveCams') {
  return fromBottom(400);
}
else if (prevScene
  && prevScene.route.routeName === 'Details'
  && nextScene.route.routeName === 'ViTrade') {
  return fromBottom(300);
}
else if (prevScene
  && prevScene.route.routeName === 'Details'
  && nextScene.route.routeName === 'ViStorage') {
  return fromBottom(300);
}
else if (prevScene
  && prevScene.route.routeName === 'Account'
  && nextScene.route.routeName === 'Legal') {
  return fromBottom(600);
}
else if (prevScene
  && prevScene.route.routeName === 'Account'
  && nextScene.route.routeName === 'Login') {
  return fromTop(1000);
}
else if (prevScene
  && prevScene.route.routeName === 'Profile'
  && nextScene.route.routeName === 'Login') {
  return fromTop(600);
}

return fromRight(500);
}

export default class App extends Component {
    render() {
        const MainNavigator = createStackNavigator({

          ViTrade: { screen: ViTrade,
            navigationOptions: () => ({
              //header: null,
              title: '',
              headerTintColor: myColors.color1,
              headerStyle: {
                backgroundColor: myColors.color2,
                borderWidth: 1,
                borderBottomColor: myColors.vlt },
              headerTitleStyle: { color: myColors.color1 },
              headerRight: <Image source={icon} style={styles.imgStyle} />,
            }),
           },

          ViStorage: { screen: ViStorage,
            navigationOptions: () => ({
              //header: null,
              title: '',
              headerTintColor: myColors.color1,
              headerStyle: {
                backgroundColor: myColors.color2,
                borderWidth: 1,
                borderBottomColor: myColors.vlt },
              headerTitleStyle: { color: myColors.color1 },
              headerRight: <Image source={icon} style={styles.imgStyle} />,
            }),
           },

          Login: { screen: Login,
            navigationOptions: () => ({
              title: '',
              headerTintColor: myColors.color1,
              headerStyle: {
                backgroundColor: myColors.color2,
                borderWidth: 1,
                borderBottomColor: myColors.vlt },
              headerTitleStyle: { color: myColors.color1 },
              headerRight: <Image source={icon} style={styles.imgStyle} />
            }),
           },

           RecoverCreds: { screen: RecoverCreds,
             navigationOptions: () => ({
               title: '',
               headerTintColor: myColors.color1,
               headerStyle: {
                 backgroundColor: myColors.color2,
                 borderWidth: 1,
                 borderBottomColor: myColors.vlt },
               headerTitleStyle: { color: myColors.color1 },
               headerRight: <Image source={icon} style={styles.imgStyle} />
             }),
            },

          Account: { screen: Account,
            navigationOptions: () => ({
              //header: null,
              headerTintColor: myColors.color1,
              headerStyle: {
                backgroundColor: myColors.color2,
                borderWidth: 1,
                borderBottomColor: myColors.vlt },
              headerTitleStyle: { color: myColors.color1 },
              headerLeft: <Image source={icon1} style={styles.imgStyle} />,
            }),
           },

           Info: { screen: Info,
             navigationOptions: () => ({
               //header: null,
               title: '',
               headerTintColor: myColors.color1,
               headerStyle: {
                 backgroundColor: myColors.color2,
                 borderWidth: 1,
                 borderBottomColor: myColors.vlt },
               headerTitleStyle: { color: myColors.color1 },
               headerRight: <Image source={icon} style={styles.imgStyle} />,
             }),
            },

            VIntent: { screen: VIntent,
              navigationOptions: () => ({
                //header: null,
                title: '',
                headerTintColor: myColors.color1,
                headerStyle: {
                  backgroundColor: myColors.color2,
                  borderWidth: 1,
                  borderBottomColor: myColors.vlt },
                headerTitleStyle: { color: myColors.color1 },
                headerRight: <Image source={icon} style={styles.imgStyle} />,
              }),
             },

           Profile: { screen: Profile,
             navigationOptions: () => ({
               //header: null,
               title: '',
               headerTintColor: myColors.color1,
               headerStyle: {
                 backgroundColor: myColors.color2,
                 borderWidth: 1,
                 borderBottomColor: myColors.vlt },
               headerTitleStyle: { color: myColors.color1 },
               headerRight: <Image source={icon} style={styles.imgStyle} />,
             }),
            },


            Legal: { screen: Legal,
              navigationOptions: () => ({
                title: '',
                headerTintColor: myColors.color1,
                headerStyle: {
                  backgroundColor: myColors.color2,
                  borderWidth: 1,
                  borderBottomColor: myColors.color1 },
                headerTitleStyle: { color: myColors.color1 },
                //headerRight: <Image source={icon} style={styles.imgStyle} />
              }),
             },

           Instructions: { screen: Instructions,
             navigationOptions: () => ({
               title: '',
               headerTintColor: myColors.color1,
               headerStyle: {
                 backgroundColor: myColors.color2,
                 borderWidth: 1,
                 borderBottomColor: myColors.vlt },
               headerTitleStyle: { color: myColors.color1 },
               //headerRight: <Image source={icon} style={styles.imgStyle} />
             }),
            },

          Menu: { screen: Menu,
            navigationOptions: () => ({
              //title: '',
              //headerTintColor: 'transparent',
              // headerStyle: {
              //   backgroundColor: 'black',
              //   borderWidth: 1,
              //   borderBottomColor: myColors.color1 },
              //headerTitleStyle: { color: 'black' },
              // headerLeft: (
              //   <Button
              //     //onPress={() => navigation.navigate('Account')}
              //     title=""
              //     color="#fff"
              //     icon={{name: 'account-circle', color: 'white', size: 30}}
              //     buttonStyle={{
              //       backgroundColor: 'transparent',
              //     }}
              //   />
              // ),
              //headerRight: <Image source={icon} style={styles.imgStyle} />
            }),
           },


          Signup: { screen: Signup,
            navigationOptions: () => ({
              title: '',
              headerTintColor: myColors.color1,
              headerStyle: {
                backgroundColor: myColors.color2,
                borderWidth: 1,
                borderBottomColor: myColors.vlt },
              headerTitleStyle: { color: myColors.color1 },
              headerRight: <Image source={icon} style={styles.imgStyle} />
            }),
           },

           SmsCode: { screen: SmsCode,
             navigationOptions: () => ({
               title: '',
               headerTintColor: myColors.color1,
               headerStyle: {
                 backgroundColor: myColors.color2,
                 borderWidth: 1,
                 borderBottomColor: myColors.vlt },
               headerTitleStyle: { color: myColors.color1 },
               headerRight: <Image source={icon} style={styles.imgStyle} />
             }),
            },

           Signup2: { screen: Signup2,
             navigationOptions: () => ({
               title: '',
               headerTintColor: myColors.color1,
               headerStyle: {
                 backgroundColor: myColors.color2,
                 borderWidth: 1,
                 borderBottomColor: myColors.vlt },
               headerTitleStyle: { color: myColors.color1 },
               headerRight: <Image source={icon} style={styles.imgStyle} />
             }),
            },

           LiveCams: { screen: LiveCams,
             navigationOptions: () => ({
               //header: null,
               title: '',
               headerTintColor: myColors.color1,
               headerStyle: {
                 backgroundColor: myColors.color2,
                 borderWidth: 1,
                 borderBottomColor: myColors.vlt },
               headerTitleStyle: { color: myColors.color1 },
               headerRight: <Image source={icon} style={styles.imgStyle} />,
             }),
            },


          Details: { screen: Details,
            navigationOptions: () => ({
              //header: null,
              title: '',
              headerTintColor: myColors.color1,
              headerStyle: { backgroundColor: myColors.color2, borderWidth: 1, borderBottomColor: myColors.vlt },
              headerTitleStyle: { color: myColors.color1},
              headerRight: <Image source={icon} style={styles.imgStyle} />,
            }),
           },


           Gmaps: { screen: Gmaps,
             navigationOptions: () => ({
               //header: null,
               title: '',
               headerTintColor: myColors.color1,
               headerStyle: {
                 backgroundColor: myColors.color2,
                 borderWidth: 1,
                 borderBottomColor: myColors.vlt },
               headerTitleStyle: { color: myColors.color1 },
               headerRight: <Image source={icon} style={styles.imgStyle} />,
             }),
            },

          Payment: { screen: Payment,
            navigationOptions: () => ({
              //header: null,
              title: '',
              headerTintColor: myColors.color1,
              headerStyle: {
                backgroundColor: myColors.color2,
                borderWidth: 1,
                borderBottomColor: myColors.vlt },
              headerTitleStyle: { color: myColors.color1 },
              headerRight: <Image source={icon} style={styles.imgStyle} />,
            }),
           },



          Guide: { screen: Guide,
            navigationOptions: () => ({
              //header: null,
              title: '',
              headerTintColor: myColors.color1,
              headerStyle: {
                backgroundColor: myColors.color2,
                borderWidth: 1,
                borderBottomColor: myColors.color2 },
              headerTitleStyle: { color: myColors.color1 },
              headerRight: <Image source={icon} style={styles.imgStyle} />,
            }),
           },

           Keys: { screen: Keys,
             navigationOptions: () => ({
               //header: null,
               title: '',
               headerTintColor: myColors.color1,
               headerStyle: {
                 backgroundColor: myColors.color2,
                 borderWidth: 1,
                 borderBottomColor: myColors.color2 },
               headerTitleStyle: { color: myColors.color1 },
               headerRight: <Image source={icon} style={styles.imgStyle} />,
             }),
            },



           Controls: { screen: Controls,
             navigationOptions: () => ({
               //header: null,
               title: '',
               headerTintColor: myColors.color1,
               headerStyle: {
                 backgroundColor: myColors.color2,
                 borderWidth: 1,
                 borderBottomColor: myColors.color2 },
               headerTitleStyle: { color: myColors.color1 },
               headerRight: <Image source={icon} style={styles.imgStyle} />,
             }),
            },

           Edit: { screen: Edit,
             navigationOptions: () => ({
               //header: null,
               title: '',
               headerTintColor: myColors.color1,
               headerStyle: {
                 backgroundColor: myColors.color2,
                 borderWidth: 1,
                 borderBottomColor: myColors.color2 },
               headerTitleStyle: { color: myColors.color1 },
               headerRight: <Image source={icon} style={styles.imgStyle} />,
             }),
            },

            Help: { screen: Help,
              navigationOptions: () => ({
                //header: null,
                title: '',
                headerTintColor: myColors.color1,
                headerStyle: {
                  backgroundColor: myColors.color2,
                  borderWidth: 1,
                  borderBottomColor: myColors.color2 },
                headerTitleStyle: { color: myColors.color1 },
                headerRight: <Image source={icon} style={styles.imgStyle} />,
              }),
             },

        },
        {
          initialRouteName: 'VIntent',
          transitionConfig: (nav) => handleCustomTransition(nav),
        },
      );

      return (

          <Provider store={store}>

              <MainNavigator />

          </Provider>

      );
    }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  imgStyle: {
    width: 200,
    height: 200,
    marginLeft: 10,
    marginRight: 10
  },
});
