import { ImagePicker, Permissions } from 'expo';
import React, { Component } from 'react';
import {
  Alert,
  AsyncStorage,
  ActivityIndicator,
  ImageBackground,
  Text,
  StyleSheet,
  View,
  TextInput,
  Keyboard,
  TouchableWithoutFeedback,
  TouchableHighlight,
  Dimensions,
  ScrollView,
  Linking
} from 'react-native';
import { connect } from 'react-redux';
import { StackActions, NavigationActions } from 'react-navigation';
import { SocialIcon, Button, Icon, Avatar, ListItem } from 'react-native-elements';
import StarRating from 'react-native-star-rating';

import { socket, myColors, bgImageNoLogo } from '../constants/constants';
import { updateUsrFullName, updateUsrUsername, updateUsrPassword, updateUsrEmail,
  updateSysStarCount, updateSysProfilePic,
  updateVltReserved, updateVltMasterKey, updateVltKey1, updateVltKey2, updateVltIndex, updateVltUnit,
  socketIO } from '../stateStore/actions';


const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;
const BG_IMAGE = require('../images/bg-1.png');


class Account extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerRight: (
        <Button
          onPress={()=> navigation.navigate('Menu')}
          title=""
          color="#fff"
          icon={{name: 'dashboard', color: myColors.vlt, size: 25}}
          buttonStyle={{
            backgroundColor: 'transparent',
          }}
        />
      ),

    };
  }

  _pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      allowsEditing: true,
      aspect: [4, 3],
    });

    if (!result.cancelled) {
      this.props.updateSysProfilePic(result.uri)
    }
  };

  onStarRatingPress(rating) {
    this.props.updateSysStarCount(rating)
  }

  render() {
    const list = [
      {
        title: 'Host Hub',
        titleStyle: {
          color: myColors.control,
          //fontWeight: 'bold',
          //fontStyle: 'italic',
          fontSize: 20
        },
        leftIcon: 'add-location',
        leftIconColor: myColors.control,
        rightIcon: 'chevron-right',
        rightIconColor: myColors.control,
        backgroundColor: myColors.vlt,
        onPress: 'Host Hub',
      },
      {
        title: 'Profile',
        subtitle: this.props.reduxState.usr_creds_username,
        subtitleStyle: {
          color: myColors.control1,
          fontWeight: 'bold',
          fontSize: 16
        },
        leftIcon: 'edit',
        rightIcon: 'chevron-right',
        backgroundColor: myColors.control,
        //marginTop: 10,
        onPress: 'Profile',
      },
      {
        title: 'Wallet',
        leftIcon: 'credit-card',
        rightIcon: 'chevron-right',
        backgroundColor: myColors.control,
        onPress: 'Test',
      },
      // {
      //   title: 'Reviews',
      //   leftIcon: 'short-text',
      //   rightIcon: 'chevron-right',
      //   backgroundColor: myColors.control,
      //   onPress: 'Test',
      // },
      // {
      //   title: 'Location',
      //   leftIcon: 'location-searching',
      //   rightIcon: 'chevron-right',
      //   backgroundColor: myColors.control,
      //   marginTop: 10,
      // },
      {
        title: 'Help/Feedback',
        leftIcon: 'help-outline',
        rightIcon: 'chevron-right',
        backgroundColor: myColors.control,
        onPress: 'Help',
      },
      // {
      //   title: 'Security',    //touchid, 2 step verification
      //   leftIcon: 'info',
      //   rightIcon: 'chevron-right',
      //   backgroundColor: myColors.control,
      // },
      // {
      //   title: 'Notification',
      //   leftIcon: 'info',
      //   rightIcon: 'chevron-right',
      //   backgroundColor: myColors.control,
      // },
      {
        title: 'Logout',
        leftIcon: 'reply',
        rightIcon: null,
        backgroundColor: myColors.control,
        //marginTop: 15,
        onPress: 'Login',
      },
    ]


    const {
      reduxState: {
        usr_creds_email,
        usr_creds_fullName,
        usr_creds_username,
        usr_creds_password,
        sys_pageLoaded,
        sys_starCount,
        sys_profilePic,
      }
    } = this.props;

    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
       <View style={styles.container}>
         <ScrollView style={styles.list}>
           {
            list.map((item, i) => (
              <ListItem
                containerStyle={{
                  marginTop: item.marginTop,
                  backgroundColor: item.backgroundColor,
                  borderColor: 'white',
                  width: SCREEN_WIDTH,
                }}
                key={i}
                title={item.title}
                titleStyle={item.titleStyle}
                subtitle={item.subtitle}
                subtitleStyle={item.subtitleStyle}
                leftIcon={{ name: item.leftIcon, color: item.leftIconColor }}
                rightIcon={{ name: item.rightIcon, color: item.rightIconColor }}
                onPress={()=>{
                  setTimeout(()=> {
                    if (item.onPress == 'Login') {
                      Alert.alert(
                        'Logout?',
                        '',
                        [
                          {text: 'Yes', onPress: () => {
                              AsyncStorage.removeItem('username', ()=>{
                                this.props.updateUsrFullName('')
                                this.props.updateUsrEmail('')
                                this.props.updateUsrUsername('')
                                this.props.updateUsrPassword('')
                                this.props.updateVltReserved(false)
                                this.props.updateVltMasterKey('')
                                this.props.updateVltKey1('')
                                this.props.updateVltKey2('')
                                this.props.updateVltIndex('')
                                this.props.updateVltUnit('')

                                this.props.navigation.navigate(item.onPress)
                              })
                              AsyncStorage.removeItem('token')
                            }
                          },
                          {text: 'cancel', style: 'cancel'},
                        ]
                      );
                    }
                    else if (item.onPress == 'Host Hub') {
                      Alert.alert(
                        'Great News!',
                        'Would you receive information about hosting a Hub at your location?',
                        [
                          {text: 'Yes', onPress: () => {
                            const payload = {
                              cmd: 'email',
                              contents: {
                                emailSubject: 'Help/Feedback',
                                emailTarget: 'support@voault.com',
                                emailText: 'Host Hub request! ---- **** ---- ' + this.props.reduxState.usr_creds_email,
                                emailType: 'help',
                              },
                            }
                            this.props.socketIO(payload)
                            }, style: 'cancel'
                          },
                          {text: 'cancel'},
                        ]
                      );
                    }
                    else {
                      this.props.navigation.navigate(item.onPress)
                    }
                  }, 250)
                }}
              />
            ))
          }
        <View style={{justifyContent: 'center', alignItems: 'center', flexDirection: 'row', marginTop: 30}}>
          <SocialIcon type='instagram' onPress={() => Linking.openURL('http://instagram.com/voault_')}/>
          {/* <SocialIcon type='facebook' onPress={() => Linking.openURL('http://facebook.com/voault')}/> */}
          {/* <SocialIcon type='twitter' />
          <SocialIcon type='youtube' /> */}
          <SocialIcon type='linkedin' onPress={() => Linking.openURL('http://linkedin.com/company/voault')}/>
        </View>
        <View style={{justifyContent: 'center', alignItems: 'center', flexDirection: 'row', marginTop: 40}}>
          <Text style={{ color: 'rgb(193, 189, 191)', fontSize: 14, fontWeight:'bold', marginTop: 5, textDecorationLine:'underline' }}
                 onPress={() => Linking.openURL('https://www.voault.com/user-license')}> User Agreement & Privacy Policy
          </Text>
        </View>
        <View style={{justifyContent: 'center', alignItems: 'center', flexDirection: 'row'}}>
          {/* <Text style={{ color: 'rgb(193, 189, 191)', fontSize: 12, fontWeight:'bold', marginTop: 5, marginBottom: 10 }}
                 onPress={() => Alert.alert('link')}> Site License Agreement
          </Text> */}
          <Text style={{ color: 'rgb(193, 189, 191)', fontSize: 12, fontWeight:'bold', marginTop: 10 }}> version: 2.5.2 beta
          </Text>
        </View>
        <Text></Text>
        </ScrollView>
       </View>
     </TouchableWithoutFeedback>
   );//return
   }//render
 }

 const styles = StyleSheet.create({
   container: {
     flex: 1,
     //justifyContent: 'center',
     alignItems: 'center',
     backgroundColor: 'white'
   },
   bgImage: {
     flex: 1,
     top: 0,
     left: 0,
     width: SCREEN_WIDTH,
     // height: SCREEN_HEIGHT,
     justifyContent: 'center',
     alignItems: 'center',
   },
   txt1: {
     color: 'white',
     fontWeight: 'bold',
     //fontStyle: 'italic',
     fontSize: 22,
     textAlign: 'center',
     marginTop: 15,
     marginBottom: 5,
   },
   txt2: {
     color: 'white',
     fontWeight: 'bold',
     fontStyle: 'italic',
     fontSize: 16,
     textAlign: 'center',
     marginBottom: 5,
   },
   list: {
     flex: 0.2,
   },
   profile: {
     flex: 0.8
   }
 });

const mapStateToProps = ({ reduxState }) => ({ reduxState });

export default connect(mapStateToProps, {
  updateUsrFullName, updateUsrUsername, updateUsrPassword, updateUsrEmail,
  updateSysStarCount, updateSysProfilePic,
  updateVltReserved, updateVltMasterKey, updateVltKey1, updateVltKey2, updateVltIndex, updateVltUnit,
  socketIO })(Account);
