import React, { Component } from 'react';
import {
  ActivityIndicator,
  ImageBackground,
  Text,
  StyleSheet,
  View,
  TextInput,
  Keyboard,
  TouchableWithoutFeedback,
  TouchableHighlight,
  Platform,
  Image,
  WebView,
  Dimensions,
  ScrollView,
  Alert,
} from 'react-native';
import { connect } from 'react-redux';

import { Button, Icon, Avatar, ListItem, Input } from 'react-native-elements';
import { Card, CardTitle, CardContent, CardAction, CardButton, CardImage } from 'react-native-material-cards';

import {
  updateSysEditContent, updateSysWaitSocket, socketIO
} from '../../stateStore/actions';

import { myColors, usrPassCheck, emailCheck, socket, bgImageNoLogo } from '../../constants/constants';

const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;


class NewUsername extends Component {
  constructor(props){
    super(props)
    socket.on('validEditUsername', (result, socketID, newUsername) => {
      if (socketID == socket.id) {
        switch (result) {
          case true:
            this.props.updateUsrUsername(newUsername)
            Alert.alert('Username updated')
            setTimeout(() => {
              this.props.navigation.navigate('Profile')
            }, 300);

            break;
          case false:
            Alert.alert('Passwords do not match. Please try again.')
            break;
          default:
        }
      }
    })//validEditUsername

  }//constructor


  edit(event) {
    switch (this.props.reduxState.sys_editTarget) {
      case 'Username':
        if (this.props.reduxState.sys_editContent.username == '') {
          this.content = {
            currentPassword: this.props.reduxState.sys_editContent.currentPassword,
            username: '#',
            newPassword: this.props.reduxState.sys_editContent.newPassword,
            confirmPassword: this.props.reduxState.sys_editContent.confirmPassword,
            email: this.props.reduxState.sys_editContent.email,
          }
          this.props.updateSysEditContent(this.content)

          this.content1 = {
            currentPassword: this.props.reduxState.sys_editContent.currentPassword,
            username: '#' + event.nativeEvent.text,
            newPassword: this.props.reduxState.sys_editContent.newPassword,
            confirmPassword: this.props.reduxState.sys_editContent.confirmPassword,
            email: this.props.reduxState.sys_editContent.email,
          }
          setTimeout(() => {
            this.props.updateSysEditContent(this.content1)
          }, 50)

        }else {
          this.content = {
            currentPassword: this.props.reduxState.sys_editContent.currentPassword,
            username: event.nativeEvent.text,
            newPassword: this.props.reduxState.sys_editContent.newPassword,
            confirmPassword: this.props.reduxState.sys_editContent.confirmPassword,
            email: this.props.reduxState.sys_editContent.email,
          }
          this.props.updateSysEditContent(this.content)
        }
        break;

      case 'Password':
        this.content = {
          currentPassword: this.props.reduxState.sys_editContent.currentPassword,
          username: this.props.reduxState.sys_editContent.username,
          newPassword: event.nativeEvent.text,
          confirmPassword: this.props.reduxState.sys_editContent.confirmPassword,
          email: this.props.reduxState.sys_editContent.email,
        }
        this.props.updateSysEditContent(this.content)
        break;

      case 'Email':
        this.content = {
          currentPassword: this.props.reduxState.sys_editContent.currentPassword,
          username: this.props.reduxState.sys_editContent.username,
          newPassword: this.props.reduxState.sys_editContent.newPassword,
          confirmPassword: this.props.reduxState.sys_editContent.confirmPassword,
          email: event.nativeEvent.text,
        }
        this.props.updateSysEditContent(this.content)
        break;

      default:
    }
  }//edit

  editCurrentPassword(event){
    this.content = {
      currentPassword: event.nativeEvent.text,
      username: this.props.reduxState.sys_editContent.username,
      newPassword: this.props.reduxState.sys_editContent.newPassword,
      confirmPassword: this.props.reduxState.sys_editContent.confirmPassword,
      email: this.props.reduxState.sys_editContent.email,
    }
    this.props.updateSysEditContent(this.content)
  }//editCurrentPassword

  editConfirmPassword(event){
    this.content = {
      currentPassword: this.props.reduxState.sys_editContent.currentPassword,
      username: this.props.reduxState.sys_editContent.username,
      newPassword: this.props.reduxState.sys_editContent.newPassword,
      confirmPassword: event.nativeEvent.text,
      email: this.props.reduxState.sys_editContent.email,
    }
    this.props.updateSysEditContent(this.content)
  }//editConfirmPassword

  updateTarget(){
    switch (this.props.reduxState.sys_editTarget) {
      case 'Username':
      if (
        usrPassCheck
        .is().min(4)
        .is().max(20)
        .has().lowercase()
        .has().not().spaces()
        //.has().not().symbols()
        .is().not().oneOf(['username', 'Username123'])
        .validate(this.props.reduxState.sys_editContent.username.substring(1))
      ) {
        //valid username
        const payload = {
          cmd: 'editUsername',
          contents: {
            currentUsername: this.props.reduxState.usr_creds_username,
            newUsername: this.props.reduxState.sys_editContent.username,
            currentPassword: this.props.reduxState.sys_editContent.currentPassword,
            newPassword: this.props.reduxState.sys_editContent.newPassword,
            newEmail: this.props.reduxState.sys_editContent.email,
          },
        }
        this.props.socketIO(payload)
      } else {
        Alert.alert('Please enter valid Username')
      }
      break;
      case 'Password':
        if (
          usrPassCheck
          .is().min(7)
          .is().max(25)
          .has().not().spaces()
          .has().uppercase()
          .has().lowercase()
          //.has().symbols()
          //.has().digits()
          .is().not().oneOf(['Passw0rd', 'Password123'])
          .validate(this.props.reduxState.sys_editContent.newPassword)
        ) {
          if (this.props.reduxState.sys_editContent.newPassword == this.props.reduxState.sys_editContent.confirmPassword) {
            const payload = {
              cmd: 'editPassword',
              contents: {
                newPassword: this.props.reduxState.sys_editContent.newPassword,
                currentEmail: this.props.reduxState.usr_creds_email,
              },
            }
            this.props.socketIO(payload)
          } else {
            Alert.alert('Passwords must match');
          }
        } else {
          Alert.alert('Please enter valid password. Must contain 7 characters w/ uppercase')
        }
        break;
      case 'Email':
        if (emailCheck.validate(this.props.reduxState.sys_editContent.email)) {
        }else {
          Alert.alert('Please enter valid Email')
        }
        break;
      default:
    }
  }//updateTarget

  render() {
    const {
      reduxState: {
        usr_creds_username,
        usr_creds_email,
        vlt_meta_index,
        vlt_meta_directions,
        sys_activeTab,
        sys_pageLoaded,
        sys_editTarget,
        sys_editContent
      }
    } = this.props;

    switch (sys_editTarget) {
      case 'Username':
        this.currentPassword = sys_editContent.currentPassword
        this.currentPasswordPlaceholder = 'Enter current password'
        this.inputValue = sys_editContent.username
        this.placeholder = 'Enter new username'
        this.secureEntry = false
        break;
      case 'Password':
        this.currentPassword = sys_editContent.currentPassword
        this.currentPasswordPlaceholder = 'Enter current password'
        this.inputValue = sys_editContent.newPassword
        this.inputValue1 = sys_editContent.confirmPassword
        this.placeholder = 'Enter new password'
        this.placeholder1 = 'Confirm new password'
        this.secureEntry = true
        break;
      case 'Email':
        this.currentPassword = sys_editContent.currentPassword
        this.currentPasswordPlaceholder = 'Enter current password'
        this.inputValue = sys_editContent.email
        this.placeholder = 'Enter new email'
        this.secureEntry = false
        break;
      default:

    }

    return (
     <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
      <View style={styles.container}>
        <ScrollView style={styles.scrollView}>
            <Input
              leftIcon={
                <Icon
                  name="edit"
                  color= {'black'}
                  size={25}
                />
              }
              containerStyle={{ marginTop: 100, width: 300 }}
              onChange={this.edit.bind(this)}
              value={this.inputValue}
              inputStyle={styles.inputStyle}
              keyboardAppearance="light"
              placeholder={this.placeholder}
              secureTextEntry={this.secureEntry}
              autoFocus={false}
              autoCapitalize="none"
              autoCorrect={false}
              keyboardType="email-address"
              returnKeyType="next"
              //ref={input => (this.emailInput = input)}
              onSubmitEditing={() => {
                // this.setState({ email_valid: this.validateEmail(email) });
                // this.newPasswordInput.focus();
              }}
              blurOnSubmit={false}
              placeholderTextColor="black"
              // errorStyle={{ textAlign: 'center', fontSize: 12 }}
              // errorMessage={
              //   sys_pageLoaded ? null : 'Please enter a new username'
              // }
            />
            {sys_editTarget == 'Password' ? (
              <Input
                leftIcon={
                  <Icon
                    name="edit"
                    color= {'black'}
                    size={25}
                  />
                }
                containerStyle={{ marginTop: 7, width: 300 }}
                onChange={this.editConfirmPassword.bind(this)}
                value={this.inputValue1}
                inputStyle={styles.inputStyle}
                keyboardAppearance="light"
                placeholder={this.placeholder1}
                secureTextEntry={this.secureEntry}
                autoFocus={false}
                autoCapitalize="none"
                autoCorrect={false}
                keyboardType="email-address"
                returnKeyType="next"
                shake={this.shake}
                //ref={input => (this.emailInput = input)}
                onSubmitEditing={() => {
                  // this.setState({ email_valid: this.validateEmail(email) });
                  // this.newPasswordInput.focus();
                }}
                blurOnSubmit={true}
                placeholderTextColor="black"
                // errorStyle={{ textAlign: 'center', fontSize: 12 }}
                // errorMessage={
                //   sys_pageLoaded ? null : 'Please enter a new username'
                // }
              />
            ) : (
              null
            )}
            {/* <Input
              leftIcon={
                <Icon
                  name="edit"
                  color= {'black'}
                  size={25}
                />
              }
              containerStyle={{ marginTop: 50, width: 300 }}
              onChange={this.editCurrentPassword.bind(this)}
              value={this.currentPassword}
              inputStyle={styles.inputStyle}
              keyboardAppearance="light"
              placeholder={this.currentPasswordPlaceholder}
              secureTextEntry={true}
              autoFocus={false}
              autoCapitalize="none"
              autoCorrect={false}
              keyboardType="email-address"
              returnKeyType="next"
              blurOnSubmit={false}
              placeholderTextColor="black"
            /> */}
       </ScrollView>

          <ImageBackground source={bgImageNoLogo()} style={styles.bgImage}>
            <Button
              small
              disabled = {false}
              buttonStyle={styles.buttonStyle2}
              title={'Update'}
              titleStyle={{color: myColors.control}}
              onPress={()=>this.updateTarget()}
            />
          </ImageBackground>
      </View>
     </TouchableWithoutFeedback>
    );//return
  }//render
}//Component


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },
  subContainer: {
    //flex: 0.5,
    top: 0,
    left: 0,
    width: SCREEN_WIDTH,
    height: SCREEN_HEIGHT/2,
  },
  bgImage: {
    //flex: 0.5,
    top: 0,
    left: 0,
    width: SCREEN_WIDTH,
    height: SCREEN_HEIGHT/2,
    //justifyContent: 'center',
    //alignItems: 'center',
  },
  buttonStyle2: {
    width: SCREEN_WIDTH,
    height: 50,
    backgroundColor: myColors.vlt,
  },
  inputStyle: {
    fontSize: 16,
    marginLeft: 5,
    color: 'black'
  },
});

const mapStateToProps = ({ reduxState }) => ({ reduxState });

export default connect(mapStateToProps, {
  updateSysEditContent, updateSysWaitSocket, socketIO
})(NewUsername);
