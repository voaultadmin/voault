import React, { Component } from 'react';
import {
  ActivityIndicator,
  ImageBackground,
  Text,
  StyleSheet,
  View,
  TextInput,
  Keyboard,
  TouchableWithoutFeedback,
  TouchableHighlight,
  Platform,
  Image,
  WebView,
  Dimensions,
  ScrollView,
  Alert,
} from 'react-native';
import { connect } from 'react-redux';

import { Button, Icon, Avatar, ListItem, Input } from 'react-native-elements';
import { Card, CardTitle, CardContent, CardAction, CardButton, CardImage } from 'react-native-material-cards';

import {
  updateSysEditContent, updateSysWaitSocket, socketIO
} from '../../stateStore/actions';

import { myColors, socket, bgImageNoLogo } from '../../constants/constants';

const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;


class Help extends Component {
  constructor(props){
    super(props)

  }//constructor

  edit(event) {
    this.content = {
      currentPassword: this.props.reduxState.sys_editContent.currentPassword,
      username: this.props.reduxState.sys_editContent.username,
      newPassword: this.props.reduxState.sys_editContent.newPassword,
      confirmPassword: this.props.reduxState.sys_editContent.confirmPassword,
      email: event.nativeEvent.text,
    }
    this.props.updateSysEditContent(this.content)
  }

  sendEmail(){
    if (this.props.reduxState.sys_editContent.email.length > 5) {
      Alert.alert(
        'Send Email?',
        '',
        [
          {text: 'Yes', onPress: () => {
            const payload = {
              cmd: 'email',
              contents: {
                emailSubject: 'Help/Feedback',
                emailTarget: 'info@voault.com',
                emailText: this.props.reduxState.sys_editContent.email + ' ---- **** ---- ' + this.props.reduxState.usr_creds_email,
                emailType: 'help',
              },
            }
            this.props.socketIO(payload)
            }
          },
          {text: 'cancel', style: 'cancel'},
        ]
      );
    } else {
      Alert.alert('Please type your message first')
    }
  }//sendEmail

  render() {
    const {
      reduxState: {
        usr_creds_username,
        usr_creds_email,
        vlt_meta_index,
        vlt_meta_directions,
        sys_activeTab,
        sys_pageLoaded,
        sys_editTarget,
        sys_editContent
      }
    } = this.props;


    return (
     <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
      <View style={styles.container}>
        <ScrollView style={styles.scrollView}>
            <Input
              containerStyle={{ marginTop: 5, width: 300, height: 300 }}
              multiline = {true}
              onChange={this.edit.bind(this)}
              value={sys_editContent.email}
              inputStyle={styles.inputStyle}
              keyboardAppearance="light"
              placeholder={'Type message here...'}
              placeholderStyle={styles.inputStyle1}
              secureTextEntry={false}
              autoFocus={false}
              autoCapitalize="none"
              autoCorrect={false}
              //keyboardType="email-address"
              returnKeyType="next"
              //ref={input => (this.emailInput = input)}
              onSubmitEditing={() => {
                // this.setState({ email_valid: this.validateEmail(email) });
                // this.passwordInput.focus();
              }}
              blurOnSubmit={false}
              placeholderTextColor="black"
              // errorStyle={{ textAlign: 'center', fontSize: 12 }}
              // errorMessage={
              //   sys_pageLoaded ? null : 'Please enter a new username'
              // }
            />
       </ScrollView>
          <ImageBackground source={bgImageNoLogo()} style={styles.bgImage}>
            <Button
              icon={{name: 'email', color:'white', size: 25 }}
              disabled = {false}
              buttonStyle={styles.buttonStyle2}
              title={'Send message'}
              titleStyle={{color: myColors.control}}
              onPress={()=>this.sendEmail()}
            />
          </ImageBackground>
      </View>
     </TouchableWithoutFeedback>
    );//return
  }//render
}//Component


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },
  subContainer: {
    //flex: 0.5,
    top: 0,
    left: 0,
    width: SCREEN_WIDTH,
    height: SCREEN_HEIGHT/2,
  },
  bgImage: {
    //flex: 0.5,
    top: 0,
    left: 0,
    width: SCREEN_WIDTH,
    height: SCREEN_HEIGHT/2,
    //justifyContent: 'center',
    //alignItems: 'center',
  },
  buttonStyle2: {
    width: SCREEN_WIDTH,
    height: 50,
    backgroundColor: myColors.vlt,
  },
  inputStyle: {
    fontSize: 16,
    //fontStyle: 'italic',
    marginLeft: 5,
    color: 'black'
  },
  inputStyle1: {
    fontSize: 16,
    fontStyle: 'italic',
    marginLeft: 5,
    color: 'black'
  },
});

const mapStateToProps = ({ reduxState }) => ({ reduxState });

export default connect(mapStateToProps, {
  updateSysEditContent, updateSysWaitSocket, socketIO
})(Help);
