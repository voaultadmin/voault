import React, { Component } from 'react';
import {
  Text,
  StyleSheet,
  View,
  Keyboard,
  TouchableWithoutFeedback,
  Platform,
  Image,
  WebView,
  Dimensions,
  Alert,
  ScrollView,
  ImageBackground,
} from 'react-native';
import { connect } from 'react-redux';
import { vlts, tabs, tabsMeta, myColors, socket } from '../../constants/constants';
import { Button } from 'react-native-elements';

import {
  updateSysEditContent, updateSysWaitSocket, socketIO
} from '../../stateStore/actions';

const BG_IMAGE = require('../../images/bg-1.png');
const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;



class Legal extends Component {
  static navigationOptions = ({ navigation, router }) => {
    return {
      headerLeft: (
        <Button
          onPress={()=> {
            navigation.goBack()
          }}
          title=""
          color="#fff"
          icon={{name: 'chevron-left', color: myColors.vlt, size: 30}}
          buttonStyle={{
            backgroundColor: 'transparent',
          }}
        />
      ),
    };
  }

  componentDidMount(){
    socket.on('agreedTerms', (result, socketID) => {
      if ((result == true) && (socket.id == socketID)) {
        const payload = {
          cmd: 'login',
          contents: {
            username: this.props.reduxState.usr_creds_username,
            password: this.props.reduxState.usr_creds_password,
          },
        }
        this.props.socketIO(payload)
      }
    })
  }


  render() {

    const {
      reduxState: {
        usr_creds_fullName,
      }
    } = this.props;


    return (
     <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <ImageBackground source={BG_IMAGE} style={styles.bgImage}>
             <ScrollView style={styles.subContainer} contentContainerStyle={{flexGrow:1}}>
                <Text style={{color: 'black'}}> USER LICENSE AGREEMENT:

                  This User License Agreement (“USER AGREEMENT”) is
                  entered into by and between Voault (“VOAULT”)
                  and {usr_creds_fullName}, the user (“LICENSEE”) and is made effective as
                  of the date LICENSEE first logs into or uses any
                  products or services supplied by VOAULT and is
                  reaffirmed on each successive use or log in:{"\n"}{"\n"}
                  1. License. In consideration of the fees described
                  herein, and to abide by the terms and conditions
                  contained in this USER LICENSE AGREEMENT, VOAULT hereby
                  grants to LICENSEE a revocable license to occupy, for
                  the purposes and term herein described, a VOAULT locker
                  (a “LOCKER”) for (the “LICENSE”) at the
                  price agreed to between the parties. The specific
                  LOCKER may be re-designated by VOAULT at any time for
                  any reason.{"\n"}
                  2. Term and Early Termination. Either party can
                  terminate this agreement at any time by notice to the
                  other. Moreover, the LICENSEE shall remove all items
                  from the LOCKER prior to termination or the contents of
                  said LOCKER shall be deemed tendered to VOAULT. Should
                  LICENSEE breach or fail to observe any of the covenants
                  and conditions herein contained, after notice (the
                  “NOTICE”) and expiration of a reasonable opportunity to
                  cure such deficiencies, then VOAULT may, without
                  waiving any other legal rights or remedies, forthwith
                  terminate this agreement. Reasonable opportunity to
                  cure as used herein shall be that period of time
                  required under the circumstances (and as indicated in
                  the notice), but not to exceed 3 calendar days unless
                  the use of the LOCKER is harmful or detrimental to
                  public safety as determined by VOAULT in its sole
                  discretion. In the event of any malfunction, VOAULT
                  may require up to forty-eight (48) hours to permit the
                  retrieval of these items.{"\n"}
                  3. Use. The LOCKER is provided for the primary purpose
                  of dry storage of documents, technology or other non-
                  perishable items for a limited time until the same can
                  be retrieved by LICENSEE or their designee. LICENSEE
                  may use the LOCKER for storage only and may not conduct
                  commercial activities nor operate a business utilizing
                  the LOCKER. LICENSEE may not store illegal, toxic,
                  hazardous, flammable or other environmentally harmful
                  materials in the LOCKER, nor keep any items in or about
                  the LOCKER which may be prohibited by a standard form
                  of fire insurance policy or which may increase the
                  existing rate of insurance upon the building in which
                  LOCKER is located. VOAULT reserves the right record any
                  users and the premises surrounding any VOAULT property.
                  Further, the contents of anything committed to any
                  VOAULT premises is subject to search and review by
                  VOAULT staffers and their assigns.{"\n"}
                  4. Delinquent Accounts. LOCKER fees shall be deemed
                  delinquent if not paid on demand and when due. VOAULT
                  may retake the LOCKER at any time and dispose of all
                  items therein on any delinquency.{"\n"}
                  5. Access. VOAULT shall be permitted to access the
                  LOCKER at any time, and for any reason. LICENSEE
                  agrees not to block access to the LOCKER for any
                  reason.{"\n"}
                  6. Maintenance/Prohibited Activities. LICENSEE shall at
                  all times keep and maintain the LOCKER in a good and
                  sanitary order, condition and repair. LICENSEE shall
                  further not make or suffer to be made any alterations
                  to the LOCKER, or the building in which the Locker is
                  located. Under no circumstances, nor at any time, shall
                  LICENSEE change, modify or otherwise alter the LOCKER.
                  LICENSEE shall indemnify VOAULT for all liability and
                  damages caused by any violation of this paragraph. Any
                  expense incurred for clean up or disposal required by
                  VOAULT as the result of LICENSEE’S failure to comply
                  with the terms and conditions of this USER LICENSE
                  AGREEMENT shall be immediately reimbursed by LICENSEE.{"\n"}
                  7. Surrender. Upon the termination of the License
                  granted herein for any cause, LICENSEE shall
                  immediately vacate the LOCKER, remove LICENSEE’S
                  personal property therefrom, and restore possession of
                  the LOCKER to VOAULT in good order and repair. If
                  LICENSEE abandons, vacates, surrenders possession of
                  the LOCKER, or is dispossessed by process of law, or
                  otherwise, any personal property belonging to LICENSEE
                  and left in the LOCKER shall be deemed abandoned. In
                  the event of such breach or abandonment, VOAULT may, at
                  its option, dispose of any property left in the LOCKER
                  according to Delaware law regulating the disposition of
                  abandoned property. LICENSEE hereby appoints VOAULT as
                  LICENSEE’S agent to relocate LICENSEE’s personal
                  property upon termination of this agreement for storage
                  in a public warehouse or elsewhere, should LICENSEE
                  fail to voluntarily restore possession of the LOCKER to
                  VOAULT. The cost of such removal and storage shall be
                  paid by LICENSEE.{"\n"}
                  8. Rules and Regulations: LICENSEE covenants and agrees
                  to faithfully, promptly and strictly observe and
                  otherwise comply with all VOAULT rules and regulations
                  as may from time to time be promulgated by VOAULT, as
                  well as the rules, regulations and instructions of
                  local authorities, and all Municipal ordinances and
                  State and Federal statutes now in force, or which may
                  hereafter be in force. Failure to comply with the
                  foregoing rules, regulations, instructions or laws
                  shall constitute a material breach of this agreement.{"\n"}
                  9. Risk of Loss: In taking possession of the LOCKER and
                  accessing or otherwise utilizing VOAULT services under
                  this USER LICENSE AGREEMENT, LICENSEE acknowledge (s)he
                  does so at LICENSEE’S own risk of loss or injury.
                  LICENSEE has examined and knows the condition of the
                  LOCKER premises and acknowledges receipt of the LOCKER
                  in good order and repair. VOAULT shall not be obligated
                  to care for or otherwise protect the LICENSEE, or the
                  contents stored in LICENSEE’S LOCKER, or to perform any
                  services for LICENSEE. VOAULT MAKES NO WARRANTY OF ANY
                  KIND, EXPRESS OR IMPLIED, AS TO THE CONDITION OF THE
                  LOCKER, LOCKER DOORS, LIGHTING, ELECTRICAL FIXTURES,
                  VENTILATION OR OTHER CONDITIONS OF THE LOCKER OR THE
                  BUILDING IN WHICH THE LOCKER IS SITUATED. NOR DOES
                  MARINA WARRANT THE SUITABILITY OF THE LOCKER FOR
                  LICENSEE S INTENDED USE.{"\n"}
                  10. Indemnity. VOAULT shall not be liable for, and
                  Licensee (on behalf of LICENSEE and LICENSEE’S agents,
                  employees, guests and invitees) hereby waives all
                  claims against VOAULT for damages or other loss, injury
                  or damage, whether direct, consequential or incidental,
                  and regardless of cause or source of injury, including
                  litigation costs (collectively “CLAIMS”) resulting from
                  LICENSEE’S use of or access to the LOCKER, including
                  but not limited to, damage to goods, wares, and
                  merchandise stored in the LOCKER, or for personal
                  injury suffered by LICENSEE or the guests or invitees
                  of LICENSEE, except those Claims directly and
                  proximately caused by VOAULT’S active negligence or
                  intentional misconduct. LICENSEE shall insure, defend,
                  indemnify and hold harmless VOAULT and VOAULT’S owners,
                  officers, employees, agents, suppliers, guests and
                  invitees, from such Claims at LICENSEE’S sole expense.
                  IT IS LICENSEE’S RESPONSIBILITY TO OBTAIN INSURANCE
                  AGAINST LOSS OR DAMAGE TO LICENSEE’S PROPERTY STORED IN
                  THE LOCKER.{"\n"}
                  11. Assignment. This USER LICENSE AGREEMENT, and
                  LICENSEE’S rights thereunder, are personal to LICENSEE,
                  and may not be sold, transferred, encumbered, or
                  otherwise assigned. LICENSEE may not sublet the LOCKER
                  or otherwise transfer LICENSEE’S rights under this USER
                  LICENSE AGREEMENT without VOAULT’S prior written
                  consent. Any such attempted transfer shall terminate
                  LICENSEE’S rights under this USER LICENSE AGREEMENT, at
                  VOAULT’S option.{"\n"}
                  12. Attorney’s Fees/Liens. In the event any controversy
                  arises regarding the interpretation or enforcement of
                  this USER LICENSE AGREEMENT, the prevailing party shall
                  recover, whether or not suit be instituted, all
                  reasonable expenses incurred, including attorneys fees.
                  13. Notices. All notices to LICENSEE required under
                  this USER LICENSE AGREEMENT may be given either
                  personally, by facsimile transmission (in which event
                  the notice shall be effective as of the time of
                  delivery or facsimile transmission), or by depositing
                  the same in the United States mail, postage prepaid,
                  and addressed to LICENSEE at any address given by the
                  LICENSEE. Written notices to VOAULT shall be addressed
                  to the same at info@voault.com.{"\n"}
                  14. Entire Agreement/Enforceability. This agreement,
                  including any attachments, constitutes the entire
                  agreement between the parties, supercedes all prior
                  agreements or understandings between them, and may not
                  be modified without their prior written consent. This
                  agreement shall be interpreted fairly and shall not be
                  strictly construed either for or against either party.
                  In the event a court of competent jurisdiction finds
                  any portion of this agreement unenforceable, then the
                  unenforceable provision(s) shall be excised and the
                  remaining portions shall remain binding and enforceable
                  as between the parties. This agreement is binding upon
                  the heirs, personal representatives and other
                  successors in interest of the parties.{"\n"}
                  15. Dispute Resolution; Binding Arbitration.
                  PLEASE READ THE FOLLOWING SECTION CAREFULLY BECAUSE IT
                  REQUIRES LICENSEE TO ARBITRATE CERTAIN DISPUTES AND
                  CLAIMS WITH VOAULT AND LIMITS THE MANNER IN WHICH
                  LICENSEE CAN SEEK RELIEF FROM US. Except for disputes
                  in which LICENSEE or VOAULT seeks injunctive or other
                  equitable relief for the alleged unlawful use of
                  intellectual property, LICENSEE (and all successors
                  including trustees of LICENSEE’S assets and any third
                  party seeking to vindicate LICENSEE’s rights for any
                  purpose) and VOAULT waive LICENSEE’s rights to a jury
                  trial and to have any dispute arising out of or related
                  to these Terms or our Services resolved in court.
                  Instead, all disputes arising out of or relating to
                  these Terms or our Services will be resolved through
                  confidential binding arbitration held in Wilmington, DE
                  in accordance with the Streamlined Arbitration Rules
                  and Procedures ("Rules") of the Judicial Arbitration
                  and Mediation Services ("JAMS"), which are available on
                  the JAMS website and hereby incorporated by reference.
                  LICENSEE either acknowledge and agree that LICENSEE
                  have read and understand the rules of JAMS or waive
                  LICENSEE’S opportunity to read the rules of JAMS and
                  any claim that the rules of JAMS are unfair or should
                  not apply for any reason.{"\n"}
                  LICENSEE and VOAULT agree that any dispute arising out
                  of or related to these Terms or our Services is
                  personal to LICENSEE and VOAULT and that any dispute
                  will be resolved solely through individual arbitration
                  and will not be brought as a class arbitration, class
                  action or any other type of representative proceeding.
                  LICENSEE and VOAULT agree that these Terms affect
                  interstate commerce and that the enforceability of this
                  Section will be substantively and procedurally governed
                  by the Federal Arbitration Act, 9 U.S.C. § 1, et seq.
                  (the "FAA"), to the maximum extent permitted by
                  applicable law. As limited by the FAA, these Terms and
                  the JAMS Rules, the arbitrator will have exclusive
                  authority to make all procedural and substantive
                  decisions regarding any dispute and to grant any remedy
                  that would otherwise be available in court; provided,
                  however, that the arbitrator does not have the
                  authority to conduct a class arbitration or a
                  representative action, which is prohibited by these
                  Terms. The arbitrator may only conduct an individual
                  arbitration and may not consolidate more than one
                  individual’s claims, preside over any type of class or
                  representative proceeding or preside over any
                  proceeding involving more than one individual. LICENSEE
                  and Company agree that for any arbitration LICENSEE
                  initiate, LICENSEE will pay the filing fee and Company
                  will pay the remaining JAMS fees and costs. For any
                  arbitration initiated by Company, Company will pay all
                  JAMS fees and costs. LICENSEE and VOAULT agree that the
                  state or federal courts of the State of Delaware and
                  the United States sitting in Wilmington, DE have
                  exclusive jurisdiction over any appeals and the
                  enforcement of an arbitration award.{"\n"}
                  ANY CLAIM ARISING OUT OF OR RELATED TO THESE TERMS OR
                  OUR SERVICES MUST BE FILED WITHIN ONE YEAR AFTER SUCH
                  CLAIM AROSE; OTHERWISE, THE CLAIM IS PERMANENTLY
                  BARRED, WHICH MEANS THAT LICENSEE AND VOAULT WILL NOT
                  HAVE THE RIGHT TO ASSERT THE CLAIM.{"\n"}
                  LICENSEE have the right to opt out of binding
                  arbitration within thirty (30) days of the date
                  LICENSEE first accepted the terms of this Section by
                  sending an email to inquiry@VOAULT.com. In order to be
                  effective, the opt out notice must include LICENSEE’S
                  full name and clearly indicate LICENSEE’s intent to opt
                  out of binding arbitration. By opting out of binding
                  arbitration, LICENSEE are agreeing to resolve Disputes
                  in accordance with the following laws of Delaware
                  without reference to any choice of law principles and
                  to be conducted in the District Court of the State of
                  Delaware.{"\n"}
                  16. Refund Policy
                  LICENSEE assume responsibility for LICENSEE’S purchase,
                  and no refunds will be issued.
                  17. Governing Law. This Agreement shall be governed by
                  the laws of the State of Delaware, without regard to
                  conflicts of law provisions. Any legal action or
                  proceeding between VOAULT and LICENSEE related to this
                  Agreement will be brought exclusively in a federal or
                  state court of competent jurisdiction sitting in the
                  State of Delaware, New Castle County.{"\n"}
                  18. INTELLECTUAL PROPERTY
                  All content provided by VOAULT including on its
                  website, including but not limited, text, graphics,
                  logos, button icons, images, audio clips, trade names,
                  trademarks, service marks, trade dress, digital
                  downloads, data compilations, software, and the
                  compilation of any of the foregoing, is VOAULT property
                  and is protected by United States and international
                  patent, copyright, and trademark laws. The display and
                  availability of the content does not convey or create
                  any license or other rights in the content. Any
                  unauthorized copying, reverse engineering,
                  redistribution, reproduction, publication or
                  modification of Website content by any person without
                  our prior written authorization is strictly prohibited,
                  may be a violation of federal or common law, trademark,
                  patent and copyright laws and may subject such a
                  violator to legal action. VOAULT owns all the data that
                  users provide including but not limited to all question
                  content and associated analytic data.
                  The use of VOAULT content is prohibited. Requests for
                  permission to reproduce or distribute materials found
                  on the Website can be made by contacting VOAULT in
                  writing. LICENSEE is also strictly prohibited from
                  creating works or materials that derive from or are
                  based on VOAULT content or other materials including,
                  without limitation, fonts, icons, link buttons,
                  wallpaper, desktop themes and unlicensed merchandise.
                  This prohibition applies regardless of whether the
                  derivative materials are sold, bartered or given away.{"\n"}
                  19. LICENSEE Account
                  You are responsible for maintaining the confidentiality
                  of LICENSEE account and password and for restricting
                  access and LICENSEE agree to accept responsibility for
                  all activities that occur under LICENSEE’s account.
                  LICENSEE agree to immediately notify us of any
                  unauthorized use of LICENSEE’s password or account, or
                  any other breach of security of which LICENSEE is
                  aware.{"\n"}
                  LICENSEE acknowledges and agrees (s)he has read,
                  understands and approves this agreement and agrees to
                  be bound thereby.{"\n"}
                  </Text>

            </ScrollView>
            <View>
              <Button
                onPress={()=> {
                  Alert.alert(
                    'I have read and agree to the Terms and Conditions',
                    '',
                    [
                      {text: 'Yes', onPress: () => {
                        const payload = {
                          cmd: 'userAgreement',
                          contents: {
                            username: this.props.reduxState.usr_creds_username
                          },
                        }
                        this.props.socketIO(payload)
                        //this.props.navigation.navigate('Menu')
                        }
                      },
                      {text: 'cancel', style: 'cancel'},
                    ]
                  );
                }}
                title="I Agree"
                color="#fff"
                icon={{name: 'check', color: 'white', size: 30}}
                buttonStyle={{
                  backgroundColor: myColors.vlt,
                  width: SCREEN_WIDTH,
                }}
                // disabled={()=>{
                //   if (this.props.reduxState.usr_creds_password == '') {
                //     return true
                //   } else {
                //     return false
                //   }
                // }}
              />
            </View>

        {/* <WebView
        source={{ uri: 'https://drive.google.com/file/d/18JOZE9RPXJyZm0D9ZTu_TUjTAuTWuJgS/view?usp=sharing' }}
        /> */}
      </ImageBackground>
     </TouchableWithoutFeedback>
    );//return
  }//render
}//Component


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  subContainer: {
    flex: 1,
    marginLeft: 5,
    marginRight: 5,
    marginBottom: 10,
    //marginTop: 10,
    backgroundColor: 'rgb(205, 205, 205)'
  },
  bgImage: {
    flex: 1,
    top: 0,
    left: 0,
    width: SCREEN_WIDTH,
    height: SCREEN_HEIGHT,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

const mapStateToProps = ({ reduxState }) => ({ reduxState });

export default connect(mapStateToProps, {socketIO})(Legal);
