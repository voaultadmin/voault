import React, { Component } from 'react';
import {
  Alert,
  ActivityIndicator,
  ImageBackground,
  Text,
  StyleSheet,
  View,
  TextInput,
  Keyboard,
  TouchableWithoutFeedback,
  TouchableHighlight,
  Platform,
  Image,
  WebView,
  Dimensions,
  ScrollView
} from 'react-native';
import { connect } from 'react-redux';
import { myColors, socket,bgImageNoLogo } from '../../constants/constants';
import { Button, Icon, Avatar, ListItem } from 'react-native-elements';
import { Card, CardTitle, CardContent, CardAction, CardButton, CardImage } from 'react-native-material-cards'

import {
  updateUsrEmail, updateUsrUsername, updateSysEditTarget, updateSysWaitSocket, socketIO
} from '../../stateStore/actions';

const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;

class Profile extends Component {
  constructor(props){
    super(props)

    socket.on('accountDeleted', (result , socketID) => {
      if (socketID == socket.id) {
        switch (result) {
          case true:
            setTimeout(() => {
              this.props.updateUsrEmail('')
              this.props.updateUsrUsername('')
              this.props.navigation.navigate('Login')
            }, 500);
            break;
          case false:
              Alert.alert('Could not delete account, please try again later')
            break;
          default:
        }
      }
    })//accountDeleted
  }//constructor


  goTo(target){
    this.props.updateSysEditTarget(target)
    this.props.navigation.navigate('Edit')
  }

  deleteAccount(){
    Alert.alert(
      'Are you sure you want to delete your account?',
      '',
      [
        {text: 'Delete Account', onPress: () => {
          console.warn('delete account');
          // const payload = {
          //   cmd: 'deleteAccount',
          //   contents: {
          //     username: this.props.reduxState.usr_creds_username,
          //     password: this.props.reduxState.usr_creds_password,
          //   },
          // }
          // this.props.socketIO(payload)
        }},
        {
          text: 'Cancel',
          style: 'cancel',
        },
      ],
      {cancelable: true},
    );
  }

  render() {
    const list = [
      {
        title: 'Username',
        backgroundColor: 'white'
      },
      {
        title: 'Password',
        backgroundColor: 'white'
      },
      {
        title: 'Email',
        backgroundColor: 'white'
      },
    ]

    const {
      reduxState: {
        usr_creds_username,
        vlt_meta_index,
        vlt_meta_directions,
        sys_activeTab
      }
    } = this.props;

    return (
     <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
      <View style={styles.container}>
          <ScrollView style={styles.scrollView}>
            {
             list.map((item, i) => (
               <ListItem
                 containerStyle={{
                   backgroundColor: item.backgroundColor,
                   borderColor: 'white',
                   width: SCREEN_WIDTH,
                 }}
                 key={i}
                 title={item.title}
                 titleStyle={item.titleStyle}
                 subtitle={item.subtitle}
                 subtitleStyle={item.subtitleStyle}
                 leftIcon={{ name: 'edit' }}
                 rightIcon={{ name: 'chevron-right' }}
                 onPress={()=>this.goTo(item.title)}
               />
             ))
           }
         </ScrollView>
        <ImageBackground source={bgImageNoLogo()} style={styles.bgImage}>
             <ListItem
               containerStyle={{
                 backgroundColor: 'black',
                 borderColor: 'white',
                 width: SCREEN_WIDTH,
               }}
               key={0}
               title={'Delete Account'}
               titleStyle={{ color: 'white'}}
               // subtitle={item.subtitle}
               // subtitleStyle={item.subtitleStyle}
               leftIcon={{ name: 'delete', color: 'white' }}
               rightIcon={{ color: 'white'  }}
               //onPress={()=>this.deleteAccount()}
             />
       </ImageBackground>
      </View>
     </TouchableWithoutFeedback>
    );//return
  }//render
}//Component


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },
  bgImage: {
    //flex: 0.5,
    top: 0,
    left: 0,
    width: SCREEN_WIDTH,
    height: SCREEN_HEIGHT/2,
    //justifyContent: 'center',
    //alignItems: 'center',
  },
  scrollView: {
    //flex: 0.5,
    top: 0,
    left: 0,
    width: SCREEN_WIDTH,
    height: SCREEN_HEIGHT/2,
  },

});

const mapStateToProps = ({ reduxState }) => ({ reduxState });

export default connect(mapStateToProps, {
  updateUsrEmail, updateUsrUsername, updateSysEditTarget, updateSysWaitSocket, socketIO
})(Profile);
