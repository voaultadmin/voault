import React, { Component } from 'react';
import {
  Alert,
  ImageBackground,
  StyleSheet,
  View,
  Text,
  TextInput,
  Keyboard,
  TouchableWithoutFeedback,
  WebView,
  Dimensions,
  ScrollView
} from 'react-native';
import { connect } from 'react-redux';
import {
  DotIndicator,
  BallIndicator,
  BarIndicator,
  MaterialIndicator,
} from 'react-native-indicators';
import BottomNavigation, {
  FullTab
} from 'react-native-material-bottom-navigation';
import Swiper from 'react-native-swiper';
import { Avatar, Card, List, ListItem, Button, Icon } from 'react-native-elements';
import { vlts, myColors, tabs, tabsMeta, socket, bgImageNoLogo } from '../constants/constants';
import {
  socketIO, updateSysWaitSocket
 } from '../stateStore/actions';

const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;
const CARD_HEIGHT = 0.3 * SCREEN_HEIGHT
const CARDIMG_HEIGHT = SCREEN_HEIGHT



class Controls extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerLeft: (
        <Button
          onPress={()=> navigation.goBack()}
          title=""
          color="#fff"
          icon={{name: 'chevron-left', color: myColors.vlt, size: 35}}
          buttonStyle={{
            backgroundColor: 'transparent',
          }}
        />
      ),
    };
  }

  constructor(props){
    super(props)
    this.doorText = ''
    this.doorIndicatorColor = 'transparent'
    this.packagePresentText = ''
    this.packagePresentIndicatorColor = 'transparent'
    this.connectionIndicatorColor = 'red'
    this.connectionStatus = false
    this.firstWD = false

    socket.on('vltWatchdog', (tag) => {
      //console.warn(tag, this.props.reduxState.vlt_meta_tag);
      if (tag == this.props.reduxState.vlt_meta_tag) {
        this.connectionIndicatorColor = myColors.control1
        this.connectionStatus = true
        this.firstWD = true
        clearInterval(this.vltTimeoutTmr)
      }
    })

  }

  componentDidMount(){
    this.controlTmr = setTimeout(() => {
      this.props.navigation.navigate('Details')
      Alert.alert('Control screen timeout')
    }, 60000);

    setInterval(() => {
      this.vltTimeout()
    }, 10000);

    this.vltWatchdogReqInterval = setInterval(() => {
      this.payload = {
        cmd: 'vltWatchdogReq',
        contents: {
          tag: this.props.reduxState.vlt_meta_tag,
        }
      }
      this.props.socketIO(this.payload)
    }, 3000);
  }

  componentWillUnmount(){
    clearTimeout(this.controlTmr)
    clearTimeout(this.vltTimeoutTmr)
    clearInterval(this.vltWatchdogReqInterval)
  }

  cmd(cmd, status){
    clearTimeout(this.controlTmr)
    if (cmd !== status) {
      this.payload = {
        cmd: 'cmd',
        contents: {
          username: this.props.reduxState.usr_creds_username,
          cmd: cmd,
          target: this.props.reduxState.vlt_meta_unit,
        }
      }
      this.props.socketIO(this.payload)
      this.props.updateSysWaitSocket(true)
    } else if ((cmd == status) && (cmd == 'open')) {
      Alert.alert('Already Open')
    }
  }

  actionAlert(){
    Alert.alert(
      'Hardware Control' ,
      'Open?',
      [
        {text: 'Yes', onPress: () => {
          this.cmd(false, vlt_meta_unit)
          }
        },
        {text: 'cancel', style: 'cancel'},
      ]
    );
  }

  vltTimeout(){
    this.vltTimeoutTmr = setTimeout(() => {
      this.connectionIndicatorColor = 'red'
      this.connectionStatus = false
    }, 6000);
  }//cloudWatchdog

  enableOpenBtn(){
    //setInterval(() => {
      if (this.connectionStatus) {
        return false
      } else {
        return true
      }
    //}, 1000);
  }

  render() {

    const list = [
      {
        title: 'Connection',
        titleStyle: {
          color: myColors.control,
          //fontWeight: 'bold',
          fontSize: 16
        },
        //subtitle: 'Establised',
        subtitleStyle: {
          color: 'green',
          //fontWeight: 'bold',
          fontSize: 12
        },
        //leftIcon: 'router',
        //leftIconColor: myColors.control,
        rightIcon: 'brightness-1',
        rightIconColor: this.connectionIndicatorColor,
        backgroundColor: 'transparent',
      },
      {
        title: 'Door',
        //title: 'Package Removed',
        titleStyle: {
          color: myColors.control,
          //fontWeight: 'bold',
          fontSize: 16
        },
        subtitle: this.doorText,
        subtitleStyle: {
          color: this.doorIndicatorColor,
          fontWeight: 'bold',
          fontSize: 14
        },
        //leftIcon: 'input',
        // leftIconColor: myColors.control,
        rightIcon: 'brightness-1',
        rightIconColor: this.doorIndicatorColor,
        backgroundColor: 'transparent',
      },
      {
        title: 'Package Present',
        //title: 'Package Removed',
        titleStyle: {
          color: myColors.control,
          //fontWeight: 'bold',
          fontSize: 16
        },
        //ubtitle: 'Dropped Monday 19th at 3:23PM',
        subtitleStyle: {
          color: 'green',
          fontWeight: 'bold',
          fontSize: 12
        },
        //leftIcon: 'input',
        //leftIconColor: this.packagePresentIndicatorColor,
        rightIcon: 'brightness-1',
        rightIconColor: this.packagePresentIndicatorColor,
        backgroundColor: 'transparent',
      },
    ]

    const {
      reduxState: {
        vlt_creds_masterKey,
        vlt_creds_key1,
        vlt_creds_key2,
        vlt_meta_index,

        vlt_meta_unit,

        usr_creds_username,

        sys_waitSocket

      }
    } = this.props;

    const VLT_DOOR = vlts[vlt_meta_index].door

    if (this.connectionStatus == true) {
      switch (VLT_DOOR) {
        case true:
          this.doorText = 'Open'
          this.doorIndicatorColor = myColors.control1
          break;

        case false:
          this.doorText = 'Closed'
          this.doorIndicatorColor = myColors.control1
          break;

        default:
          this.doorText = ''
          this.doorIndicatorColor = 'transparent'
      }
    } else {
        this.doorText = ''
        this.doorIndicatorColor = 'transparent'
    }

    return (
     <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
      <View style={styles.container}>
        <ImageBackground source={bgImageNoLogo()} style={styles.bgImage}>
          <View style={styles.details}>
            <Card containerStyle={{ backgroundColor: myColors.vlt , justifyContent: 'space-between', borderWidth: 0, borderColor: 'black' }}
              title={this.props.reduxState.vlt_meta_unit} titleStyle={{ color: 'white', fontSize: 18 }}>
                {((sys_waitSocket == false) && (this.firstWD == true)) ? (
                   list.map((item, i) => (
                     <ListItem
                       containerStyle={{
                         marginTop: item.marginTop,
                         backgroundColor: item.backgroundColor,
                         borderColor: 'white',
                         //width: 0.*SCREEN_WIDTH,
                       }}
                       key={i}
                       title={item.title}
                       titleStyle={item.titleStyle}
                       subtitle={item.subtitle}
                       subtitleStyle={item.subtitleStyle}
                       leftIcon={{ name: item.leftIcon, color: item.leftIconColor }}
                       rightIcon={{ name: item.rightIcon, color: item.rightIconColor }}
                       //onPress={()=>this.props.navigation.navigate(item.onPress)}
                     />
                   ))
               ) : (
                 <View style={{ flex: 8, alignItems: 'center', justifyContent: 'center'}}>
                     <DotIndicator color='white' size={4} style={{marginTop: 150}}/>
                 </View>
               )
             }
            </Card>
          </View>

            <View style={styles.btnContainer}>
              <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginTop: 100 }}>
                <Button
                  small
                  disabled = {this.enableOpenBtn()}
                  buttonStyle={styles.buttonStyle1}
                  title={'Open'}
                  onPress={()=>this.actionAlert()}
                />
              </View>
            </View>
        </ImageBackground>
       </View>
     </TouchableWithoutFeedback>
    );//return


  }//render
}//Component


{/*  */}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //flexDirection: 'column',
    //justifyContent: 'space-between',
  },
  btnContainer: {
    width: SCREEN_WIDTH,
    height: 0.5 * SCREEN_HEIGHT,
    //backgroundColor: 'black'
  },
  details: {
    width: SCREEN_WIDTH,
    height: 0.5 * SCREEN_HEIGHT,
    //backgroundColor: myColors.vlt
  },
  bgImage: {
    flex: 1,
    top: 0,
    left: 0,
    // width: SCREEN_WIDTH,
    // height: SCREEN_HEIGHT,
    //justifyContent: 'center',
    //alignItems: 'center',
  },
  txt1: {
    color: 'black',
    fontWeight: 'bold',
    fontSize: 18,
    //marginTop: 5,
  },
  txt2: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 14,
    marginTop: 30,
  },
  buttonStyle1: {
    width: 300,
    height: 50,
    //marginTop: 40,
    //marginLeft: 15,
    backgroundColor: myColors.control1,
    //borderRadius: 20,
    borderWidth: 0.3,
  },
});

const mapStateToProps = ({ reduxState }) => ({ reduxState });

export default connect(mapStateToProps, {socketIO, updateSysWaitSocket})(Controls);
