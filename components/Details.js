import React, { Component } from 'react';
import {
  Alert,
  ImageBackground,
  StyleSheet,
  View,
  Text,
  TextInput,
  Keyboard,
  TouchableWithoutFeedback,
  Platform,
  Image,
  Dimensions,
  Scrollview,
  Modal,
  WebView
} from 'react-native';
import { connect } from 'react-redux';
import Spinner from 'react-native-loading-spinner-overlay';
import { Avatar, Card, List, ListItem, Button, Icon} from 'react-native-elements';

import {
  updateSysPageLoaded1, updateVltUnit, updateVltMasterKey, updateVltKey1, updateVltKey2, updateSysWaitSocket, updateSysViType, socketIO
} from '../stateStore/actions';
import {
  DotIndicator,
  BallIndicator,
  BarIndicator,
  MaterialIndicator,
} from 'react-native-indicators';
import { vlts, tabs, tabsMeta, myColors, socket, bgImageNoLogo } from '../constants/constants';

const BTN_TEXT_SIZE = 25;
const BTN_HEIGHT = 50;
const BTN_WIDTH = 95;


const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;
const CARD_HEIGHT = 0.65 * SCREEN_HEIGHT
const CARDIMG_HEIGHT = 0.3 * SCREEN_HEIGHT


class Details extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerLeft: (
        <Button
          onPress={()=> navigation.goBack()}
          title=""
          color="#fff"
          icon={{name: 'chevron-left', color: myColors.vlt, size: 35}}
          buttonStyle={{
            backgroundColor: 'transparent',
          }}
        />
      ),
      headerStyle: {
        backgroundColor: myColors.color2,
        borderWidth: 1,
        borderBottomColor: myColors.color2 },
    };
  }

  constructor(props){
    super(props)
    //Get current keys
    let payload = {
      cmd: 'currentKeys',
      contents: {
        socket: '',
        username: '',

        key1: this.props.reduxState.vlt_creds_key1,
        key2: this.props.reduxState.vlt_creds_key2
      },
    }
    this.props.socketIO(payload)
  }//constructor

  componentWillUnmount(){
    this.props.updateVltUnit('')
  }

  control(unit){
        if ((this.controlBtnLogo !== 'vpn-key') && (vlts[this.props.reduxState.vlt_meta_index].reserved == false)) {
          this.props.updateSysPageLoaded1('false')
          this.reserve(unit)
        }else if ((this.controlBtnLogo == 'vpn-key') && (vlts[this.props.reduxState.vlt_meta_index].reserved == true)){
          this.props.updateSysWaitSocket('true')
          this.props.updateVltUnit(unit)
          this.props.navigation.navigate('Controls')
        }
    }//control

  reserve(unit){
    Alert.alert(
      'Choose Service:' ,
      '',
      [
        // {text: 'Pickup', onPress: () => {
        //   this.props.updateSysViType('Pickup')
        //   this.props.navigation.navigate('ViPickup')
        //   }
        // },
        {text: 'Trade', onPress: () => {
          this.props.updateSysViType('Trade')
          this.props.navigation.navigate('ViTrade')
          }
        },
        {text: 'Storage', onPress: () => {
          this.props.updateSysViType('Store')
          this.props.navigation.navigate('ViStorage')
          }
        },
        {text: 'Cancel', style: 'cancel'},
      ]
    );
  }

  adminControl(unit){
    switch (this.props.reduxState.usr_creds_username) {
      case vlts[this.props.reduxState.vlt_meta_index].adminKey:
        this.props.updateSysWaitSocket('true')
        this.props.updateVltUnit(unit)
        this.props.navigation.navigate('Controls')
        break;
      case '#dahirou':
        this.props.updateSysWaitSocket('true')
        this.props.updateVltUnit(unit)
        this.props.navigation.navigate('Controls')
      break;
      default:
    }
  }

  addKeys(){
    this.props.navigation.navigate('Keys')
  }

  removeKeys(){
    if (this.keyBtnColor == myColors.control1) {
      Alert.alert(
        'Remove keys?' ,
        '',
        [
          {text: 'Yes', onPress: () => {
              this.props.updateSysWaitSocket('true')
              setTimeout(() => {
                const payload = {
                  cmd: 'removeKeys',
                  contents: {
                    username: this.props.reduxState.usr_creds_username,
                    unit: this.props.reduxState.vlt_meta_unit,
                  }
                }
                this.props.socketIO(payload)
              }, 250);
            }
          },
          {text: 'cancel', style: 'cancel'},
        ]
      );
    }
  }//removeKeys

  release(){
    Alert.alert(
      'Release?' ,
      '',
      [
        {text: 'Yes', onPress: () => {
          this.props.updateSysWaitSocket('true')
          setTimeout(() => {
            let payload = {
              cmd: 'release',
              contents: {
                username: this.props.reduxState.usr_creds_username,

                cluster: this.props.reduxState.vlt_meta_zip,
                tag: this.props.reduxState.vlt_meta_tag,
                unit: this.props.reduxState.vlt_meta_unit,
              },
            }
            this.props.socketIO(payload)
            this.props.updateVltKey1('#key1')
            this.props.updateVltKey2('#key2')

            Alert.alert(
              'Feedback?',
              'Claim a 1 hour credit per review',
              [
                {text: 'Sure!', onPress: () => {
                  this.props.navigation.navigate('Help')
                  }
                },
                {text: 'No Thanks'},
              ]
            );

          }, 100);
          }
        },
        {text: 'cancel', style: 'cancel'},
      ]
    );
  }//release

  render() {
    const {
      reduxState: {
        usr_creds_username,

        vlt_meta_index,
        vlt_meta_unit,
        vlt_meta_directions,

        sys_reserveBtnTxt,
        sys_reserveBtnColor,
        sys_reserveBtnLogo,
        sys_activeTab,
        sys_waitSocket,
        sys_pageLoaded1,
      }
    } = this.props;

    const VLT_IMAGE = vlts[vlt_meta_index].image_url
    const VLT_TAG = vlts[vlt_meta_index].tag
    const VLT_UNIT = vlts[vlt_meta_index].unit
    const VLT_ADDRESS = vlts[vlt_meta_index].address
    const VLT_TITLE = vlts[vlt_meta_index].title
    const VLT_SIZE = vlts[vlt_meta_index].size
    const VLT_RATE = vlts[vlt_meta_index].rate

    const VLT_RESERVED = vlts[vlt_meta_index].reserved
    const VLT_MASTERKEY = vlts[vlt_meta_index].masterKey
    const VLT_KEY1 = vlts[vlt_meta_index].key1
    const VLT_KEY2 = vlts[vlt_meta_index].key2

    //Update style
    this.releaseBtnDisabled = true
    this.reserveBtnDisabled = true
    this.controlBtnDisabled = true
    this.usrHasControl = false


    if (VLT_RESERVED) {
      this.controlBtnColor = myColors.vlt
      this.controlBtnTextColor = myColors.control
      this.controlBtnText = ''
      this.controlBtnLogo = 'vpn-key'
      this.usrHasControl = false

      switch (usr_creds_username) {
        case VLT_MASTERKEY:
          this.releaseBtnDisabled = false
          this.reserveBtnDisabled = false
          this.usrHasControl = true
          break;

        case VLT_KEY1:
          this.reserveBtnDisabled = false
          this.usrHasControl = true
          break;

        case VLT_KEY2:
          this.reserveBtnDisabled = false
          this.usrHasControl = true
          break;
        default:
      }

      this.controlBtnDisabled = false
    } else {
      //this.reserveBtnDisabled = false
      this.controlBtnColor = myColors.control1
      this.controlBtnTextColor = myColors.control
      this.controlBtnText = ''
      this.controlBtnLogo = 'lock'
      this.usrHasControl = true
    }

    if ((VLT_KEY1 !== '#key1') || (VLT_KEY2 !== '#key2')) {
      this.keyBtnColor = myColors.control1
    }
    else {
      this.keyBtnColor = 'transparent'
    }


    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <View style={styles.container}>
          <ImageBackground source={bgImageNoLogo()} style={styles.bgImage}>
              <View style={{ justifyContent: 'center', alignItems: 'center'}}>
                {/* <Modal
                    visible={true}
                    //onRequestClose={() => this.setState({ showModal: false })}
                > */}
                <Spinner
                   visible={!sys_pageLoaded1}
                 />
                <Card containerStyle={{ backgroundColor: myColors.vlt, justifyContent: 'space-between', borderWidth: 0, borderColor: 'black' }} title={vlt_meta_unit}
                  titleStyle={{ color: 'white', fontSize: 18, fontWeight: 'bold' }} >
                  {/* <Text style={styles.txt1}> Size: {VLT_SIZE},  Rate: ${VLT_RATE}/hr </Text> */}
                  <View style={{ justifyContent: 'space-between', alignItems: 'center', marginBottom: 5 }}>
                    <Text style={styles.txt2}> {VLT_TITLE} </Text>
                    <Text style={styles.txt2}> {VLT_ADDRESS} </Text>
                    <Text style={styles.txt2}> Rate: ${VLT_RATE} per hour </Text>
                  </View>

                  <Image style={styles.imgStyle} resizeMode="cover" source={{ uri: VLT_IMAGE }} />
                    {this.releaseBtnDisabled == false ? (
                      <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginTop: 10  }}>
                        <Button
                          //raised
                          title=""
                          //activeOpacity={1}
                          //underlayColor="transparent"
                          icon={{name: 'person-outline', color:'white', size: BTN_TEXT_SIZE }}
                          onPress={()=> this.removeKeys()}
                          //loading={showLoading}
                          loadingProps={{ size: 'small', color: 'white' }}
                          //disabled={!email_valid && password.length < 8}
                          buttonStyle={{
                            height: BTN_HEIGHT,
                            width: BTN_WIDTH,
                            backgroundColor: this.keyBtnColor,
                            borderWidth: 0.5,
                            borderColor: myColors.action,
                          }}
                          //containerStyle={{ marginVertical: 10 }}
                          titleStyle={styles.hyperlink}
                        />
                        <Button
                          //raised
                          title={""}
                          //activeOpacity={1}
                          //underlayColor="transparent"
                          disabled={this.releaseBtnDisabled}
                          icon={{name: 'person-add', color: myColors.control, size: BTN_TEXT_SIZE }}
                          onPress={() => this.addKeys()}
                          //loading={showLoading}
                          loadingProps={{ size: 'large', color: 'white' }}
                          //disabled={!email_valid && password.length < 8}
                          buttonStyle={{
                            height: BTN_HEIGHT,
                            width: BTN_WIDTH,
                            backgroundColor: this.keyBtnColor,
                            borderWidth: 0.5,
                            borderColor: myColors.action,
                          }}
                          //containerStyle={{ marginVertical: 10 }}
                          titleStyle={styles.hyperlink}
                        />
                        <Button
                          //raised
                          title={""}
                          //activeOpacity={1}
                          //underlayColor="transparent"
                          disabled={this.releaseBtnDisabled}
                          icon={{name: 'eject', color: myColors.control, size: BTN_TEXT_SIZE}}
                          onPress={()=> this.release(VLT_UNIT)}
                          //loading={showLoading}
                          loadingProps={{ size: 'large', color: 'white' }}
                          //disabled={!email_valid && password.length < 8}
                          buttonStyle={{
                            height: BTN_HEIGHT,
                            width: BTN_WIDTH,
                            backgroundColor: myColors.control1,
                            borderWidth: 0.5,
                            borderColor: myColors.action,
                          }}
                          //containerStyle={{ marginVertical: 10 }}
                          titleStyle={{ fontWeight: 'bold', color: 'white' }}
                        />
                      </View>
                    ) : (
                      <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginTop: 5  }}>
                        <Button
                          //raised
                          buttonStyle={{
                            height: 45,
                            width: 200,
                            backgroundColor: 'transparent',
                          }}
                        />
                        <Button
                          //raised
                          buttonStyle={{
                            height: 45,
                            width: 75,
                            backgroundColor: 'transparent',
                          }}
                          //containerStyle={{ marginVertical: 5 }}
                        />
                      </View>
                      )
                    }

                  <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginTop: 10 }}>
                    <Button
                      //raised
                      title=""
                      //activeOpacity={1}
                      //underlayColor="transparent"
                      icon={{name: 'videocam', color:'white', size: BTN_TEXT_SIZE }}
                      onPress={()=> this.props.navigation.navigate('LiveCams')}
                      //loading={showLoading}
                      loadingProps={{ size: 'small', color: 'white' }}
                      //disabled={!email_valid && password.length < 8}
                      buttonStyle={{
                        height: BTN_HEIGHT,
                        width: BTN_WIDTH,
                        backgroundColor: 'transparent',
                        borderWidth: 0.5,
                        borderColor: 'white',
                        //borderRadius: 30,
                      }}
                      //containerStyle={{ marginVertical: 10 }}
                      titleStyle={styles.hyperlink}
                    />
                    <Button
                      //raised
                      title=""
                      //activeOpacity={1}
                      //underlayColor="transparent"
                      icon={{name: 'info', color:'white', size: BTN_TEXT_SIZE }}
                      onPress={()=> this.props.navigation.navigate('Info')}
                      onLongPress={()=> this.adminControl(VLT_UNIT)}
                      //loading={showLoading}
                      loadingProps={{ size: 'small', color: 'white' }}
                      //disabled={!email_valid && password.length < 8}
                      buttonStyle={{
                        height: BTN_HEIGHT,
                        width: BTN_WIDTH,
                        backgroundColor: 'transparent',
                        borderWidth: 0.5,
                        borderColor: 'white',
                        //borderRadius: 30,
                      }}
                      //containerStyle={{ marginVertical: 10 }}
                      titleStyle={styles.hyperlink}
                    />
                    <Button
                      //raised
                      title=""
                      //activeOpacity={1}
                      //underlayColor="transparent"
                      icon={{name: 'directions-car', color:'white', size: BTN_TEXT_SIZE }}
                      onPress={()=> this.props.navigation.navigate('Gmaps')}
                      //loading={showLoading}
                      loadingProps={{ size: 'small', color: 'white' }}
                      //disabled={!email_valid && password.length < 8}
                      buttonStyle={{
                        height: BTN_HEIGHT,
                        width: BTN_WIDTH,
                        backgroundColor: 'transparent',
                        borderWidth: 0.5,
                        borderColor: 'white',
                        //borderRadius: 30,
                      }}
                      //containerStyle={{ marginVertical: 10 }}
                      titleStyle={styles.hyperlink}
                    />
                  </View>
                </Card>


                {(this.usrHasControl) && (this.controlBtnColor == myColors.control1) ? (
                  <Button
                    raised
                    title={this.controlBtnText}
                    titleStyle={{color: this.controlBtnTextColor}}
                    //activeOpacity={1}
                    //underlayColor="transparent"
                    //disabled={this.reserveBtnDisabled}
                    icon={{name: this.controlBtnLogo, color: this.controlBtnTextColor, size: 30}}
                    onPress={()=> this.control(VLT_UNIT)}
                    //loading={showLoading}
                    loadingProps={{ size: 'small', color: 'white' }}
                    //disabled={!email_valid && password.length < 8}
                    buttonStyle={{
                      height: 60,
                      width: 350,
                      backgroundColor: this.controlBtnColor,
                      borderWidth: 0.4,
                      borderColor: myColors.action,
                      borderRadius: 15,
                    }}
                    containerStyle={{ marginVertical: 10 }}
                    titleStyle={styles.hyperlink}
                  />
                ):(this.usrHasControl) && (this.controlBtnColor == myColors.vlt) ? (
                  <Button
                    raised
                    title={this.controlBtnText}
                    //activeOpacity={1}
                    //underlayColor="transparent"
                    disabled={this.controlBtnDisabled}
                    icon={{name: this.controlBtnLogo, color: this.controlBtnTextColor, size: 30}}
                    onPress={()=> this.control(VLT_UNIT)}
                    //loading={showLoading}
                    loadingProps={{ size: 'small', color: 'white' }}
                    //disabled={!email_valid && password.length < 8}
                    buttonStyle={{
                      height: 60,
                      width: 350,
                      backgroundColor: this.controlBtnColor,
                      borderWidth: 0.4,
                      borderColor: myColors.action,
                      borderRadius: 15,
                    }}
                    containerStyle={{ marginVertical: 10 }}
                    titleStyle={styles.hyperlink}
                  />
              ):(
                <View style={{ flex: 8, alignItems: 'center', justifyContent: 'center'}}>
                    <Text></Text>
                </View>
                )
              }
            </View>
          </ImageBackground>
        </View>
      </TouchableWithoutFeedback>
    );//return
  }//render
}//Component


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  imgStyle: {
    height: CARDIMG_HEIGHT,
    width: 320,
  },
  btn: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgb(185, 185, 185)',
  },
  bgImage: {
    flex: 1,
    top: 0,
    left: 0,
    width: SCREEN_WIDTH,
    height: SCREEN_HEIGHT,
  },
  txt1: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 13,
    textAlign: 'center',
    marginBottom: 5,
  },
  txt2: {
    color: 'white',
    fontWeight: 'bold',
    //fontStyle: 'italic',
    fontSize: 13.5,
    textAlign: 'center',
    marginBottom: 5,
  },
  hyperlink: {
    color: 'white',
    fontSize: 14,
    fontWeight:'bold',
    //textDecorationLine:'underline',
    marginTop: 5,
    height: 30
  }

});

const mapStateToProps = ({ reduxState }) => ({ reduxState });

export default connect(mapStateToProps, {
  updateSysPageLoaded1, updateVltUnit, updateVltMasterKey, updateVltKey1, updateVltKey2, updateSysWaitSocket, updateSysViType, socketIO
})(Details);
