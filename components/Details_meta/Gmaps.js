import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Keyboard,
  TouchableWithoutFeedback,
  Platform,
  Image,
  WebView,
} from 'react-native';
import { connect } from 'react-redux';
import { myColors } from '../../constants/constants';

import { Button } from 'react-native-elements';

class Gmaps extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerLeft: (
        <Button
          onPress={()=> navigation.navigate('Details')}
          title=""
          color="#fff"
          icon={{name: 'keyboard-arrow-down', color: myColors.vlt, size: 35}}
          buttonStyle={{
            backgroundColor: 'transparent',
          }}
        />
      ),
    };
  }

  render() {
    const {
      reduxState: {
        vlt_meta_directions,
      }
    } = this.props;


    return (
     // <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
      <View style={styles.container}>
        <WebView
        //startInLoadingState
        source={{uri: vlt_meta_directions.map}}
        //source={{uri: 'https://www.google.com'}}
        //originWhitelist={['*']}
        //style={{backgroundColor: 'black'}}
        startInLoadingState
        //pointerEvents="none"
        //scrollEnabled={true}
        useWebKit={true}
        />
      </View>

     // </TouchableWithoutFeedback>
    );//return
  }//render
}//Component


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'black'
  },
});

const mapStateToProps = ({ reduxState }) => ({ reduxState });

export default connect(mapStateToProps, {})(Gmaps);
