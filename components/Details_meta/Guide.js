import React, { Component } from 'react';
import {
  Text,
  ImageBackground,
  StyleSheet,
  View,
  Keyboard,
  TouchableWithoutFeedback,
  Platform,
  WebView,
  Dimensions,
} from 'react-native';
import { connect } from 'react-redux';
import { Image, Button } from 'react-native-elements';
import { dummys, myColors, vlts, currentKeys, searchKeys, socket } from '../../constants/constants';

const BG_IMAGE = require('../../images/bg.png');
const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;
const CARDIMG_HEIGHT = 0.15 * SCREEN_HEIGHT
const CARDIMG_WIDTH = 0.6 * SCREEN_WIDTH

class Guide extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerLeft: (
        <Button
          onPress={()=> navigation.navigate('Details')}
          title=""
          color="#fff"
          icon={{ name: 'keyboard-arrow-down', color: 'white', size: 45 }}
          buttonStyle={{
            backgroundColor: 'transparent',
          }}
        />
      ),
    };
  }

  render() {
    const {
      reduxState: {
        vlt_meta_index,
        vlt_meta_directions,
      }
    } = this.props;

    const VLT_VIDEO = vlts[vlt_meta_index].video_url

    return (
     <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
      <View style={styles.container}>
        <ImageBackground source={BG_IMAGE} style={styles.bgImage}>
          <View style={styles.details}>

              <Image style={styles.imgStyle} resizeMode="cover" source={{ uri: VLT_VIDEO }} />

          </View>
        </ImageBackground>
        {/* <WebView
        source={{ uri: vlt_meta_directions.guide }}
        /> */}
      </View>
     </TouchableWithoutFeedback>
    );//return
  }//render
}//Component


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  bgImage: {
    flex: 1,
    top: 0,
    left: 0,
    width: SCREEN_WIDTH,
    height: SCREEN_HEIGHT,
    justifyContent: 'center',
    alignItems: 'center',
  },
  imgStyle: {
    height: CARDIMG_HEIGHT,
    width: CARDIMG_WIDTH,
  },
  details: {
    // width: SCREEN_WIDTH,
    // height: 0.6*SCREEN_HEIGHT,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

const mapStateToProps = ({ reduxState }) => ({ reduxState });

export default connect(mapStateToProps, {})(Guide);
