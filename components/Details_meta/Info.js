import React, { Component } from 'react';
import {
  ActivityIndicator,
  Image,
  ImageBackground,
  Text,
  StyleSheet,
  View,
  TextInput,
  Keyboard,
  TouchableWithoutFeedback,
  TouchableHighlight,
  Platform,
  WebView,
  Dimensions,
} from 'react-native';
import { connect } from 'react-redux';
import { Avatar, Card, List, ListItem, Button, Icon } from 'react-native-elements';
import { dummys, myColors, vlts, currentKeys, searchKeys, socket, bgImageNoLogo } from '../../constants/constants';
const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;
const CARDIMG_HEIGHT = 0.15 * SCREEN_HEIGHT
const CARDIMG_WIDTH = 0.6 * SCREEN_WIDTH


class Info extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerLeft: (
        <Button
          onPress={()=> navigation.navigate('Details')}
          title=""
          color="#fff"
          icon={{name: 'keyboard-arrow-down', color: myColors.vlt, size: 35}}
          buttonStyle={{
            backgroundColor: 'transparent',
          }}
        />
      ),
    };
  }
  render() {

    const {
      reduxState: {
        usr_creds_username,
        vlt_meta_index,
        vlt_meta_directions,
        vlt_meta_unit,
        vlt_meta_rate,
        sys_activeTab,
      }
    } = this.props;

    return (
     <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
      <View style={styles.container}>
        <WebView
          source={{uri: 'https://www.voault.com'}}
          //injectedJavaScript={`document.f1.submit()`}
          startInLoadingState
          useWebKit={true}
        />
      </View>
     </TouchableWithoutFeedback>
    );//return
  }//render
}//Component
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'black'
  },
  bgImage: {
    flex: 1,
    top: 0,
    left: 0,
    width: SCREEN_WIDTH,
    height: SCREEN_HEIGHT,
    justifyContent: 'center',
    alignItems: 'center',
  },
  txt1: {
    color: myColors.vlt,
    fontWeight: 'bold',
    fontSize: 14,
    textAlign: 'center',
    marginBottom: 20,
    // marginTop: 7,
  },
  txt2: {
    color: myColors.vlt,
    fontWeight: 'bold',
    //fontStyle: 'italic',
    fontSize: 13.5,
    textAlign: 'center',
    marginBottom: 10,
    // marginTop: 7,
  },
  details: {
    // width: SCREEN_WIDTH,
    // height: 0.6*SCREEN_HEIGHT,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
const mapStateToProps = ({ reduxState }) => ({ reduxState });
export default connect(mapStateToProps, {})(Info);
