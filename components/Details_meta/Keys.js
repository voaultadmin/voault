import React, { Component } from 'react';
import {
  Alert,
  StyleSheet,
  View,
  Text,
  Keyboard,
  ScrollView,
  TouchableWithoutFeedback,
  Platform,
  Image,
  WebView,
  Dimensions,
  ImageBackground
} from 'react-native';
import { connect } from 'react-redux';
import Spinner from 'react-native-loading-spinner-overlay';
import { SearchBar, ListItem, Card, Button } from 'react-native-elements';

import { updateSysSearchValue, updateSysPageLoaded, updateSysPageLoaded1, updateSysWaitSocket,updateVltKey1, updateVltKey2, socketIO } from '../../stateStore/actions';
import { dummys, myColors, vlts, currentKeys, searchKeys, socket, bgImageNoLogo } from '../../constants/constants';
import {
  DotIndicator,
  BallIndicator,
  BarIndicator,
  MaterialIndicator,
} from 'react-native-indicators';

const btnTextSize = 25;

const CARD_HEIGHT = 0.50 * SCREEN_HEIGHT
const CARDIMG_HEIGHT = 0.05 * SCREEN_HEIGHT
const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;

const recentKeys = []

class Keys extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerLeft: (
        <Button
          onPress={()=> navigation.navigate('Details')}
          title=""
          color="#fff"
          icon={{name: 'keyboard-arrow-down', color: myColors.vlt, size: 35}}
          buttonStyle={{
            backgroundColor: 'transparent',
          }}
        />
      ),
    };
  }

  constructor(props){
    super(props)

    socket.on('currentKeys', (result, username, comingFrom) => {
      if (username == this.props.reduxState.usr_creds_username) {
        this.props.updateSysSearchValue('')
        this.pageLoaded = false
        this.props.updateVltKey1(vlts[this.props.reduxState.vlt_meta_index].key1)
        this.props.updateVltKey2(vlts[this.props.reduxState.vlt_meta_index].key2)
        if (currentKeys.length !== result.length) {
          diff = result.length - currentKeys.length
          if (diff > 0) {
            for (var k = 0; k < diff; k++) {
              var obj = {}
              currentKeys.unshift(obj)
            }
          }else {
            for (var k = 0; k > diff; k--) {
              currentKeys.splice(0,1)
            }
          }
        }

        for (var i = 0; i < result.length; i++) {
          if ((result[i].username !== '#key1') && (result[i].username !== '#key2')) {
            currentKeys[i].username = result[i].username
            currentKeys[i].name = result[i].name
            currentKeys[i].avatar_url = result[i].avatar_url
            currentKeys[i].lastSeen = result[i].lastSeen
            currentKeys[i].rightIconColor = myColors.color2

            if((result[i].username == dummys.key1) || (result[i].username == dummys.key2)) {
              currentKeys[i].bgColor = 'transparent'
            }else {
              currentKeys[i].bgColor = myColors.vlt
            }
          }else{
            currentKeys[i].rightIconColor = 'transparent'
          }
        }

        if (comingFrom == 'addKey2') {
          this.props.updateSysPageLoaded(false)
          this.props.updateSysPageLoaded1(true)
          const payload = {
            cmd: 'currentKeys',
            contents: {
              username: this.props.reduxState.usr_creds_username,

              masterKey: vlts[this.props.reduxState.vlt_meta_index].masterKey,
              key1: vlts[this.props.reduxState.vlt_meta_index].key1,
              key2: vlts[this.props.reduxState.vlt_meta_index].key2
            }
          }
          setTimeout(() => {
            this.props.socketIO(payload)
            this.props.navigation.navigate('Details')
          }, 500);
        }
        this.pageLoaded = true
        this.searching = false
      }
    })//currentKeys

    socket.on('searchKeys', (result, socketID) => {
       if (socketID == socket.id) {
        this.showLoading = false
        if (searchKeys.length !== result.length) {
          diff = result.length - searchKeys.length
          if (diff > 0) {
            for (var k = 0; k < diff; k++) {
              var obj = {}
              searchKeys.unshift(obj)
            }
          }else {
            for (var k = 0; k > diff; k--) {
              searchKeys.splice(0,1)
            }
          }
        }

        for (var i = 0; i < result.length; i++) {
          searchKeys[i].username = result[i].username
          searchKeys[i].name = result[i].name
          searchKeys[i].avatar_url = result[i].avatar_url

        }
      }
    })//searchKeys
  }//constructor

  componentWillMount(){
    const payload = {
      cmd: 'currentKeys',
      contents: {
        username: this.props.reduxState.usr_creds_username,

        masterKey: vlts[this.props.reduxState.vlt_meta_index].masterKey,
        key1: vlts[this.props.reduxState.vlt_meta_index].key1,
        key2: vlts[this.props.reduxState.vlt_meta_index].key2
      }
    }
    this.props.socketIO(payload)
    this.props.updateSysSearchValue('')
    this.searching = false
  }

  updateSearch(event){
    var tmr
    const search = event.nativeEvent.text
    this.props.updateSysSearchValue(search)
    this.searching = true
    tmr = setTimeout(() => {
      const payload = {
        cmd: 'searchKeys',
        contents: {
          username: this.props.reduxState.usr_creds_username,

          searchKeys: this.props.reduxState.sys_searchValue,
        }
      }
      if (search.length > 3) {
        this.showLoading = true
        this.searchLength = true
        this.props.socketIO(payload)
      }
      else if (search.length == 0) {
        this.searching = false
      }
      else {
        this.showLoading = false
        this.searchLength = false
      }
    }, 200);
  };


  addKey(key){
    if (key == this.props.reduxState.usr_creds_username) { //check that key insn't already added
      Alert.alert('You already have the master key')
    } else if ((key == vlts[this.props.reduxState.vlt_meta_index].key1) || (key == vlts[this.props.reduxState.vlt_meta_index].key2)) {
      Alert.alert(key + ' is already in your keychain')
    } else {
      if (vlts[this.props.reduxState.vlt_meta_index].key1 == dummys.key1) { //add key 1 or key 2
        const payload = {
          cmd: 'addKey',
          contents: {
            username: this.props.reduxState.usr_creds_username,

            unit: this.props.reduxState.vlt_meta_unit,
            idx: 1,
            key1: key,
            key2: this.props.reduxState.vlt_creds_key2
          }
        }
        this.props.socketIO(payload)
      } else {
        Alert.alert(
           key,
          'Swap with',
          [
            {text: vlts[this.props.reduxState.vlt_meta_index].key1, onPress: () => {
              const payload = {
                cmd: 'addKey',
                contents: {
                  username: this.props.reduxState.usr_creds_username,

                  unit: this.props.reduxState.vlt_meta_unit,
                  idx: 1,
                  key1: key,
                  key2: this.props.reduxState.vlt_creds_key2
                }
              }
              this.props.socketIO(payload)
              }
            },
            {text: vlts[this.props.reduxState.vlt_meta_index].key2, onPress: () => {
              const payload = {
                cmd: 'addKey',
                contents: {
                  username: this.props.reduxState.usr_creds_username,

                  unit: this.props.reduxState.vlt_meta_unit,
                  idx: 2,
                  key1: this.props.reduxState.vlt_creds_key1,
                  key2: key
                }
              }
              this.props.socketIO(payload)
              }
            },
            {text: 'cancel', style: 'cancel'},
          ]
        );
      }//else
    }//else
    this.searching = false
  }//addKey

  render() {
    const {
      reduxState: {
        vlt_creds_masterKey,
        usr_creds_username,
        vlt_meta_directions,
        vlt_meta_index,
        vlt_meta_unit,
        sys_searchValue,
        sys_pageLoaded,
        sys_pageLoaded1,
        sys_waitSocket
      }
    } = this.props;

    const VLT_MASTERKEY = vlts[vlt_meta_index].masterKey
    const VLT_KEY1 = vlts[vlt_meta_index].key1
    const VLT_KEY2 = vlts[vlt_meta_index].key2

    if ((VLT_KEY1 == '#key1') && (VLT_KEY2 !== '#key2')) {
      this.key1 = null
      this.key2 = VLT_KEY2
    } else if ((VLT_KEY1 !== '#key1') && (VLT_KEY2 == '#key2')) {
      this.key1 = VLT_KEY1
      this.key2 = null
    } else if ((VLT_KEY1 == '#key1') && (VLT_KEY2 == '#key2')) {
      this.key1 = null
      this.key2 = null
    } else {
      this.key1 = VLT_KEY1
      this.key2 = VLT_KEY2
    }

    //List item without gradient
    return (
     <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
      <View style={styles.container}>
        <ImageBackground source={bgImageNoLogo()} style={styles.bgImage}>
            <SearchBar
              searchIcon
              placeholder="Search #username...  (case sensitive)"
              onChange={this.updateSearch.bind(this)}
              value={sys_searchValue}
              autoFocus={false}
              autoCapitalize="none"
              autoCorrect={false}
              showLoading={this.showLoading}
              clearIcon={false}
            />
            <Spinner
               visible={ (!sys_pageLoaded) || (!sys_pageLoaded1) ? (true):(false) }
            />
            <Card containerStyle={{ backgroundColor: 'transparent', justifyContent: 'space-between', borderWidth: 0, borderColor: 'black' }}>
              <View>
                { (this.searching) && (this.searchLength) ? (
                  searchKeys.map((l, i) => (
                    <ListItem
                      containerStyle={{
                        //marginTop: 0.1,
                        backgroundColor: 'transparent',
                        borderColor: 'white',
                      }}
                      key={i}
                      //leftAvatar={{ source: { uri: l.avatar_url } }}
                      title={l.name}
                      titleStyle={{color: myColors.color2}}
                      subtitle={l.username}
                      subtitleStyle={{color: myColors.vlt}}
                      rightIcon={{name: 'add-circle', color: myColors.color2}}
                      onPress={()=>this.addKey(l.username)}
                    />
                  ))
                ):(!this.searching) && (sys_pageLoaded == true) ? (
                    currentKeys.map((l, i) => (
                      <ListItem
                        containerStyle={{
                          //marginTop: 0.1,
                          backgroundColor: l.bgColor,
                          borderColor: 'black',
                        }}
                        key={i}
                        //leftAvatar={{ source: { uri: l.avatar_url } }}
                        title={l.name}
                        titleStyle={{color: myColors.color2}}
                        subtitle={l.username}
                        subtitleStyle={{color: myColors.control2}}
                        rightIcon={{name: 'vpn-key', color: l.rightIconColor}}
                      />
                    ))
                  ):(
                    <View style={{ flex: 8, alignItems: 'center', justifyContent: 'center'}}>
                        <Text>Null</Text>
                    </View>
                  )

                  }
                </View>
            </Card>
        </ImageBackground>
      </View>

     </TouchableWithoutFeedback>
    );//return
  }//render
}//Component


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'black'
  },
  bgImage: {
    flex: 1,
    top: 0,
    left: 0,
    width: SCREEN_WIDTH,
    height: SCREEN_HEIGHT,
    //justifyContent: 'center',
    //alignItems: 'center',
  },
  txt1: {
    color: myColors.color2,
    fontWeight: 'bold',
    fontSize: 14,
    textAlign: 'center',
    marginBottom: 10,
  },
});

const mapStateToProps = ({ reduxState }) => ({ reduxState });

export default connect(mapStateToProps, {updateSysSearchValue, updateSysPageLoaded, updateSysPageLoaded1, updateSysWaitSocket, updateVltKey1, updateVltKey2, socketIO})(Keys);
