import React, { Component } from 'react';
import { ScreenOrientation } from 'expo';
import {
  Alert,
  ImageBackground,
  StyleSheet,
  View,
  Text,
  TextInput,
  Keyboard,
  TouchableWithoutFeedback,
  WebView,
  Dimensions,
} from 'react-native';
import { connect } from 'react-redux';
import BottomNavigation, {
  FullTab
} from 'react-native-material-bottom-navigation';
import Swiper from 'react-native-swiper';
import { Avatar, Card, List, ListItem, Button, Icon, Image } from 'react-native-elements';
import { vlts, myColors, tabs, tabsMeta, getNavigation, bgImageNoLogo } from '../../constants/constants';

const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;
const CARDIMG_HEIGHT = 0.15 * SCREEN_HEIGHT
const CARDIMG_WIDTH = 0.6 * SCREEN_WIDTH

class liveCams extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerLeft: (
        <Button
          onPress={()=> navigation.navigate('Details')}
          title=""
          color="#fff"
          icon={{name: 'keyboard-arrow-down', color: myColors.vlt, size: 35}}
          buttonStyle={{
            backgroundColor: 'transparent',
          }}
        />
      ),
    };
  }//navigationOptions

  componentWillMount(){
    this.iterator = '?size=1'
    i = 1
    setInterval(() => {
      this.iterator = '?size=' + i
      i++
    }, 1500);
  }

  render() {

    const {
      reduxState: {
        vlt_creds_key1,

        vlt_meta_unit,
        vlt_meta_index,

        sys_reserveBtnLogo,
        sys_reserveBtnColor,
        sys_reserveBtnTxt

      }
    } = this.props;

    const VLT_CAM = vlts[vlt_meta_index].cam_url + this.iterator
    const INJECTEDJS = `const meta = document.createElement('meta'); meta.setAttribute('content', 'width=device-width, initial-scale=0.5, maximum-scale=0.9, user-scalable=0'); meta.setAttribute('name', 'viewport'); document.getElementsByTagName('head')[0].appendChild(meta); `

    return (
     <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
      <View style={styles.container}>

          {/* <View style={styles.details}> */}
            {/* <View pointerEvents="none" style={{ flex: 0.5, backgroundColor: 'black'}}> */}
              <WebView
                //originWhitelist={['*']}
                style={{backgroundColor: 'black'}}
                startInLoadingState
                //injectedJavaScript={INJECTEDJS}
                //scalesPageToFit={false}
                source={{uri: VLT_CAM}}
                pointerEvents="none"
                //scalesPageToFit={true}
                scrollEnabled={true}
                useWebKit={true}
              />
            {/* </View> */}
       </View>
     </TouchableWithoutFeedback>
    );//return


  }//render
}//Component


const styles = StyleSheet.create({
  container: {
    flex: 0.5,
    //flexDirection: 'column',
    //justifyContent: 'space-between',
    backgroundColor: 'black'
  },
  bgImage: {
    //flex: 1,
    //top: 0,
    //left: 0,
    width: SCREEN_WIDTH,
    height: SCREEN_HEIGHT,
    //justifyContent: 'center',
    //alignItems: 'center',
  },
  details: {
    // width: SCREEN_WIDTH,
    // height: 0.6*SCREEN_HEIGHT,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

const mapStateToProps = ({ reduxState }) => ({ reduxState });

export default connect(mapStateToProps, {})(liveCams);
