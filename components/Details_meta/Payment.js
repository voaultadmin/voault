import React, { Component } from 'react';
import {
  Alert,
  StyleSheet,
  View,
  Keyboard,
  TouchableWithoutFeedback,
  Platform,
  Image,
  WebView,
} from 'react-native';
import { connect } from 'react-redux';
import { myColors, SERVERURL } from '../../constants/constants';
import {
  socketIO
} from '../../stateStore/actions';
import { Button } from 'react-native-elements';

class Payment extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerLeft: (
        <Button
          onPress={()=> navigation.goBack()}
          title=""
          color="#fff"
          icon={{name: 'keyboard-arrow-left', color: myColors.vlt, size: 35}}
          buttonStyle={{
            backgroundColor: 'transparent',
          }}
        />
      ),
    };
  }

  handleResponse = data => {
    if (data.title === "success") {
        // const payload = {
        //   cmd: 'reserve',
        //   contents: {
        //     username: this.props.reduxState.usr_creds_username,
        //     cluster: this.props.reduxState.vlt_meta_zip,
        //     tag: this.props.reduxState.vlt_meta_tag,
        //     unit: this.props.reduxState.vlt_meta_unit,
        //     expirationDate: this.props.reduxState.sys_viExpDate
        //   },
        // }
        // this.props.socketIO(payload)
        Alert.alert("Buyer notified....")
        this.props.navigation.navigate('Menu')
    } else if (data.title === "cancel") {
        this.props.navigation.goBack()
    } else {
        return;
    }
  };

  render() {
    const {
      reduxState: {
        vlt_meta_directions,
        vlt_meta_rate,
        sys_viTimeSlider
      }
    } = this.props;

    return (
     // <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
      <View style={styles.container}>
        <WebView
          source={{uri: `${SERVERURL}/paypal/${vlt_meta_rate}/${sys_viTimeSlider}`}}
          // source={{uri: 'http://google.com'}}
          onNavigationStateChange={data =>
              this.handleResponse(data)
          }
          //injectedJavaScript={`document.f1.submit()`}
          startInLoadingState
          useWebKit={true}
        />
      </View>

     // </TouchableWithoutFeedback>
    );//return
  }//render
}//Component


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'black'
  },
});

const mapStateToProps = ({ reduxState }) => ({ reduxState });

export default connect(mapStateToProps, {socketIO})(Payment);
