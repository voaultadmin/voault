import React, { Component } from 'react';
import {
  Alert,
  ImageBackground,
  StyleSheet,
  Dimensions,
  View,
  Keyboard,
  TouchableWithoutFeedback,
  Platform,
  Image,
  WebView,
  Text,
  ScrollView,
} from 'react-native';
import { connect } from 'react-redux';
import ToggleSwitch from 'toggle-switch-react-native';
import { myColors, bgImageNoLogo } from '../../constants/constants';
import {
  updateSysViTimeSlider, updateSysViToggle, updateSysViExpDate, socketIO
} from '../../stateStore/actions';
import moment from 'moment';
import { Button, Slider, Input, CheckBox, Card } from 'react-native-elements';
const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;


class ViStorage extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerLeft: (
        <Button
          onPress={()=> navigation.navigate('Details')}
          title=""
          color="#fff"
          icon={{name: 'keyboard-arrow-down', color: myColors.vlt, size: 35}}
          buttonStyle={{
            backgroundColor: 'transparent',
          }}
        />
      ),
    };
  }

  sliderUpdate(value){
    Date.prototype.addHours= function(h){
      this.setHours(this.getHours()+h);
      return this;
    }

    this.props.updateSysViExpDate(new Date().addHours(this.props.reduxState.sys_viTimeSlider))
    this.props.updateSysViTimeSlider(value)
  }

  toggle(status){
    switch (status) {
      case false:
        this.props.updateSysViTimeSlider(4)
        break;
      case true:
        this.props.updateSysViTimeSlider(72)
        break;
      default:
    }
    this.props.updateSysViToggle(status)
  }

  render() {
    const {
      reduxState: {
        sys_viTimeSlider,
        sys_viToggle,
        sys_viExpDate,
        vlt_meta_rate,
      }
    } = this.props;

    return (
     <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
       <View style={styles.container}>
         <Card title='VI Storage' containerStyle={{backgroundColor: 'transparent', borderWidth: 0}}/>
        <View style={styles.subContainer}>
          <View style={styles.sliderContainer}>
            <Text>Reservation Time at ${vlt_meta_rate}/hr </Text>
            <Text>{sys_viTimeSlider} Hours</Text>
          </View>
          <Slider
            disabled={sys_viToggle}
            value={sys_viTimeSlider}
            //style={{width: 300}}
            thumbTintColor={myColors.vlt}
            minimumTrackTintColor={myColors.vlt}
            maximumTrackTintColor="white"
            minimumValue={4}
            maximumValue={240}
            step={4}
            onValueChange={value => this.sliderUpdate(value)}
          />
          <Text style={{ marginBottom: 50}}>Expires: {moment(sys_viExpDate).format('MMMM Do YYYY, h:mm a')} </Text>
          {/* <ToggleSwitch
            isOn={sys_viToggle}
            onColor="green"
            offColor="gray"
            label="Item needs to be delivered?"
            labelStyle={{ color: "black", fontWeight: "900" }}
            size="medium"
            onToggle={isOn => this.toggle(isOn)}
          /> */}
          <Text>On Expiration</Text>
          <CheckBox
            title='Auto extend reservation'
            checked={!sys_viToggle}
            onPress={() => this.toggle(!sys_viToggle)}
          />
          <CheckBox
            title='Deliver item to home address'
            checked={sys_viToggle}
            onPress={() => this.toggle(!sys_viToggle)}
          />
          <View style={styles.quote}>
            <Text>Total:  ${sys_viTimeSlider * vlt_meta_rate}</Text>
          </View>
        </View>
        <ImageBackground source={bgImageNoLogo()} style={styles.bgImage}>
          <Button
            buttonStyle={styles.buttonStyle2}
            title={'Checkout'}
            titleStyle={{color: myColors.control}}
            onPress={()=>this.props.navigation.navigate('Payment')}
          />
        </ImageBackground>
      </View>
     </TouchableWithoutFeedback>
    );//return
  }//render
}//Component


const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'rgb(245, 245, 245)',
    alignItems: 'center',
    //justifyContent: 'space-between',
  },
  bgImage: {
    flex: 1,
    top: 0,
    left: 0,
    width: SCREEN_WIDTH,
    height: SCREEN_HEIGHT,
  },
  subContainer:{
    width: SCREEN_WIDTH * 0.85,
    marginBottom: 30
  },
  sliderContainer:{
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  inputStyle: {
    fontSize: 14,
    backgroundColor: 'white',
    fontStyle: 'italic',
    //borderWidth: 0.5,
    height: 200
  },
  buttonStyle2: {
    width: SCREEN_WIDTH,
    height: 60,
    backgroundColor: myColors.vlt,
  },
  quote: {
    marginTop: 15,
    height: 100,
    justifyContent: 'space-around',
    backgroundColor: 'rgb(233, 233, 233)',

  },

  // scrollView:{
  //   height: 500,
  // }
});

const mapStateToProps = ({ reduxState }) => ({ reduxState });

export default connect(mapStateToProps, { updateSysViTimeSlider, updateSysViToggle, updateSysViExpDate, socketIO })(ViStorage);
