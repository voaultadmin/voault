import React, { Component } from 'react';
import axios from 'axios';
import {
  Alert,
  ImageBackground,
  StyleSheet,
  Dimensions,
  View,
  Keyboard,
  TouchableWithoutFeedback,
  Platform,
  Image,
  WebView,
  Text,
  ScrollView,
} from 'react-native';
import { connect } from 'react-redux';
import ToggleSwitch from 'toggle-switch-react-native';
import { myColors, bgImageNoLogo, dummys, currentKeys, searchKeys, vlts, socket } from '../../constants/constants';
import {
  updateSysViTimeSlider, updateSysViToggle, updateSysSearchValue, updateOrdItemName, updateOrdItemPrice, updateOrdItemSize, updateOrdSecondaryID, socketIO
} from '../../stateStore/actions';
import moment from 'moment';
import Spinner from 'react-native-loading-spinner-overlay';
import { SearchBar, Button, Slider, Input, CheckBox, Card, ListItem } from 'react-native-elements';
const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;


class ViTrade extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerLeft: (
        <Button
          onPress={()=> navigation.navigate('Details')}
          title=""
          color="#fff"
          icon={{name: 'keyboard-arrow-down', color: myColors.vlt, size: 35}}
          buttonStyle={{
            backgroundColor: 'transparent',
          }}
        />
      ),
    };
  }

  constructor(props){
    super(props)

    this.confirmDisabled = false
  }//constructor

  getItemName(event) {
    this.props.updateOrdItemName(event.nativeEvent.text);
  }

  getItemPrice(event) {
    this.props.updateOrdItemPrice(event.nativeEvent.text);
  }

  getItemSize(event) {
    this.props.updateOrdItemSize(event.nativeEvent.text);
  }

  getSecondaryID(event) {
    this.props.updateOrdSecondaryID(event.nativeEvent.text);
  }


  confirmOrder(){

    this.confirmDisabled = true
    fetch('https://pacific-plateau-35190.herokuapp.com/graphql', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          query: `query {
            schema_dev_users(where: {user_name: {_eq: ${JSON.stringify(this.props.reduxState.usr_creds_username)}}}) {
              user_id
            }
          }`
        }),
      })
      .then(response => response.json())
      .then(response => {
        this.primaryID = response.data.schema_dev_users[0].user_id;

        fetch('https://pacific-plateau-35190.herokuapp.com/graphql', {
            method: 'POST',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/json',
            },
            body: JSON.stringify({
              query: `query {
                schema_dev_users(where: {user_name: {_eq: ${JSON.stringify(this.props.reduxState.ord_secondaryID)}}}) {
                  user_id
                }
              }`
            }),
          })
          .then(response => response.json())
          .then(response => {
            this.secondaryID = response.data.schema_dev_users[0].user_id;

            fetch('https://pacific-plateau-35190.herokuapp.com/graphql', {
                method: 'POST',
                headers: {
                  Accept: 'application/json',
                  'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                  query: `mutation {
                      insert_schema_dev_event_vlt_one(object: {
                        item_name: ${JSON.stringify(this.props.reduxState.ord_itemName)},
                        item_price: ${this.props.reduxState.ord_itemPrice},
                        item_size: ${JSON.stringify(this.props.reduxState.ord_itemSize)},
                        pod_id: "a891276e-ba7b-4109-95aa-9b628cae1567",
                        primary_user_id: ${JSON.stringify(this.primaryID)},
                        secondary_user_id: ${JSON.stringify(this.secondaryID)},
                      }) {
                        event_vlt_id
                      }
                    }`
                }),
              })
              .then(response => response.json())
              .then(response => {
                if (response) {
                  this.props.navigation.navigate('Payment')
                  this.confirmDisabled = false
                }
              });

          });
      });

      // axios.post('https://pacific-plateau-35190.herokuapp.com/graphql', {
      //   method: 'POST',
      //   headers: {
      //     Accept: 'application/json',
      //     'Content-Type': 'application/json',
      //   },
      //   body: JSON.stringify({
      //     query: `mutation {
      //         insert_schema_dev_event_vlt_one(object: {
      //           item_name: ${JSON.stringify(this.props.reduxState.ord_itemName)},
      //           item_price: ${this.props.reduxState.ord_itemPrice},
      //           item_size: ${JSON.stringify(this.props.reduxState.ord_itemSize)},
      //           pod_id: "a891276e-ba7b-4109-95aa-9b628cae1567",
      //           primary_user_id: ${JSON.stringify(this.props.reduxState.usr_creds_username)},
      //           secondary_user_id: ${JSON.stringify(this.props.reduxState.ord_secondaryID)}
      //         }) {
      //           event_vlt_id
      //         }
      //       }`
      //   }),
      // })
      // .then((response) => {
      //   console.log(response);
      // })
      // .catch((error) => {
      //   console.log(error);
      // });



      // fetch('https://pacific-plateau-35190.herokuapp.com/graphql', {
      //     method: 'POST',
      //     headers: {
      //       Accept: 'application/json',
      //       'Content-Type': 'application/json',
      //     },
      //     body: JSON.stringify({
      //       query: `mutation {
      //           insert_schema_dev_event_vlt_one(object: {
      //             item_name: ${JSON.stringify(this.props.reduxState.ord_itemName)},
      //             item_price: ${this.props.reduxState.ord_itemPrice},
      //             item_size: ${JSON.stringify(this.props.reduxState.ord_itemSize)},
      //             pod_id: "a891276e-ba7b-4109-95aa-9b628cae1567",
      //             primary_user_id: ${primaryID},
      //             secondary_user_id: "a891276e-ba7b-4109-95aa-9b628cae1567"
      //           }) {
      //             event_vlt_id
      //           }
      //         }`
      //     }),
      //   })
      //   .then(response => response.json())
      //   .then(response => {
      //     console.warn(response);
      //   });


  }



  // addVIReceiver(key){
  //   if (vlts[this.props.reduxState.vlt_meta_index].key1 == dummys.key1) { //add key 1 or key 2
  //     const payload = {
  //       cmd: 'addVIReceiver',
  //       contents: {
  //         username: this.props.reduxState.usr_creds_username,
  //
  //         unit: this.props.reduxState.vlt_meta_unit,
  //         viReceiver: 1,
  //       }
  //     }
  //     this.props.socketIO(payload)
  //   }
  //   this.searching = false
  // }//addVIReceiver

  render() {

    const {
      reduxState: {
        sys_viTimeSlider,
        sys_viToggle,
        sys_searchValue,
        sys_pageLoaded,
        sys_pageLoaded1,
        ord_itemName,
        ord_itemSize,
        ord_itemPrice,
        ord_secondaryID,
      }
    } = this.props;

    Date.prototype.addHours= function(h){
      this.setHours(this.getHours()+h);
      return this;
    }
    this.expirationDate = new Date().addHours(sys_viTimeSlider)

    return (
     <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
       <View style={styles.container}>
        <Card title='New Trade' containerStyle={{backgroundColor: 'transparent', borderWidth: 0}}/>
        <View style={styles.subContainer}>

          {/* <Spinner
             visible={ (!sys_pageLoaded) || (!sys_pageLoaded1) ? (true):(false) }
          /> */}
          <View>
            <Input
              containerStyle={{ width: 300, marginVertical: 10 }}
              onChange={this.getItemName.bind(this)}
              value={ord_itemName}
              inputStyle={{ marginLeft: 10, color: myColors.vlt, fontSize: 16 }}
              //keyboardAppearance="light"
              placeholder="Item Name"
              autoFocus={false}
              autoCapitalize="none"
              autoCorrect={false}
              //keyboardType="email-address"
              returnKeyType="next"
              blurOnSubmit={false}
              placeholderTextColor='black'
            />
            <Input
              containerStyle={{ width: 300, marginVertical: 10 }}
              onChange={this.getItemPrice.bind(this)}
              value={ord_itemPrice}
              inputStyle={{ marginLeft: 10, color: myColors.vlt, fontSize: 16 }}
              //keyboardAppearance="light"
              placeholder="Price"
              autoFocus={false}
              autoCapitalize="none"
              autoCorrect={false}
              //keyboardType="email-address"
              returnKeyType="next"
              blurOnSubmit={false}
              placeholderTextColor='black'
            />
            <View style={{flexDirection: 'row'}}>
              <Input
                containerStyle={{ width: 100, marginVertical: 10 }}
                onChange={this.getItemSize.bind(this)}
                value={ord_itemSize}
                inputStyle={{ marginLeft: 10, color: myColors.vlt, fontSize: 16 }}
                //keyboardAppearance="light"
                placeholder="Size"
                autoFocus={false}
                autoCapitalize="none"
                autoCorrect={false}
                keyboardType="number-pad"
                returnKeyType="next"
                blurOnSubmit={false}
                placeholderTextColor='black'
              />
              {/* <Input
                containerStyle={{ width: 100, marginVertical: 10 }}
                //onChange={this.getEmail.bind(this)}
                //value={usr_creds_email}
                inputStyle={{ marginLeft: 10, color: myColors.vlt, fontSize: 16 }}
                //keyboardAppearance="light"
                placeholder="Width"
                autoFocus={false}
                autoCapitalize="none"
                autoCorrect={false}
                keyboardType="number-pad"
                returnKeyType="next"
                blurOnSubmit={false}
                placeholderTextColor='black'
              />
              <Input
                containerStyle={{ width: 100, marginVertical: 10 }}
                //onChange={this.getEmail.bind(this)}
                //value={usr_creds_email}
                inputStyle={{ marginLeft: 10, color: myColors.vlt, fontSize: 16 }}
                //keyboardAppearance="light"
                placeholder="Height"
                autoFocus={false}
                autoCapitalize="none"
                autoCorrect={false}
                keyboardType="number-pad"
                returnKeyType="next"
                blurOnSubmit={false}
                placeholderTextColor='black'
              /> */}
            </View>
            <Input
              containerStyle={{ width: 300, marginVertical: 10 }}
              onChange={this.getSecondaryID.bind(this)}
              disabled={this.confirmDisabled}
              value={ord_secondaryID}
              inputStyle={{ marginLeft: 10, color: myColors.vlt, fontSize: 16 }}
              //keyboardAppearance="light"
              placeholder="BuyerID"
              autoFocus={false}
              autoCapitalize="none"
              autoCorrect={false}
              //keyboardType="email-address"
              returnKeyType="next"
              blurOnSubmit={false}
              placeholderTextColor='black'
            />
          </View>

        </View>
        <ImageBackground source={bgImageNoLogo()} style={styles.bgImage}>
          <Button
            buttonStyle={styles.buttonStyle2}
            title={'Confirm'}
            titleStyle={{color: myColors.control}}
            onPress={()=> this.confirmOrder()}
          />
        </ImageBackground>
      </View>
     </TouchableWithoutFeedback>
    );//return
  }//render
}//Component


const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'rgb(245, 245, 245)',
    alignItems: 'center',
    //justifyContent: 'space-between',
  },
  bgImage: {
    width: SCREEN_WIDTH,
    height: SCREEN_HEIGHT,
    // justifyContent: 'center',
    // alignItems: 'center',
  },
  subContainer:{
    width: SCREEN_WIDTH * 0.85,
    marginBottom: 30
  },
  resContainer:{
    marginTop: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  inputStyle: {
    fontSize: 14,
    backgroundColor: 'white',
    fontStyle: 'italic',
    //borderWidth: 0.5,
    height: 200
  },
  buttonStyle2: {
    width: SCREEN_WIDTH,
    height: 60,
    backgroundColor: myColors.vlt,
  },
  // scrollView:{
  //   height: 500,
  // }
});

const mapStateToProps = ({ reduxState }) => ({ reduxState });

export default connect(mapStateToProps, { updateSysViTimeSlider, updateSysViToggle, updateSysSearchValue, updateOrdItemName, updateOrdItemPrice, updateOrdItemSize, updateOrdSecondaryID, socketIO })(ViTrade);
