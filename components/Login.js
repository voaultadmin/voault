import React, { Component } from 'react';
import { ScreenOrientation } from 'expo';
import {
  Alert,
  AsyncStorage,
  ImageBackground,
  Text,
  StyleSheet,
  View,
  TextInput,
  Keyboard,
  TouchableWithoutFeedback,
  TouchableHighlight,
  Dimensions,
} from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import { Permissions, Notifications } from 'expo';
import { connect } from 'react-redux';
import { Input, Button, Icon } from 'react-native-elements';
import {
  DotIndicator,
  BallIndicator,
  BarIndicator,
  MaterialIndicator,
} from 'react-native-indicators';
import { updateSysConnectStatus, updateSysActiveTab, updateUsrUsername, updateUsrFullName,
  updateUsrPassword, updateUsrEmail, updateSysPageLoaded, updateSysWaitSocket, updateSysEditContent, updateSysActivityIndicator, socketIO } from '../stateStore/actions';
import { socket, myColors, bgImage, bgImageNoLogo, dummys} from '../constants/constants';
import daynight from 'daynight';

const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;

//const PUSH_ENDPOINT = 'https://voaultserver.herokuapp.com/tokens'

const PUSH_ENDPOINT = 'http://65f6d80f.ngrok.io/token'
const MESSAGE_ENDPOINT = 'http://65f6d80f.ngrok.io/message'

class Login extends Component {

  // registerForPushNotificationsAsync = async () => {
  //
  //   const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS)
  //
  //   if (status !== 'granted') {
  //     //console.warn('Notification perm');
  //     return;
  //   }
  //
  //   let token  = await Notifications.getExpoPushTokenAsync()
  //   console.log(token);
  //
  //   this.notificationSubscription = Notifications.addListener(this.handleNotification)
  //
  //   return fetch(PUSH_ENDPOINT, {
  //     method: 'POST',
  //     headers: {
  //       'Accept': 'application/json',
  //       'Content-Type': 'application/json',
  //     },
  //     body: JSON.stringify({
  //       token: {
  //         value: token,
  //       },
  //       user: {
  //         username: this.props.reduxState.usr_creds_username,
  //       },
  //     }),
  //   });
  //
  // }//registerForPushNotificationsAsync

  handleNotification = (notification) => {
    this.notification = notification
    console.warn(this.notification);
  }

  sendMessage = async() => {
    fetch(MESSAGE_ENDPOINT, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        message: 'testing',
      })
    })
  }

  constructor(props){
    super(props)

    // setInterval(() => {
    //   this.cloudWatchdog()
    // }, 10000);
  }//constructor

  cloudWatchdog(){
    this.cloudTmr = setTimeout(() => {
      switch (this.props.reduxState.sys_connectStatus.cloud) {
        case 'init':
          this.props.updateSysConnectStatus({cloud: false, vlt: false})
          //Alert.alert('Please check your connection...')
          break;
        case false:
          this.props.updateSysConnectStatus({cloud: 'hold', vlt: false})
          //Alert.alert('Could not connect to server')
          break;
        case true:
          this.props.updateSysConnectStatus({cloud: 'hold', vlt: false})
          //Alert.alert('Connection timeout. Please check your connection')
          break;
      }
    }, 10000);
  }//cloudWatchdog

  // async componentDidMount(){
  //   await ScreenOrientation.lockAsync(ScreenOrientation.Orientation.PORTRAIT);
  // }

  componentWillMount() {
    //this.registerForPushNotificationsAsync()
    this.props.updateSysConnectStatus({cloud: true, vlt: this.props.reduxState.sys_connectStatus.vlt})
    this.btnColor = myColors.vlt;

    socket.on('connect', () => {
      setTimeout(() => {
        this.getUserToken()
      }, 250);
    })

    socket.on('disconnect', () => {
      this.props.updateSysWaitSocket(true)
    })

    // socket.on('cloudWatchdog', () => {
    //   this.props.updateSysConnectStatus({cloud: true, vlt: this.props.reduxState.sys_connectStatus.vlt})
    //   clearTimeout(this.cloudTmr)
    // })

    socket.on('validLogin', (resp, username, socketID, fullName, comingFrom, email, confirmedEmail, agreedTerms) => {
      if ((this.props.reduxState.usr_creds_username == username) && (socketID == socket.id)) {
        if (resp == true) {
          setTimeout(() => {
            this.props.updateUsrFullName(fullName)
            this.props.updateUsrEmail(email)
            if (comingFrom !== 'fromGetToken') {
              AsyncStorage.setItem('username', username)
              AsyncStorage.setItem('email', email)
              AsyncStorage.setItem('token', socketID)
            }
              this.props.updateSysActivityIndicator(false)
              this.props.navigation.navigate('Menu')
          }, 500);

        } else if (resp == false) {
          this.props.updateSysActivityIndicator(false)
          this.btnColor = myColors.vlt
          this.props.updateSysWaitSocket(false)
          this.props.updateSysPageLoaded(true)
          if (comingFrom !== 'fromGetToken') {
            if (confirmedEmail == false) {
              Alert.alert(
                'Please confirm email' ,
                '',
                [
                  {text: 'Resend', onPress: () => {
                      this.props.updateSysWaitSocket('true')
                      setTimeout(() => {
                        const payload = {
                          cmd: 'email',
                          contents: {
                            emailSubject: 'Voault Registration',
                            emailTarget: this.props.reduxState.usr_creds_email,
                            emailType: 'emailConfirmation',
                            username: this.props.reduxState.usr_creds_username,
                          },
                        }
                        this.props.socketIO(payload)
                      }, 250);
                    }
                  },
                  {text: 'Help', onPress: () => {
                    this.props.navigation.navigate('Help')
                    }
                  },
                  {text: 'Close', style: 'cancel'},
                ]
              );
            }else {
              setTimeout(() => {
                Alert.alert('Invalid login')
              }, 500);
            }
          }
        }
      }
    })//validLogin

    socket.on('emailSent', (result, socketID, emailTarget) => {
      if (socket.id == socketID) {
        if (result == true) {
          Alert.alert(' Email sent to ' + emailTarget) //Generic for help, confirmation email
          this.props.updateSysEditContent('')
        } else {
          Alert.alert('Sorry we are having trouble logging your request. Please try again')
        }
      }
    })//socket emailSent

    socket.on('validEditPassword', (result, socketID, email) => {
      if (email == this.props.reduxState.usr_creds_email) {
        switch (result) {
          case true:
            Alert.alert('Password updated')
            setTimeout(() => {
              this.props.navigation.navigate('Login')
            }, 500);
            break;
          case false:
            Alert.alert('Please follow link sent to your email to proceed')
            break;
          default:
        }
      }
    })//validEditPassword
  }//componentWillMount

  getUsername(event) {
    if (this.props.reduxState.usr_creds_username == '') {
      this.props.updateUsrUsername('#');
      this.edit = '#' + event.nativeEvent.text
      setTimeout(() => {
        this.props.updateUsrUsername(this.edit);
      }, 50);
    }else {
      this.props.updateUsrUsername(event.nativeEvent.text);
    }
  }//getUsername

  getPassword(event) {
    const newpass = event.nativeEvent.text;
    this.props.updateUsrPassword(newpass);
  }

  login() {
    this.sendMessage()/////REMOVE
    if ((this.props.reduxState.usr_creds_username.length > 5) && (this.props.reduxState.usr_creds_password.length > 5)) {
      const payload = {
        cmd: 'login',
        contents: {
          username: this.props.reduxState.usr_creds_username,
          password: this.props.reduxState.usr_creds_password,
        },
      }
      this.props.socketIO(payload)
      this.props.updateSysActivityIndicator(true)
      this.btnColor = 'gray'
      setTimeout(() => {
        this.props.updateSysActivityIndicator(false)
        this.btnColor = myColors.vlt
        this.props.updateSysWaitSocket(false)
      }, 2300);
      this.props.updateSysWaitSocket(true)
    } else {
      Alert.alert('Invalid Crendials')
    }

  }//login

  async getUserToken(){
  try{
    let token_username = await AsyncStorage.getItem('username')
    let token_email = await AsyncStorage.getItem('email')
    let token_id = await AsyncStorage.getItem('token')

    if (token_username != null) {
      this.props.updateUsrUsername(token_username)
      this.props.updateUsrEmail(token_email)

      const payload = {
        cmd: 'getToken',
        contents: {
          username: token_username,
          token: token_id,
        },
      }
      this.props.socketIO(payload)
    }
    else {
      this.props.updateSysActiveTab('all')
      setTimeout(() => {
        this.props.updateSysPageLoaded(true)
      }, 1500);
    }
  }catch (error){
    Alert.alert('Error. Please try again')
  }
}//getUserToken


  render() {
    const {
      reduxState: {
        usr_creds_email,
        usr_creds_username,
        usr_creds_password,
        sys_connectStatus,
        sys_activeTab,
        sys_activityIndicator,
        sys_waitSocket,
        sys_pageLoaded,
        sys_pageLoaded1
      }
    } = this.props;

    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
       <View style={styles.container} onPress={Keyboard.dismiss}>
           {(sys_pageLoaded == true ) && (sys_connectStatus.cloud == true) ? (
             <ImageBackground source={bgImage()} style={styles.bgImage}>
               <View style={styles.loginView} onPress={Keyboard.dismiss}>
                 <View style={styles.loginInput}>
                   <Spinner
                      visible={sys_activityIndicator}
                    />
                   <Input
                     leftIcon={
                       <Icon
                         name="person"
                         color={myColors.vlt}
                         size={25}
                       />
                     }
                     containerStyle={{ width: 300 }}
                     onChange={this.getUsername.bind(this)}
                     value={usr_creds_username}
                     inputStyle={{ marginLeft: 10, color: myColors.vlt, fontSize: dummys.fontSize, fontWeight: 'bold' }}
                     keyboardAppearance="light"
                     placeholder="Username"
                     autoFocus={false}
                     autoCapitalize="none"
                     autoCorrect={false}
                     keyboardType="default"
                     returnKeyType="next"
                     //ref={input => (this.emailInput = input)}
                     onSubmitEditing={() => {
                       // this.setState({ email_valid: this.validateEmail(email) });
                       // this.passwordInput.focus();
                     }}
                     blurOnSubmit={false}
                     errorStyle={{ textAlign: 'center', fontSize: dummys.fontSize, }}
                     errorMessage={
                       sys_pageLoaded ? null : 'Please enter a valid email address'
                     }
                     placeholderTextColor='white'
                   />
                   <Input
                     leftIcon={
                       <Icon
                         name="lock"
                         color={myColors.vlt}
                         size={25}
                       />
                     }
                     containerStyle={{ width: 300, marginVertical: 10, marginBottom: 30 }}
                     onChange={this.getPassword.bind(this)}
                     value={usr_creds_password}
                     inputStyle={{ marginLeft: 10, color: myColors.vlt, fontSize: dummys.fontSize, fontWeight: 'bold' }}
                     secureTextEntry={true}
                     keyboardAppearance="light"
                     placeholder="Password"
                     autoCapitalize="none"
                     autoCorrect={false}
                     keyboardType="default"
                     returnKeyType="done"
                     //ref={input => (this.passwordInput = input)}
                     blurOnSubmit={true}
                     placeholderTextColor='white'
                   />

                 <Button
                   raised
                   title="Login"
                   //activeOpacity={1}
                   //underlayColor="transparent"
                   onPress={()=> this.login()}
                   //loading={showLoading}
                   loadingProps={{ size: 'small', color: 'white' }}
                   //disabled={!email_valid && password.length < 8}
                   buttonStyle={{
                     height: 50,
                     width: 250,
                     backgroundColor: this.btnColor,
                     borderWidth: 2,
                     borderColor: myColors.action,
                     borderRadius: 30,
                   }}
                   containerStyle={{ marginVertical: 10 }}
                   titleStyle={{ fontWeight: 'bold', color: 'white' }}
                 />
                 </View>
                 <View style={styles.footerView}>
                   {/* <Text style={{ color: 'grey' }}>Forgot Login?</Text> */}
                   <Text style={{ color: myColors.vlt, fontSize: 15, fontWeight:'bold', textDecorationLine:'underline', height: 30 }}
                          onPress={() => {
                            this.props.navigation.navigate('RecoverCreds')
                            // this.props.updateUsrFullName('')
                            // this.props.updateUsrEmail('')
                            // this.props.updateUsrUsername('')
                            // this.props.updateUsrPassword('')
                          }}> Recover login
                    </Text>

                   <Text style={{ color: 'grey', marginTop: 20 }}>Don't have an account yet?</Text>
                   <Text style={{ color: myColors.vlt, fontSize: 15, fontWeight:'bold', textDecorationLine:'underline', marginTop: 5, height: 30 }}
                          onPress={() => {
                            this.props.navigation.navigate('Signup')
                            // this.props.updateUsrFullName('')
                            // this.props.updateUsrEmail('')
                            // this.props.updateUsrUsername('')
                            // this.props.updateUsrPassword('')
                          }}> Sign Up here
                    </Text>
                 </View>
               </View>
            </ImageBackground>
           ) : (
              <ImageBackground source={bgImageNoLogo()} style={styles.bgImage}/>
           )}
       </View>
     </TouchableWithoutFeedback>
     );
   }//render
 }

 const styles = StyleSheet.create({
   container: {
     flex: 1,
   },
   bgImage: {
     width: SCREEN_WIDTH,
     height: SCREEN_HEIGHT,
     justifyContent: 'center',
     alignItems: 'center',
   },
   loginView: {
     backgroundColor: 'transparent',
     width: 250,
     height: 400,
   },
   loginTitle: {
     flex: 1,
     justifyContent: 'center',
     alignItems: 'center',
   },
   travelText: {
     color: 'white',
     fontSize: 30,
   },
   plusText: {
     color: 'white',
     fontSize: 30,
   },
   loginInput: {
     flex: 1,
     justifyContent: 'center',
     alignItems: 'center',
   },
   footerView: {
     flex: 0.5,
     justifyContent: 'center',
     alignItems: 'center',
   },
 });

const mapStateToProps = ({ reduxState }) => ({ reduxState });

export default connect(mapStateToProps, { updateSysConnectStatus, updateSysActiveTab, updateUsrEmail, updateUsrUsername,
  updateUsrFullName, updateUsrPassword, updateSysPageLoaded, updateSysWaitSocket, updateSysEditContent, updateSysActivityIndicator, socketIO })(Login);
