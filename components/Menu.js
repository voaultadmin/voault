import React, { Component } from 'react';
import Expo from "expo";
import {
  Alert,
  ActivityIndicator,
  ImageBackground,
  StyleSheet,
  View,
  ScrollView,
  Text,
  TextInput,
  Keyboard,
  TouchableWithoutFeedback,
  TouchableHighlight,
  Platform,
  Image,
  WebView,
  Dimensions,
  BackHandler
} from 'react-native';
import { connect } from 'react-redux';
import BottomNavigation, {
  FullTab
} from 'react-native-material-bottom-navigation';
import TouchableScale from 'react-native-touchable-scale';
import {
  DotIndicator,
  BallIndicator,
  BarIndicator,
  MaterialIndicator,
} from 'react-native-indicators';
import moment from 'moment';
import { Avatar, Card, List, ListItem, Button, Icon } from 'react-native-elements';
import { vlts, tabs, tabsMeta, myColors, socket, bgImageNoLogo } from '../constants/constants';

import {
  updateVltTag,
  updateVltUnit,
  updateVltAddress,
  updateVltRate,
  updateVltSize,
  updateVltLocation,
  updateVltZip,
  updateVltIndex,
  updateVltMasterKey,
  updateVltKey1,
  updateVltKey2,
  updateVltReserved,
  updateVltDirections,

  updateVltDoor,

  updateUsrPassword,
  updateUsrLatitude,
  updateUsrLongitude,

  updateSysConnectStatus,
  updateSysPageLoaded,
  updateSysPageLoaded1,
  updateSysWaitSocket,
  updateSysActiveTab,
  updateSysActivityIndicator,

  socketIO,
 } from '../stateStore/actions';

const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;


class Menu extends Component {
  static navigationOptions = ({ navigation, router }) => {
    return {
      headerLeft: (
        <Button
          onPress={()=> {
            navigation.navigate('Account')
          }}
          title=""
          color="#fff"
          icon={{name: 'account-circle', color: myColors.vlt, size: 30}}
          buttonStyle={{
            backgroundColor: 'transparent',
          }}
        />
      ),
      headerRight: (
        <Button
          onPress={()=> {
            navigation.navigate('Instructions')
          }}
          title=""
          color="#fff"
          icon={{name: 'info-outline', color: myColors.vlt, size: 30}}
          buttonStyle={{
            backgroundColor: 'transparent',
          }}
        />
      ),
    };
  }

  componentWillMount(){
    this.props.updateUsrPassword('')
    this.props.updateSysActivityIndicator(false)
    this.pullStatus()

    socket.on('cloudWatchdog', () => {
      switch (this.props.reduxState.sys_connectStatus.cloud) {
        case false:
          this.props.updateSysConnectStatus({cloud: true, vlt: this.props.reduxState.sys_connectStatus.vlt})
          break;
      }
      clearTimeout(this.cloudTmr)
    })//cloudWatchdog

    socket.on('vltDetails', (result, username, interval, socketID) => {
      if ((username = this.props.reduxState.usr_creds_username) && (socket.id == socketID)) {
        this.newVltsList(result, username, interval)
      }
    })//vltDetails

    socket.on('reserved', (username)=> {
      if (username == this.props.reduxState.usr_creds_username) {
        this.props.updateVltMasterKey(username)
        this.props.updateSysPageLoaded1(false)
        this.props.navigation.navigate('Details')

        setTimeout(() => {
          this.payload = {
            cmd: 'vltDetails',
            contents: {
              filter: this.props.reduxState.sys_activeTab,
              username: this.props.reduxState.usr_creds_username,
              interval: false
            }
          }
          this.props.socketIO(this.payload)
        }, 250);
      }
    })//socket reserved


    socket.on('released', (username, unit)=> {
      if (username == this.props.reduxState.usr_creds_username) {
        this.props.updateSysPageLoaded1(false)
        setTimeout(() => {
          this.payload = {
            cmd: 'vltDetails',
            contents: {
              filter: this.props.reduxState.sys_activeTab,
              username: this.props.reduxState.usr_creds_username,
              interval: false
            }
          }
          this.props.socketIO(this.payload)
        }, 250);
          this.props.navigation.navigate('Menu')
      }
      this.keyRemoved(username, unit)
    })//socket released

    socket.on('removedKeys', (username, unit) => {
      this.keyRemoved(username, unit)
    })//socket removedKeys

    socket.on('swappedKeys', (username, unit) => {
      if ((username == this.props.reduxState.usr_creds_username) && (unit == this.props.reduxState.vlt_meta_unit)) {
        this.removedKeyAction(username, unit)
      }
    })//socket removedKeys
  }//componentWillMount

  componentDidMount() {
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
  }

  componentWillUnmount() {
    clearInterval(this.vltDetailsIntervalTmr);
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }

  handleBackButton() {
    return true;
  }

  // constructor(props){
  //   super(props)
  // }//constructor

  cloudWD(){
    this.cloudTmr = setTimeout(() => {
      switch (this.props.reduxState.sys_connectStatus.cloud) {
        case 'init':
          this.props.updateSysConnectStatus({cloud: false, vlt: false})
          this.props.updateSysPageLoaded(false)
          break;
        case false:
          this.props.updateSysConnectStatus({cloud: 'hold', vlt: false})
          this.props.updateSysPageLoaded(false)
          break;
        case true:
          this.props.updateSysConnectStatus({cloud: 'hold', vlt: false})
          this.props.updateSysPageLoaded(false)
          break;
      }
    }, 10000);
  }

  pullStatus(){

    this.date = new Date()
    setTimeout(() => {
      this.payload = {
        cmd: 'vltDetails',
        contents: {
          filter: this.props.reduxState.sys_activeTab,
          username: this.props.reduxState.usr_creds_username,
          interval: false
        }
      }
      this.props.socketIO(this.payload)
    }, 250);

    this.vltDetailsIntervalTmr = setInterval(() => {
      const date = new Date()
      const payload = {
        cmd: 'vltDetails',
        contents: {
          filter: this.props.reduxState.sys_activeTab,
          username: this.props.reduxState.usr_creds_username,
        }
      }
      this.props.socketIO(payload)
      this.cloudWD()
    }, 5000);

  }//pullStatus

  newVltsList(result, username, interval){

    if (vlts.length !== result.length) {
      diff = result.length - vlts.length
      if (diff > 0) {
        for (var k = 0; k < diff; k++) {
          var obj = {}
          vlts.unshift(obj)
        }
      }else {
        for (var k = 0; k > diff; k--) {
          vlts.splice(0,1)
        }
      }

      if(this.props.reduxState.sys_activeTab == 'pickup'){
        this.props.navigation.navigate('Menu')
      }
    }

    for (var i = 0; i < result.length; i++) {
      vlts[i].unit = result[i].unit
      vlts[i].tag = result[i].tag
      vlts[i].title = result[i].location
      vlts[i].area = result[i].area
      vlts[i].address = result[i].address
      vlts[i].size = result[i].size
      vlts[i].rate = result[i].rate
      vlts[i].zip = result[i].zip
      vlts[i].longitude = result[i].longitude
      vlts[i].latitude = result[i].latitude

      vlts[i].reserved = result[i].reserved
      vlts[i].adminKey = result[i].adminKey
      vlts[i].masterKey = result[i].masterKey
      vlts[i].key1 = result[i].key1
      vlts[i].key2 = result[i].key2
      vlts[i].isOpen = result[i].isOpen
      vlts[i].dateReserved = result[i].dateReserved

      vlts[i].avatar_url = result[i].avatar_url
      vlts[i].image_url = result[i].image_url
      vlts[i].video_url = result[i].video_url
      vlts[i].cam_url = result[i].cam_url
      vlts[i].directions = result[i].directions
      //vlts[i].remainingTime = result[i].remainingTime

      vlts[i].door = result[i].door

      if (result[i].reserved) {
        switch (this.props.reduxState.usr_creds_username) {
          case result[i].masterKey:
            vlts[i].iconRightColor = myColors.control1
            vlts[i].iconRight = 'lock'

            var now = moment(new Date())
            var dateReserved = moment(result[i].dateReserved)
            var duration = moment.duration(now.diff(dateReserved));
            var reservedTime = duration.asHours();

            vlts[i].backgroundColor = myColors.vlt
            break;
          case result[i].key1:
            vlts[i].iconRightColor = 'white'
            vlts[i].iconRight = 'lock'
            vlts[i].backgroundColor = myColors.vlt
            break;
          case result[i].key2:
            vlts[i].iconRightColor = 'white'
            vlts[i].iconRight = 'lock'
            vlts[i].backgroundColor = myColors.vlt
            break;
          default:
            vlts[i].iconRightColor = myColors.vlt
            vlts[i].iconRight = 'lock'
            vlts[i].backgroundColor = myColors.vlt
        }
      } else {
        vlts[i].iconRightColor = 'white'
        vlts[i].iconRight = 'lock-open'
        vlts[i].backgroundColor = myColors.vlt
      }

    }//for

    if (interval == false) {
      this.props.updateSysWaitSocket(true)
      setTimeout(()=> {
          this.props.updateSysWaitSocket(false)
          this.props.updateSysPageLoaded(true)
          this.props.updateSysPageLoaded1(true)
      }, 500);
    } else {
          this.props.updateSysPageLoaded(true)
          this.props.updateSysWaitSocket(false)
    }
    this.props.updateSysConnectStatus({cloud: true, vlt: this.props.reduxState.sys_connectStatus.vlt})
    this.alerted = true
  }//newVltsList


  goTo(unit, zip, i){
    this.props.updateVltIndex(i)
    this.props.updateVltMasterKey(vlts[i].masterKey)
    this.props.updateVltKey1(vlts[i].key1)
    this.props.updateVltKey2(vlts[i].key2)
    this.props.updateVltReserved(vlts[i].reserved)
    this.props.updateVltZip(vlts[i].zip)
    this.props.updateVltUnit(vlts[i].unit)
    this.props.updateVltTag(vlts[i].tag)
    this.props.updateVltRate(vlts[i].rate)
    this.props.updateVltDoor(vlts[i].door)

    const directions = { map: vlts[i].directions,  guide: vlts[i].video_url }
    this.props.updateVltDirections(directions)

    this.props.navigation.navigate('Details')
  }//goTo

  keyRemoved(username, unit){
    if ((this.props.reduxState.usr_creds_username == this.props.reduxState.vlt_creds_key1)
    || (this.props.reduxState.usr_creds_username == this.props.reduxState.vlt_creds_key2)
    ) {
        if (unit == this.props.reduxState.vlt_meta_unit) {
          this.removedKeyAction(username, unit)
        }
    }
  }

  removedKeyAction(username, unit){
      this.props.updateSysActiveTab('all')
      this.props.updateSysPageLoaded1(false)
      setTimeout(() => {
        this.payload = {
          cmd: 'vltDetails',
          contents: {
            filter: this.props.reduxState.sys_activeTab,
            username: this.props.reduxState.usr_creds_username,
            interval: false
          }
        }
        this.props.socketIO(this.payload)
        this.props.updateVltUnit('')
      }, 750);
      this.props.navigation.navigate('Menu')
      Alert.alert(username + ' removed your Key to ' + unit)
  }

  renderIcon = icon => ({ isActive }) => (
    <Icon size={tabsMeta.iconSize} color={tabsMeta.iconColor} name={icon} />
  )

  renderTab = ({ tab, isActive }) => (
    <FullTab
      isActive={isActive}
      key={tab.key}
      label={tab.label}
      labelStyle={{ color: myColors.vlt }}
      renderIcon={this.renderIcon(tab.icon)}
    />
  )

  render() {
    //console.warn('rendermenu');
    const {
      reduxState: {
        usr_creds_username,
        sys_pageLoaded,
        sys_activeTab,
        sys_waitSocket,
        sys_connectStatus
      }
    } = this.props;


    return (
     <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
      <View style={styles.container}>
        <ImageBackground source={bgImageNoLogo()} style={styles.bgImage}>
         {(sys_pageLoaded == true) && (sys_waitSocket == false) && (sys_connectStatus.cloud == true)? (
            <ScrollView style={{ flex: 8, }} refreshing={true}>
              {vlts.length !== 0 ? (

              vlts.map((l, i) => (
                <ListItem
                  Component={TouchableScale}
                  containerStyle={{
                    //marginTop: 1,
                    backgroundColor: l.backgroundColor,
                    borderColor: 'white',
                  }}
                  onPress={() => this.goTo(l.unit, l.zip, i)}
                  friction={90}
                  tension={100}
                  activeScale={0.95}
                  // linearGradientProps={{
                  //   //colors: ['#FF9800', #F44336],
                  //   colors: ['black',myColors.vlt,
                  //   start: [1, 0],
                  //   end: [0.2, 0],
                  // }}
                  key={i}
                  //leftIcon={{name: l.iconLeft, color: l.iconLeftColor}}
                  //onPressLeftIcon={() => this.props.navigation.navigate('Details')}
                  rightIcon={{name: l.iconRight, color: l.iconRightColor}}
                  //onPressRightIcon={() => this.goTo()}
                  leftAvatar={{ source: { uri: l.avatar_url } }}
                  title={l.unit}
                  titleStyle={{color: 'white', fontSize: 16}}
                  subtitle={l.title + ' - ' + l.area}
                  subtitleStyle={{color: 'white', fontSize: 13}}
                  //badge={{ value: '$' + l.rate + '-' + l.size , textStyle: { color: 'white'}, badgeStyle: { backgroundColor: myColors.vlt } }}
                />
              ))

          ):(
            <View style={{ flex: 8, alignItems: 'center', justifyContent: 'center'}}>
                <Text style={styles.txt}>No pickups at this time</Text>
            </View>
          )
        }
          </ScrollView>
           ) : (
             <View style={{ flex: 8, alignItems: 'center', justifyContent: 'center'}}>
                 {/* <MaterialIndicator color='white' size={40} /> */}
                 <Text></Text>
             </View>
           )
         }
         <BottomNavigation
            onTabPress={newTab => {

              if (newTab.key == "VI") {
                this.props.navigation.navigate('VIntent')
                return;
              }

              this.date = new Date()
              this.props.updateSysActiveTab(newTab.key)
              setTimeout(() => {
                this.payload = {
                  cmd: 'vltDetails',
                  contents: {
                    filter: this.props.reduxState.sys_activeTab,
                    username: this.props.reduxState.usr_creds_username,
                    interval: false
                  }
                }
                this.props.socketIO(this.payload)
              }, 500);


            }}
            renderTab={this.renderTab}
            tabs={tabs}
          />
      </ImageBackground>
      </View>
     </TouchableWithoutFeedback>
    );//return
  }//render
}//Component


const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
    backgroundColor: 'white',
  },
  bgImage: {
    flex: 1,
    top: 0,
    left: 0,
    width: SCREEN_WIDTH,
    height: SCREEN_HEIGHT,
  },
  imgStyle: {
    height: 100,
    width: 100,
  },
  btn: {

    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'rgb(185, 185, 185)',

  },
  txt: {
    color: 'white',
    fontSize: 14,
    marginTop: 30
  },
  txtInput: {
    width: 175,
    height: 40,
    fontSize: 12,
    fontStyle: 'italic',
    fontWeight: 'bold',
    borderColor: 'white',
    borderWidth: 0.1,
    borderRadius: 15,
    color: 'white',
    textAlign: 'center',
    justifyContent: 'center',
    marginTop: 12,
  }
});

const mapStateToProps = ({ reduxState }) => ({ reduxState });

export default connect( mapStateToProps, {
  updateVltTag,
  updateVltUnit,
  updateVltAddress,
  updateVltRate,
  updateVltSize,
  updateVltLocation,
  updateVltZip,
  updateVltIndex,
  updateVltMasterKey,
  updateVltKey1,
  updateVltKey2,
  updateVltReserved,
  updateVltDirections,

  updateVltDoor,

  updateUsrPassword,
  updateUsrLatitude,
  updateUsrLongitude,

  updateSysConnectStatus,
  updateSysPageLoaded,
  updateSysPageLoaded1,
  updateSysWaitSocket,
  updateSysActiveTab,
  updateSysActivityIndicator,

  socketIO,
})(Menu);
