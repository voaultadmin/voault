import React, { Component } from 'react';
import {
  Alert,
  ActivityIndicator,
  ImageBackground,
  Text,
  StyleSheet,
  View,
  TextInput,
  Keyboard,
  TouchableWithoutFeedback,
  TouchableHighlight,
  Dimensions,
} from 'react-native';
import { connect } from 'react-redux';
import { Input, Button, Icon } from 'react-native-elements';
import {
  DotIndicator,
  BallIndicator,
  BarIndicator,
  MaterialIndicator,
} from 'react-native-indicators';
import { updateUsrFullName, updateUsrUsername, updateUsrPassword, updateUsrEmail, updateSysPageLoaded, updateSysWaitSocket, updateSysActiveTab, updateSysEditTarget, socketIO } from '../stateStore/actions';
import { myColors, usrPassCheck, usrPassCheck1, usrPassCheck2, emailCheck, socket, bgImage } from '../constants/constants';

const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;

class RecoverCreds extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerLeft: (
        <Button
          onPress={()=> navigation.goBack()}
          title=""
          color="#fff"
          icon={{name: 'chevron-left', color: myColors.vlt, size: 35}}
          buttonStyle={{
            backgroundColor: 'transparent',
          }}
        />
      ),
    };
  }

  constructor(props){
    super(props)

    const date = new Date()
    const payload = {
      cmd: 'vltDetails',
      contents: {
        filter: this.props.reduxState.sys_activeTab,
        username: this.props.reduxState.usr_creds_username,
        lastSeen: date
      }
    }
    this.props.socketIO(payload)
  }//constructor

  componentWillMount() {

    this.animatingIndicator = false
    this.btnColor = myColors.vlt
    this.emailSent = false


    socket.on('emailRecoverCreds', (result, socketID, emailTarget) => {
      if (socket.id == socketID) {
        if (this.emailSent == false) {

          if (result == true) {
            Alert.alert('Email sent to ' + emailTarget)
            this.emailSent = true
          } else {
            Alert.alert('Email not found')
          }

        }else {

          Alert.alert(
            'Please follow link sent to your email',
            '',
            [
              {text: 'Help', onPress: () => {
                this.props.navigation.navigate('Help')
                }
              },
              {text: 'Close', style: 'cancel'},
            ]
          );

        }
      }
    })//socket emailSent

    socket.on('confirmedRecoverCreds', (result, emailTarget, socketID) => {
      if ((emailTarget == this.props.reduxState.usr_creds_email) && (socket.id == socketID)) {
        if (result == true) {
          setTimeout(() => {
            this.props.updateSysEditTarget('Password')
            this.props.navigation.navigate('Edit')
          }, 1500);
        } else {
          Alert.alert('Sorry we are having trouble logging your request. Please try again')
        }
      }
    })//socket emailSent

  }

  getEmail(event) {
    const newemail = event.nativeEvent.text;
    this.props.updateUsrEmail(newemail);
  }

  emailCheck(){
    if (emailCheck.validate(this.props.reduxState.usr_creds_email)) {
      this.animatingIndicator = true
      this.btnColor = 'gray'
      setTimeout(() => {
        this.animatingIndicator = false
        this.btnColor = myColors.vlt
        this.props.updateSysWaitSocket(false)
      }, 5000);
      this.props.updateSysWaitSocket(true)
      const payload = {
        cmd: 'recoverCreds',
        contents: {
          email: this.props.reduxState.usr_creds_email,
        },
      }
      this.props.socketIO(payload)
    }else {
      Alert.alert('Please enter valid Email')
    }
  }

  render() {
    this.signupColor = '#03d0d0';
    const {
      reduxState: {
        usr_creds_username,
        usr_creds_password,
        usr_creds_fullName,
        usr_creds_email,
        sys_pageLoaded,
        sys_connectStatus
      }
    } = this.props;

    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
       <View style={styles.container} onPress={Keyboard.dismiss}>
         <ImageBackground source={bgImage()} style={styles.bgImage}>
           {(sys_pageLoaded == true ) && (sys_connectStatus.cloud == true) ? (
             <View style={styles.SignupView} onPress={Keyboard.dismiss}>
               <View style={styles.Signup} onPress={Keyboard.dismiss}>
                 {this.animatingIndicator ? (<DotIndicator color='white' size={4}/>):(<DotIndicator color='white' size={0}/>)}
                 {/* <Text style={{ color: 'grey', marginBottom: 20, marginTop: 250 }}> Enter email associated with your account</Text> */}
                 <Input
                   leftIcon={
                     <Icon
                       name="email"
                       color={myColors.vlt}
                       size={25}
                     />
                   }
                   containerStyle={{ width: 300, marginVertical: 10, marginTop: 75 }}
                   onChange={this.getEmail.bind(this)}
                   value={usr_creds_email}
                   inputStyle={{ marginLeft: 10, color: myColors.vlt, fontSize: 16, fontWeight: 'bold' }}
                   keyboardAppearance="light"
                   placeholder="Email"
                   autoFocus={false}
                   autoCapitalize="none"
                   autoCorrect={false}
                   //keyboardType="email-address"
                   returnKeyType="next"
                   //ref={input => (this.emailInput = input)}
                   onSubmitEditing={() => {
                     // this.setState({ email_valid: this.validateEmail(email) });
                     // this.passwordInput.focus();
                   }}
                   blurOnSubmit={false}
                   placeholderTextColor="white"
                   errorStyle={{ textAlign: 'center', fontSize: 12 }}
                   errorMessage={
                     sys_pageLoaded ? null : 'Please enter a valid email address'
                   }
                 />
               </View>
               <Button
                 raised
                 title="Recover"
                 //icon={{name: 'add', color: 'white'}}
                 activeOpacity={1}
                 //underlayColor="transparent"
                 onPress={()=> this.emailCheck()}
                 //loading={showLoading}
                 loadingProps={{ size: 'small', color: 'white' }}
                 //disabled={!email_valid && password.length < 8}
                 buttonStyle={{
                   height: 50,
                   width: 250,
                   backgroundColor: this.btnColor,
                   borderWidth: 2,
                   borderColor: myColors.control,
                   borderRadius: 30,
                 }}
                 containerStyle={{ marginTop: 40 }}
                 titleStyle={{ fontWeight: 'bold', color: 'white' }}
               />
             </View>
           ) : (
             <Text>Loading...</Text>
           )}
         </ImageBackground>
       </View>
     </TouchableWithoutFeedback>
     );
   }
 }

 const styles = StyleSheet.create({
   container: {
     flex: 1,
   },
   bgImage: {
     flex: 1,
     top: 0,
     left: 0,
     width: SCREEN_WIDTH,
     height: SCREEN_HEIGHT,
     justifyContent: 'center',
     alignItems: 'center',
   },
   SignupView: {
     flex: 1,
     backgroundColor: 'transparent',
     width: 250,
     height: 400,
   },
   Signup: {
     marginTop: 250,
     justifyContent: 'center',
     alignItems: 'center',
   },
 });

const mapStateToProps = ({ reduxState }) => ({ reduxState });

export default connect(mapStateToProps, { updateUsrFullName, updateUsrUsername, updateUsrPassword, updateUsrEmail, updateSysPageLoaded, updateSysWaitSocket, updateSysActiveTab, updateSysEditTarget, socketIO })(RecoverCreds);
