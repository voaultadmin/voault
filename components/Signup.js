import React, { Component } from 'react';
import {
  Alert,
  ActivityIndicator,
  ImageBackground,
  Text,
  StyleSheet,
  View,
  TextInput,
  Keyboard,
  TouchableWithoutFeedback,
  TouchableHighlight,
  Dimensions,
} from 'react-native';
import { connect } from 'react-redux';
import { Input, Button, Icon } from 'react-native-elements';
import { updateUsrFullName, updateUsrUsername, updateUsrPassword, updateUsrEmail, updateUsrPhoneNumber,
  updateSysPageLoaded, updateSysWaitSocket, updateSysActivityIndicator, updateSysCache1, socketIO } from '../stateStore/actions';
import { myColors, usrPassCheck, usrPassCheck1, usrPassCheck2, emailCheck, socket, bgImage, dummys } from '../constants/constants';

const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;

class Signup extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerLeft: (
        <Button
          onPress={()=> navigation.goBack()}
          title=""
          color="#fff"
          icon={{name: 'chevron-left', color: myColors.vlt, size: 35}}
          buttonStyle={{
            backgroundColor: 'transparent',
          }}
        />
      ),
    };
  }

  constructor(props){
    super(props)
    const date = new Date()
    const payload = {
      cmd: 'vltDetails',
      contents: {
        filter: this.props.reduxState.sys_activeTab,
        username: this.props.reduxState.usr_creds_username,
        lastSeen: date
      }
    }
    this.props.socketIO(payload)
  }//constructor

  componentWillMount() {
    socket.on('confirmedPhone', (result, socketID) => {
      if (socket.id == socketID) {
        switch (result.status) {
          case "0":
            this.props.updateSysCache1(result.request_id)
            this.props.navigation.navigate('SmsCode')
          break;
          case "10":
            setTimeout(() => {
              Alert.alert('Text message already sent, please check your inbox ')
            }, 750);
            this.props.updateSysCache1(result.request_id)
            this.props.navigation.navigate('SmsCode')
          break;
          default:
        }
      }
    })//confirmedPhone

    socket.on('confirmedCode', (result, socketID) => {
      if (socket.id == socketID) {
        switch (result.status) {
          case "0":
          this.props.updateSysCache1('')
          this.props.navigation.navigate('Signup2')
            break;
          case "16":
            Alert.alert('Code does not match. Please try again')
            break;
          default:
        }
      }
    })//confirmedPhone
  }



  // getUsername(event) {
  //   if (this.props.reduxState.usr_creds_username == '') {
  //     this.props.updateUsrUsername('#');
  //     this.edit = '#' + event.nativeEvent.text
  //     setTimeout(() => {
  //       this.props.updateUsrUsername(this.edit);
  //     }, 50);
  //   }else {
  //     this.props.updateUsrUsername(event.nativeEvent.text);
  //   }
  // }

  // getPassword(event) {
  //   const newpass = event.nativeEvent.text;
  //   this.props.updateUsrPassword(newpass);
  // }

  getEmail(event) {
    const newemail = event.nativeEvent.text;
    this.props.updateUsrEmail(newemail);
  }

  getFullName(event) {
    this.props.updateUsrFullName(event.nativeEvent.text);
  }

  getPhoneNumber(event) {
    if (this.props.reduxState.usr_creds_phoneNumber == '') {
      this.props.updateUsrPhoneNumber('+');
      this.edit = '+' + event.nativeEvent.text
      setTimeout(() => {
        this.props.updateUsrPhoneNumber(this.edit);
      }, 50);
    }else {
      this.props.updateUsrPhoneNumber(event.nativeEvent.text);
    }
  }

  fullNameCheck(){
    if (
      usrPassCheck
      .is().min(4)
      .is().max(25)
      .has().lowercase()
      //.has().not().spaces()
      .has().not().digits()
      .has().not().symbols()
      .is().not().oneOf(['username', 'Username123'])
      .validate(this.props.reduxState.usr_creds_fullName)
    ) {
      this.emailCheck()
    } else {
      Alert.alert(
        'Name Check',
        'Please enter valid full name',
        [
          {text: 'Close', style: 'cancel'},
        ]
      );
    }
  }

  emailCheck(){
    if (emailCheck.validate(this.props.reduxState.usr_creds_email)) {
      const payload = {
        cmd: 'confirmPhone',
        contents: {
          username: this.props.reduxState.usr_creds_username,
          phoneNumber: this.props.reduxState.usr_creds_phoneNumber,
          codeLength: 6
        }
      }
      this.props.socketIO(payload)
    }else {
      Alert.alert(
        'Email Check',
        'Please enter valid email',
        [
          {text: 'Close', style: 'cancel'},
        ]
      );
    }
  }

  render() {
    const {
      reduxState: {
        usr_creds_username,
        usr_creds_password,
        usr_creds_fullName,
        usr_creds_phoneNumber,
        usr_creds_email,
        sys_pageLoaded,
        sys_connectStatus,
        sys_activityIndicator
      }
    } = this.props;

    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
       <View style={styles.container} onPress={Keyboard.dismiss}>
         <ImageBackground source={bgImage()} style={styles.bgImage}>
           {(sys_pageLoaded == true ) && (sys_connectStatus.cloud == true) ? (
             <View style={styles.SignupView} onPress={Keyboard.dismiss}>
               <View style={styles.Signup} onPress={Keyboard.dismiss}>
                 <Input
                   leftIcon={
                     <Icon
                       name="contacts"
                       color={myColors.vlt}
                       size={25}
                     />
                   }
                   containerStyle={{ width: 300, marginVertical: 10 }}
                   onChange={this.getFullName.bind(this)}
                   value={usr_creds_fullName}
                   inputStyle={{ marginLeft: 10, color: myColors.vlt, fontSize: dummys.fontSize, fontWeight: 'bold' }}
                   keyboardAppearance="light"
                   placeholder="First & Last Name"
                   autoFocus={false}
                   autoCapitalize='words'
                   autoCorrect={false}
                   //keyboardType="email-address"
                   returnKeyType="next"
                   //ref={input => (this.emailInput = input)}
                   onSubmitEditing={() => {
                     // this.setState({ email_valid: this.validateEmail(email) });
                     // this.passwordInput.focus();
                   }}
                   blurOnSubmit={false}
                   placeholderTextColor='white'
                 />
                 <Input
                   leftIcon={
                     <Icon
                       name="email"
                       color={myColors.vlt}
                       size={25}
                     />
                   }
                   containerStyle={{ width: 300, marginVertical: 10 }}
                   onChange={this.getEmail.bind(this)}
                   value={usr_creds_email}
                   inputStyle={{ marginLeft: 10, color: myColors.vlt, fontSize: dummys.fontSize, fontWeight: 'bold' }}
                   keyboardAppearance="light"
                   placeholder="Email"
                   autoFocus={false}
                   autoCapitalize="none"
                   autoCorrect={false}
                   //keyboardType="email-address"
                   returnKeyType="next"
                   //ref={input => (this.emailInput = input)}
                   onSubmitEditing={() => {
                     // this.setState({ email_valid: this.validateEmail(email) });
                     // this.passwordInput.focus();
                   }}
                   blurOnSubmit={false}
                   placeholderTextColor='white'
                 />
                 <Input
                   leftIcon={
                     <Icon
                       name="phone"
                       color={myColors.vlt}
                       size={25}
                     />
                   }
                   containerStyle={{ width: 300, marginVertical: 10 }}
                   onChange={this.getPhoneNumber.bind(this)}
                   value={usr_creds_phoneNumber}
                   inputStyle={{ marginLeft: 10, color: myColors.vlt, fontSize: dummys.fontSize, fontWeight: 'bold' }}
                   keyboardAppearance="light"
                   placeholder="Phone Number"
                   autoFocus={false}
                   autoCapitalize="none"
                   autoCorrect={false}
                   keyboardType="phone-pad"
                   returnKeyType="next"
                   //ref={input => (this.emailInput = input)}
                   onSubmitEditing={() => {
                     // this.setState({ email_valid: this.validateEmail(email) });
                     // this.passwordInput.focus();
                   }}
                   blurOnSubmit={false}
                   placeholderTextColor='white'
                 />
               </View>
               <Button
                 raised
                 title="Next"
                 //icon={{name: 'add', color: 'white'}}
                 activeOpacity={1}
                 //underlayColor="transparent"
                 onPress={()=> this.fullNameCheck()}
                 //loading={showLoading}
                 loadingProps={{ size: 'small', color: 'white' }}
                 //disabled={!email_valid && password.length < 8}
                 buttonStyle={{
                   height: 50,
                   width: 250,
                   backgroundColor: myColors.vlt,
                   borderWidth: 2,
                   borderColor: myColors.control,
                   borderRadius: 30,
                 }}
                 // containerStyle={{ marginTop: 30 }}
                 titleStyle={{ fontWeight: 'bold', color: 'white' }}
               />

               {/* <View style={styles.footerView}>
                 <Text style={{ color: 'grey' }}>Already have an account?</Text>
                 <Text style={{ color: myColors.vlt, fontSize: 15, fontWeight:'bold', textDecorationLine:'underline', marginTop: 2, height: 30 }}
                        onPress={() => this.props.navigation.navigate('Login')}>Log In here</Text>
               </View> */}
             </View>
           ) : (
             <Text>Loading...</Text>
           )}
         </ImageBackground>
       </View>
     </TouchableWithoutFeedback>
     );
   }
 }

 const styles = StyleSheet.create({
   container: {
     flex: 1,
   },
   bgImage: {
     flex: 1,
     top: 0,
     left: 0,
     width: SCREEN_WIDTH,
     height: SCREEN_HEIGHT,
     justifyContent: 'center',
     alignItems: 'center',
   },
   SignupView: {
     backgroundColor: 'transparent',
     width: 250,
     height: 400,
   },
   loginTitle: {
     flex: 1,
     justifyContent: 'center',
     alignItems: 'center',
   },
   Signup: {
     flex: 1,
     justifyContent: 'center',
     alignItems: 'center',
   },
   footerView: {
     marginTop: 30,
     flex: 0.5,
     justifyContent: 'center',
     alignItems: 'center',
   },
 });

const mapStateToProps = ({ reduxState }) => ({ reduxState });

export default connect(mapStateToProps, { updateUsrFullName, updateUsrUsername, updateUsrPassword, updateUsrEmail, updateUsrPhoneNumber,
  updateSysPageLoaded, updateSysWaitSocket, updateSysActivityIndicator, updateSysCache1, socketIO })(Signup);
