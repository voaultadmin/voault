import React, { Component } from 'react';
import {
  Alert,
  ActivityIndicator,
  ImageBackground,
  Text,
  StyleSheet,
  View,
  TextInput,
  Keyboard,
  TouchableWithoutFeedback,
  TouchableHighlight,
  Dimensions,
  Linking
} from 'react-native';
import { connect } from 'react-redux';
import { Input, Button, Icon, CheckBox } from 'react-native-elements';
import {
  DotIndicator,
  BallIndicator,
  BarIndicator,
  MaterialIndicator,
} from 'react-native-indicators';
import { updateUsrFullName, updateUsrUsername, updateUsrPassword,
  updateUsrEmail, updateSysPageLoaded, updateSysWaitSocket, updateSysActiveTab, updateSysActivityIndicator, socketIO } from '../stateStore/actions';
import { myColors, usrPassCheck, usrPassCheck1, usrPassCheck2, emailCheck, socket, bgImage, dummys } from '../constants/constants';

const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;

class Signup2 extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerLeft: (
        <Button
          onPress={()=> navigation.goBack()}
          title=""
          color="#fff"
          icon={{name: 'chevron-left', color: myColors.vlt, size: 35}}
          buttonStyle={{
            backgroundColor: 'transparent',
          }}
        />
      ),
    };
  }

  constructor(props){
    super(props)

    const date = new Date()
    const payload = {
      cmd: 'vltDetails',
      contents: {
        filter: this.props.reduxState.sys_activeTab,
        username: this.props.reduxState.usr_creds_username,
        lastSeen: date
      }
    }
    this.props.socketIO(payload)

  }//constructor

  componentWillMount() {
    this.props.updateSysActivityIndicator(false)
    this.btnColor = myColors.vlt

    socket.on('vltStatus', () => {
        this.props.updateSysPageLoaded(true)
    })//vltStatus

    socket.on('validSignup', (result, socketID) => {
      if (socketID == socket.id) {
        switch (result) {
          case true:
            this.props.updateSysActiveTab('all')
            setTimeout(() => {
              this.props.navigation.navigate('Menu')
            }, 2000);
            break;
          case 'userExists':
            this.btnColor = myColors.vlt
            this.props.updateSysWaitSocket(false)
            setTimeout(() => {
              Alert.alert('Username or Email already exits. Please log in')
            }, 500);
            break;
          case 'error':
            this.btnColor = myColors.vlt
            this.props.updateSysWaitSocket(false)
            Alert.alert('We encountered an error creating the account. Please try again')
            break;
          default:
        }
        this.props.updateSysActivityIndicator(false)
      }
    })//validSignup
  }//componentWillMount

  getUsername(event) {
    if (this.props.reduxState.usr_creds_username == '') {
      this.props.updateUsrUsername('#');
      this.edit = '#' + event.nativeEvent.text
      setTimeout(() => {
        this.props.updateUsrUsername(this.edit);
      }, 50);
    }else {
      this.props.updateUsrUsername(event.nativeEvent.text);
    }
  }

  getPassword(event) {
    const newpass = event.nativeEvent.text;
    this.props.updateUsrPassword(newpass);
  }

  // getEmail(event) {
  //   const newemail = event.nativeEvent.text;
  //   this.props.updateUsrEmail(newemail);
  // }
  //
  // getFullName(event) {
  //   this.props.updateUsrFullName(event.nativeEvent.text);
  // }


  usernameCheck(){
    if (
      usrPassCheck1
      .is().min(6)
      .is().max(25)
      .has().lowercase()
      .has().not().spaces()
      //.has().not().symbols()
      .is().not().oneOf(['username', 'Username123'])
      .validate(this.props.reduxState.usr_creds_username)
    ) {
      if (this.props.reduxState.usr_creds_username.charAt(0) == '#') {
        this.passwordCheck()
      }
      else {
        this.props.updateUsrUsername('#' + this.props.reduxState.usr_creds_username)
      }

    } else {
      Alert.alert(
        'Username Check',
        'Min 6 characters',
        [
          {text: 'Close', style: 'cancel'},
        ]
      );
    }
  }

  passwordCheck(){
    if (
      usrPassCheck2
      .is().min(8)
      .is().max(25)
      .has().not().spaces()
      .has().lowercase()
      .is().not().oneOf(['Passw0rd', 'Password123'])
      .validate(this.props.reduxState.usr_creds_password)
    ) {
      this.props.updateSysActivityIndicator(true)
      this.btnColor = 'gray'
      setTimeout(() => {
        this.props.updateSysActivityIndicator(false)
        this.btnColor = myColors.vlt
        this.props.updateSysWaitSocket(false)
      }, 5000);
      this.props.updateSysWaitSocket(true)
      const payload = {
        cmd: 'signup',
        contents: {
          fullName: this.props.reduxState.usr_creds_fullName,
          username: this.props.reduxState.usr_creds_username,
          password: this.props.reduxState.usr_creds_password,
          email: this.props.reduxState.usr_creds_email,
          phoneNumber: this.props.reduxState.usr_creds_phoneNumber,
          confirmedEmail: true,
        },
      }
      this.props.socketIO(payload)
    } else {
      Alert.alert(
        'Password must be over 8 characters',
        [
          {text: 'Close', style: 'cancel'},
        ]
      );
    }
  }

  render() {
    this.signupColor = '#03d0d0';
    const {
      reduxState: {
        usr_creds_username,
        usr_creds_password,
        usr_creds_fullName,
        usr_creds_email,
        sys_pageLoaded,
        sys_connectStatus,
        sys_activityIndicator
      }
    } = this.props;

    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
       <View style={styles.container} onPress={Keyboard.dismiss}>
         <ImageBackground source={bgImage()} style={styles.bgImage}>
           {(sys_pageLoaded == true ) && (sys_connectStatus.cloud == true) ? (
             <View style={styles.SignupView} onPress={Keyboard.dismiss}>
               <View style={styles.Signup} onPress={Keyboard.dismiss}>
                 <Input
                   leftIcon={
                     <Icon
                       name="person"
                       color={myColors.vlt}
                       size={25}
                     />
                   }
                   containerStyle={{ width: 300, marginVertical: 10 }}
                   onChange={this.getUsername.bind(this)}
                   value={usr_creds_username}
                   inputStyle={{ marginLeft: 10, color: myColors.vlt, fontSize: dummys.fontSize, fontWeight: 'bold' }}
                   keyboardAppearance="light"
                   placeholder="Create Username"
                   autoFocus={false}
                   autoCapitalize="none"
                   autoCorrect={false}
                   //keyboardType="email-address"
                   returnKeyType="next"
                   //ref={input => (this.emailInput = input)}
                   onSubmitEditing={() => {
                     // this.setState({ email_valid: this.validateEmail(email) });
                     // this.passwordInput.focus();
                   }}
                   blurOnSubmit={false}
                   placeholderTextColor='white'
                   errorStyle={{ textAlign: 'center', fontSize: 12 }}
                   errorMessage={
                     sys_pageLoaded ? null : 'Please enter a valid email address'
                   }
                 />
                 <Input
                   leftIcon={
                     <Icon
                       name="lock"
                         color={myColors.vlt}
                       size={25}
                     />
                   }
                   containerStyle={{ width: 300, marginVertical: 10 }}
                   onChange={this.getPassword.bind(this)}
                   value={usr_creds_password}
                   inputStyle={{ marginLeft: 10, color: myColors.vlt, fontSize: dummys.fontSize, fontWeight: 'bold' }}
                   secureTextEntry={true}
                   keyboardAppearance="light"
                   placeholder="Create Password"
                   autoCapitalize="none"
                   autoCorrect={false}
                   //keyboardType="default"
                   returnKeyType="done"
                   //ref={input => (this.passwordInput = input)}
                   blurOnSubmit={true}
                   placeholderTextColor='white'
                 />
                 <Text style={styles.termStyle}>By signing up, I agree to the</Text>
                 <Text style={styles.termStyle} onPress={ ()=> Linking.openURL('https://www.voault.com/user-license') } >Terms and Conditions</Text>
               </View>
               <Button
                 raised
                 title="Signup"
                 //icon={{name: 'add', color: 'white'}}
                 activeOpacity={1}
                 //underlayColor="transparent"
                 onPress={()=> this.usernameCheck()}
                 //loading={showLoading}
                 loadingProps={{ size: 'small', color: 'white' }}
                 //disabled={!email_valid && password.length < 8}
                 buttonStyle={{
                   height: 50,
                   width: 250,
                   backgroundColor: this.btnColor,
                   borderWidth: 2,
                   borderColor: myColors.control,
                   borderRadius: 30,
                 }}
                 //containerStyle={{ marginTop: 40 }}
                 titleStyle={{ fontWeight: 'bold', color: 'white' }}
               />
               {/* <View style={styles.footerView}>
                 <Text style={{ color: 'grey' }}>Already have an account?</Text>
                 <Text style={{ color: myColors.vlt, fontSize: 15, fontWeight:'bold', textDecorationLine:'underline', marginTop: 2, height: 30 }}
                        onPress={() => this.props.navigation.navigate('Login')}>Log In here</Text>
               </View> */}
             </View>
           ) : (
             <Text>Loading...</Text>
           )}
         </ImageBackground>
       </View>
     </TouchableWithoutFeedback>
     );
   }
 }

 const styles = StyleSheet.create({
   container: {
     flex: 1,
   },
   bgImage: {
     flex: 1,
     top: 0,
     left: 0,
     width: SCREEN_WIDTH,
     height: SCREEN_HEIGHT,
     justifyContent: 'center',
     alignItems: 'center',
   },
   SignupView: {
     backgroundColor: 'transparent',
     width: 250,
     height: 400,
   },
   loginTitle: {
     flex: 1,
     justifyContent: 'center',
     alignItems: 'center',
   },
   travelText: {
     color: 'white',
     fontSize: 30,
     //fontFamily: 'bold',
   },
   plusText: {
     color: 'white',
     fontSize: 30,
     //fontFamily: 'regular',
   },
   termStyle: {
     color: myColors.vlt,
     fontSize: 12,
     fontWeight:'bold',
     textDecorationLine:'underline'
   },
   Signup: {
     flex: 1,
     justifyContent: 'center',
     alignItems: 'center',
   },
   footerView: {
     marginTop: 30,
     flex: 0.5,
     justifyContent: 'center',
     alignItems: 'center',
   },
 });

const mapStateToProps = ({ reduxState }) => ({ reduxState });

export default connect(mapStateToProps, { updateUsrFullName, updateUsrUsername, updateUsrPassword,
  updateUsrEmail, updateSysPageLoaded, updateSysWaitSocket, updateSysActiveTab, updateSysActivityIndicator, socketIO })(Signup2);
