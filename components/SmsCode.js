import React, { Component } from 'react';
import {
  Alert,
  ImageBackground,
  Text,
  StyleSheet,
  View,
  TextInput,
  Keyboard,
  TouchableWithoutFeedback,
  TouchableHighlight,
  Dimensions,
  SafeAreaView,
} from 'react-native';
import { connect } from 'react-redux';
import { Input, Button, Icon } from 'react-native-elements';
import CodeInput from 'react-native-code-input';
import {  updateSysCache1, socketIO } from '../stateStore/actions';
import { myColors, socket, bgImage, dummys } from '../constants/constants';

const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;

class SmsCode extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerLeft: (
        <Button
          onPress={()=> navigation.goBack()}
          title=""
          color="#fff"
          icon={{name: 'chevron-left', color: myColors.vlt, size: 35}}
          buttonStyle={{
            backgroundColor: 'transparent',
          }}
        />
      ),
    };
  }

  constructor(props){
    super(props)

  }//constructor

  componentWillMount() {
    this.btnColor = myColors.vlt
  }

  getVerificationCode(event){
    this.props.updateUsrVerificationCode(event.nativeEvent.text);
  }

  verifyCode(code){
    const payload = {
      cmd: 'confirmCode',
      contents: {
        code: code,
        request_id: this.props.reduxState.sys_cache1,
      }
    }
    this.props.socketIO(payload)
  }

  render() {
    const {
      reduxState: {
        sys_cache1,
      }
    } = this.props;

    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
       <View style={styles.container} onPress={Keyboard.dismiss}>
        <ImageBackground source={bgImage()} style={styles.bgImage}>
          <View style={styles.subContainer}>
            <Text style={styles.txtStyle}> Enter Code</Text>
            <CodeInput
              ref="codeInputRef2"
              secureTextEntry
              borderType={'underline'}
              space={7}
              codeLength={6}
              size={40}
              inputPosition='left'
              onFulfill={(code) => this.verifyCode(code)}
            />
          </View>
        </ImageBackground>
       </View>
     </TouchableWithoutFeedback>
     );
   }
 }

 const styles = StyleSheet.create({
   container: {
     flex: 1,
     justifyContent: 'space-between'
   },
   subContainer: {
     marginTop: SCREEN_HEIGHT * 0.33,
     justifyContent: 'center',
     alignItems: 'center',
   },
   txtStyle: {
     color: myColors.vlt,
     fontSize: 14,
     fontWeight:'bold',
     //textDecorationLine:'underline'
   },
   bgImage: {
     flex: 1,
     top: 0,
     left: 0,
     width: SCREEN_WIDTH,
     height: SCREEN_HEIGHT,
     justifyContent: 'center',
     alignItems: 'center',
   },
 });

const mapStateToProps = ({ reduxState }) => ({ reduxState });

export default connect(mapStateToProps, { updateSysCache1, socketIO })(SmsCode);
