import React, { Component } from 'react';
import {
  ActivityIndicator,
  Image,
  ImageBackground,
  Text,
  StyleSheet,
  View,
  TextInput,
  Keyboard,
  TouchableWithoutFeedback,
  TouchableHighlight,
  Platform,
  WebView,
  Dimensions,
  FlatList,
  SafeAreaView,
  ScrollView,
} from 'react-native';
import { connect } from 'react-redux';
import { Avatar, Card, List, ListItem, Button, Icon } from 'react-native-elements';
import { dummys, myColors, vlts, currentKeys, searchKeys, socket, bgImageNoLogo } from '../constants/constants';
const SCREEN_WIDTH = Dimensions.get('window').width;
const SCREEN_HEIGHT = Dimensions.get('window').height;
const CARDIMG_HEIGHT = 0.15 * SCREEN_HEIGHT
const CARDIMG_WIDTH = 0.6 * SCREEN_WIDTH


class VIntent extends Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerLeft: (
        <Button
          onPress={()=> navigation.navigate('Menu')}
          title=""
          color="#fff"
          icon={{name: 'keyboard-arrow-left', color: myColors.vlt, size: 35}}
          buttonStyle={{
            backgroundColor: 'transparent',
          }}
        />
      ),
    };
  }


  keyExtractor = (item, index) => index.toString()

  renderItem = ({ item }) => (
    <ListItem bottomDivider>
      <Avatar source={{uri: item.avatar_url}} />
      <ListItem.Content>
        <ListItem.Title>{item.name}</ListItem.Title>
        <ListItem.Subtitle>{item.subtitle}</ListItem.Subtitle>
      </ListItem.Content>
      <ListItem.Chevron />
    </ListItem>
  )

  render() {
    const list = [
      {
        name: 'Amy Farha',
        avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
        subtitle: 'Vice President'
      },
      {
        name: 'Chris Jackson',
        avatar_url: 'https://s3.amazonaws.com/uifaces/faces/twitter/adhamdannaway/128.jpg',
        subtitle: 'Vice Chairman'
      },
    ]

    const {
      reduxState: {
        usr_creds_username,
        vlt_meta_index,
        vlt_meta_directions,
        vlt_meta_unit,
        vlt_meta_rate,
        sys_activeTab,
      }
    } = this.props;

    return (
     <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
       <ScrollView style={styles.container}>
         <FlatList
           keyExtractor={this.keyExtractor}
           data={list}
           renderItem={this.renderItem}
         />
       </ScrollView>
     </TouchableWithoutFeedback>
    );//return
  }//render
}//Component


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  bgImage: {
    flex: 1,
    top: 0,
    left: 0,
    width: SCREEN_WIDTH,
    height: SCREEN_HEIGHT,
    justifyContent: 'center',
    alignItems: 'center',
  },
  txt1: {
    color: myColors.vlt,
    fontWeight: 'bold',
    fontSize: 14,
    textAlign: 'center',
    marginBottom: 20,
    // marginTop: 7,
  },
  txt2: {
    color: myColors.vlt,
    fontWeight: 'bold',
    //fontStyle: 'italic',
    fontSize: 13.5,
    textAlign: 'center',
    marginBottom: 10,
    // marginTop: 7,
  },
  details: {
    // width: SCREEN_WIDTH,
    // height: 0.6*SCREEN_HEIGHT,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
const mapStateToProps = ({ reduxState }) => ({ reduxState });
export default connect(mapStateToProps, {})(VIntent);
