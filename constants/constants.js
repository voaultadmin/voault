import io from 'socket.io-client';
import daynight from 'daynight';

//const SERVERURL = 'https://voaultserver.herokuapp.com/'
const SERVERURL = 'http://192.168.0.104:3000'
//const socket = io.connect('http://663642ae.ngrok.io',{'forceNew':true });
//const socket = io.connect('http://192.168.1.145:3000',{'forceNew':true });
//const socket = io.connect('http://10.0.0.103:3000',{'forceNew':true });
const socket = io.connect(SERVERURL);
//const socket = io.connect('http://localhost:3000',{'forceNew':true });

var passwordValidator = require('password-validator');
var usrPassCheck = new passwordValidator();
var usrPassCheck1 = new passwordValidator();
var usrPassCheck2 = new passwordValidator();
var emailCheck = require("email-validator");

function bgImage(){
    const result = daynight()
    const IMG1 = require('../images/bg-1.png');
    const IMG2 = require('../images/bg-2.png');

    // switch (result.dark) {
    //   case true:
    //     return IMG2
    //     break;
    //   case false:
    //     return IMG1
    //     break;
    //   default:
    //   return IMG1
    // }
    return IMG2
  }

function bgImageNoLogo(){
    const result = daynight()
    const IMG1 = require('../images/bg-1nl.png');
    const IMG2 = require('../images/bg-2nl.png');

    // switch (result.dark) {
    //   case true:
    //     return IMG2
    //     break;
    //   case false:
    //     return IMG1
    //     break;
    //   default:
    //   return IMG1
    // }
    return IMG2
  }

const myColors = {
  vlt: 'rgb(24, 157, 176)',
  color1: 'black',
  color2: 'white',
  action: 'white',
  control: 'white',
  control1: 'orange',
  control2: 'rgb(16, 204, 204)',
  info: 'green',
}

const dummys = {
  key1: '#key1',
  key2: '#key2',
  fontSize: 18,
  txtColor: myColors.vlt
}

var vlts = []
var currentKeys = []
var searchKeys = []


const tabs = [
  // {
  //   key: 'account',
  //   icon: 'account-circle',
  //   label: 'Account',
  //   barColor: myColors.vlt,
  //   pressColor: 'rgba(255, 255, 255, 0.16)'
  // },
  {
    key: 'all',
    icon: 'dashboard',
    label: 'All Hubs',
    barColor: myColors.color2,
    pressColor: 'rgba(255, 255, 255, 0.16)'
  },
  // {
  //   key: 'free',
  //   icon: 'lock-open',
  //   label: 'Free Hubs',
  //   labelColor: 'black',
  //   barColor: myColors.color2,
  //   pressColor: 'rgba(255, 255, 255, 0.16)'
  // },
  // {
  //   key: 'reserved',
  //   icon: 'lock',
  //   label: 'My Hubs',
  //   labelColor: 'black',
  //   barColor: myColors.color2,
  //   pressColor: 'rgba(255, 255, 255, 0.16)'
  // },
  {
    key: 'pickup',
    icon: 'lock',
    label: 'My Pickups',
    barColor: myColors.color2,
    pressColor: 'rgba(255, 255, 255, 0.16)'
  },
  {
    key: 'VI',
    icon: 'event-note',
    label: 'VI',
    barColor: myColors.color2,
    pressColor: 'rgba(255, 255, 255, 0.16)'
  },
  // {
  //   key: 'reserve',
  //   icon: 'lock-open',
  //   label: 'Reserve',
  //   barColor: myColors.vlt,
  //   pressColor: 'rgba(255, 255, 255, 0.16)'
  // },
]

const tabsMeta = {
  iconSize: 22,
  iconColor: myColors.vlt,
}




module.exports = {
  vlts,
  currentKeys,
  searchKeys,
  tabs,
  tabsMeta,
  socket,
  myColors,
  dummys,
  usrPassCheck,
  usrPassCheck1,
  usrPassCheck2,
  emailCheck,
  bgImage,
  bgImageNoLogo,
  SERVERURL
}
