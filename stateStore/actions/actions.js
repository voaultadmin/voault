import {
  //---------- USR  ----------//
  UPDATE_USR_FULLNAME,
  UPDATE_USR_EMAIL,
  UPDATE_USR_USERNAME,
  UPDATE_USR_PASSWORD,
  UPDATE_USR_PHONENUMBER,
  UPDATE_USR_LATITUDE,
  UPDATE_USR_LONGITUDE,

  //---------- VLT  ----------//
  UPDATE_VLT_TAG,
  UPDATE_VLT_UNIT,
  UPDATE_VLT_SIZE,
  UPDATE_VLT_LOCATION,
  UPDATE_VLT_ADDRESS,
  UPDATE_VLT_ZIP,
  UPDATE_VLT_RATE,
  UPDATE_VLT_DIRECTIONS,
  UPDATE_VLT_INDEX,
  UPDATE_VLT_RESERVED,
  UPDATE_VLT_MASTERKEY,
  UPDATE_VLT_KEY1,
  UPDATE_VLT_KEY2,
  UPDATE_VLT_RESTIME,

  UPDATE_VLT_DOOR,

  //---------- SYSTEM  ----------//
  UPDATE_SYS_CACHE1,
  UPDATE_SYS_CONNECTSTATUS,
  UPDATE_SYS_PAGELOADED,
  UPDATE_SYS_PAGELOADED1,
  UPDATE_SYS_WAITSOCKET,
  UPDATE_SYS_SEARCHVALUE,
  UPDATE_SYS_STARCOUNT,
  UPDATE_SYS_ACTIVITYINDICATOR,
  UPDATE_SYS_ACTIVETAB,
  UPDATE_SYS_EDITPAGE_TARGET,
  UPDATE_SYS_EDITPAGE_CONTENT,
  UPDATE_SYS_PROFILEPIC,
  UPDATE_SYS_VITYPE,
  UPDATE_SYS_VITIMESLIDER,
  UPDATE_SYS_VITOGGLE,
  UPDATE_SYS_VIEXPDATE,

  //---------- ORDER  ----------//
  UPDATE_ORD_ITEMNAME,
  UPDATE_ORD_ITEMSIZE,
  UPDATE_ORD_ITEMPRICE,
  UPDATE_ORD_SECONDARYID,


  SOCKET_IO,

} from './types';

  //---------- USR  ----------//
export const updateUsrFullName = payload => ({ type: UPDATE_USR_FULLNAME, payload });
export const updateUsrEmail = payload => ({ type: UPDATE_USR_EMAIL, payload });
export const updateUsrUsername = payload => ({ type: UPDATE_USR_USERNAME, payload });
export const updateUsrPassword = payload => ({ type: UPDATE_USR_PASSWORD, payload });
export const updateUsrPhoneNumber = payload => ({ type: UPDATE_USR_PHONENUMBER, payload });
export const updateUsrLatitude = payload => ({ type: UPDATE_USR_LATITUDE, payload });
export const updateUsrLongitude = payload => ({ type: UPDATE_USR_LONGITUDE, payload });


  //---------- VLT  ----------//
export const updateVltTag = payload => ({ type: UPDATE_VLT_TAG, payload });
export const updateVltUnit = payload => ({ type: UPDATE_VLT_UNIT, payload });
export const updateVltSize = payload => ({ type: UPDATE_VLT_SIZE, payload });
export const updateVltLocation = payload => ({ type: UPDATE_VLT_LOCATION, payload });
export const updateVltAddress = payload => ({ type: UPDATE_VLT_ADDRESS, payload });
export const updateVltZip = payload => ({ type: UPDATE_VLT_ZIP, payload });
export const updateVltRate = payload => ({ type: UPDATE_VLT_RATE, payload });
export const updateVltDirections = payload => ({ type: UPDATE_VLT_DIRECTIONS, payload });
export const updateVltIndex = payload => ({ type: UPDATE_VLT_INDEX, payload });
export const updateVltReserved = payload => ({ type: UPDATE_VLT_RESERVED, payload });
export const updateVltMasterKey = payload => ({ type: UPDATE_VLT_MASTERKEY, payload });
export const updateVltKey1 = payload => ({ type: UPDATE_VLT_KEY1, payload });
export const updateVltKey2 = payload => ({ type: UPDATE_VLT_KEY2, payload });
export const updateVltResTime = payload => ({ type: UPDATE_VLT_RESTIME, payload });

export const updateVltDoor = payload => ({ type: UPDATE_VLT_DOOR, payload });


  //---------- SYSTEM  ----------//
export const updateSysCache1 = payload => ({ type: UPDATE_SYS_CACHE1, payload });
export const updateSysConnectStatus = payload => ({ type: UPDATE_SYS_CONNECTSTATUS, payload });
export const updateSysPageLoaded = payload => ({ type: UPDATE_SYS_PAGELOADED, payload });
export const updateSysPageLoaded1 = payload => ({ type: UPDATE_SYS_PAGELOADED1, payload });
export const updateSysWaitSocket = payload => ({ type: UPDATE_SYS_WAITSOCKET, payload });
export const updateSysActiveTab= payload => ({ type: UPDATE_SYS_ACTIVETAB, payload });
export const updateSysSearchValue= payload => ({ type: UPDATE_SYS_SEARCHVALUE, payload });
export const updateSysStarCount= payload => ({ type: UPDATE_SYS_STARCOUNT, payload });
export const updateSysActivityIndicator= payload => ({ type: UPDATE_SYS_ACTIVITYINDICATOR, payload });
export const updateSysEditTarget = payload => ({ type: UPDATE_SYS_EDITPAGE_TARGET, payload });
export const updateSysEditContent = payload => ({ type: UPDATE_SYS_EDITPAGE_CONTENT, payload });
export const updateSysProfilePic = payload => ({ type: UPDATE_SYS_PROFILEPIC, payload });
export const updateSysViType = payload => ({ type: UPDATE_SYS_VITYPE, payload });
export const updateSysViTimeSlider = payload => ({ type: UPDATE_SYS_VITIMESLIDER, payload });
export const updateSysViToggle = payload => ({ type: UPDATE_SYS_VITOGGLE, payload });
export const updateSysViExpDate = payload => ({ type: UPDATE_SYS_VIEXPDATE, payload });

  //---------- ORDER  ----------//
export const updateOrdItemName = payload => ({ type: UPDATE_ORD_ITEMNAME, payload });
export const updateOrdItemPrice = payload => ({ type: UPDATE_ORD_ITEMPRICE, payload });
export const updateOrdItemSize = payload => ({ type: UPDATE_ORD_ITEMSIZE, payload });
export const updateOrdSecondaryID = payload => ({ type: UPDATE_ORD_SECONDARYID, payload });



export const socketIO = payload => ({ type: SOCKET_IO, payload });
