//---------- USR  ----------//
export const UPDATE_USR_FULLNAME = 'update_usr_fullName';
export const UPDATE_USR_USERNAME = 'update_usr_username';
export const UPDATE_USR_PASSWORD = 'update_usr_password';
export const UPDATE_USR_EMAIL = 'update_usr_email';
export const UPDATE_USR_PHONENUMBER = 'update_usr_phoneNumber';
export const UPDATE_USR_LATITUDE = 'update_usr_latitude';
export const UPDATE_USR_LONGITUDE = 'update_usr_longitude';


//---------- VLT  ----------//
export const UPDATE_VLT_TAG = 'update_vlt_tag';
export const UPDATE_VLT_UNIT = 'update_vlt_unit';
export const UPDATE_VLT_SIZE = 'update_vlt_size';
export const UPDATE_VLT_LOCATION = 'update_vlt_location';
export const UPDATE_VLT_ADDRESS = 'update_vlt_address';
export const UPDATE_VLT_ZIP = 'update_vlt_zip';
export const UPDATE_VLT_RATE = 'update_vlt_rate';
export const UPDATE_VLT_DIRECTIONS = 'update_vlt_directions';
export const UPDATE_VLT_INDEX = 'update_vlt_index';
export const UPDATE_VLT_RESERVED = 'update_vlt_reserved';
export const UPDATE_VLT_MASTERKEY = 'update_vlt_masterKey';
export const UPDATE_VLT_KEY1 = 'update_vlt_key1';
export const UPDATE_VLT_KEY2 = 'update_vlt_key2';
export const UPDATE_VLT_RESTIME = 'update_vlt_resTime';

export const UPDATE_VLT_DOOR = 'update_vlt_door';

//---------- SYSTEM  ----------//
export const UPDATE_SYS_CACHE1 = 'update_sys_cache1';
export const UPDATE_SYS_CONNECTSTATUS = 'update_sys_connectstatus';
export const UPDATE_SYS_PAGELOADED = 'update_sys_pageLoaded';
export const UPDATE_SYS_PAGELOADED1 = 'update_sys_pageLoaded1';
export const UPDATE_SYS_WAITSOCKET = 'update_sys_waitSocket';
export const UPDATE_SYS_ACTIVETAB = 'update_sys_activeTab';
export const UPDATE_SYS_SEARCHVALUE = 'update_sys_searchValue';
export const UPDATE_SYS_STARCOUNT = 'update_sys_starCount';
export const UPDATE_SYS_ACTIVITYINDICATOR = 'update_sys_activityIndicator';
export const UPDATE_SYS_EDITPAGE_TARGET = 'update_sys_editTarget';
export const UPDATE_SYS_EDITPAGE_CONTENT = 'update_sys_editContent';
export const UPDATE_SYS_PROFILEPIC = 'update_sys_profilePic';
export const UPDATE_SYS_VITYPE = 'update_sys_viType';
export const UPDATE_SYS_VITIMESLIDER = 'update_sys_viTimeSlider';
export const UPDATE_SYS_VITOGGLE = 'update_sys_viToggle';
export const UPDATE_SYS_VIEXPDATE = 'update_sys_viExpDate';

//---------- ORDER  ----------//
export const UPDATE_ORD_ITEMNAME = 'update_ord_itemName';
export const UPDATE_ORD_ITEMPRICE = 'update_ord_itemPrice';
export const UPDATE_ORD_ITEMSIZE = 'update_ord_itemSize';
export const UPDATE_ORD_SECONDARYID = 'update_ord_secondaryID';

export const SOCKET_IO = 'socket_io';
