import { combineReducers } from 'redux';
import reduxState from './reducers';

export default combineReducers({
  reduxState
});
