import { socket, myColors } from '../../constants/constants'

import {
  //---------- USR  ----------//
  UPDATE_USR_FULLNAME,
  UPDATE_USR_EMAIL,
  UPDATE_USR_USERNAME,
  UPDATE_USR_PASSWORD,
  UPDATE_USR_PHONENUMBER,
  UPDATE_USR_LATITUDE,
  UPDATE_USR_LONGITUDE,

  //----------  VLT   ----------//

  UPDATE_VLT_TAG,
  UPDATE_VLT_UNIT,
  UPDATE_VLT_SIZE,
  UPDATE_VLT_LOCATION,
  UPDATE_VLT_ADDRESS,
  UPDATE_VLT_ZIP,
  UPDATE_VLT_RATE,
  UPDATE_VLT_DIRECTIONS,
  UPDATE_VLT_INDEX,
  UPDATE_VLT_RESERVED,
  UPDATE_VLT_MASTERKEY,
  UPDATE_VLT_KEY1,
  UPDATE_VLT_KEY2,
  UPDATE_VLT_RESTIME,

  UPDATE_VLT_DOOR,

  //---------- SYSTEM  ----------//
  UPDATE_SYS_CACHE1,
  UPDATE_SYS_CONNECTSTATUS,
  UPDATE_SYS_PAGELOADED,
  UPDATE_SYS_PAGELOADED1,
  UPDATE_SYS_WAITSOCKET,
  UPDATE_SYS_ACTIVETAB,
  UPDATE_SYS_SEARCHVALUE,
  UPDATE_SYS_STARCOUNT,
  UPDATE_SYS_ACTIVITYINDICATOR,
  UPDATE_SYS_EDITPAGE_TARGET,
  UPDATE_SYS_EDITPAGE_CONTENT,
  UPDATE_SYS_PROFILEPIC,
  UPDATE_SYS_VITYPE,
  UPDATE_SYS_VITIMESLIDER,
  UPDATE_SYS_VITOGGLE,
  UPDATE_SYS_VIEXPDATE,

  //----------- ORDER -----------//
  UPDATE_ORD_ITEMNAME,
  UPDATE_ORD_ITEMSIZE,
  UPDATE_ORD_ITEMPRICE,
  UPDATE_ORD_SECONDARYID,

  SOCKET_IO,




} from '../actions/types';

const STATE = {
  //---------- USR  ----------//
  usr_creds_username: '#dahirou',
  usr_creds_password: '',
  usr_creds_email: '',
  usr_creds_fullName: '',
  usr_creds_phoneNumber: '',


  // usr_geo_latitude: '',
  // usr_geo_longitude: '',


  //----------  VLT   ----------//
  // vlt_meta_tag: '',
  //vlt_meta_unit: 'Addis',
  // vlt_meta_location: '',
  // vlt_meta_address: '',
  // vlt_meta_zip: '',
  // vlt_meta_size: '',
  // vlt_meta_rate: '',
  // vlt_meta_directions: '',
  // vlt_meta_resTime: '',

  // vlt_geo_latitude: '',
  // vlt_geo_longitude: '',

  vlt_creds_reserved: false,
  vlt_creds_masterKey: '',
  vlt_creds_key1: '',
  vlt_creds_key2: '',

  vlt_hw_door: '',
  //vlt_feedbback_open: false,


  //---------- SYSTEM  ----------//
  sys_cache1: '',
  sys_connectStatus: {
    cloud: 'init',
    vlt: false
  },
  sys_pageLoaded: false,
  sys_pageLoaded1: false,
  sys_activeTab: 'all',
  sys_waitSocket: false,
  sys_searchValue: '',
  sys_starCount: 4.5,
  sys_activityIndicator: false,
  sys_editContent: {
    currentPassword: '',
    username: '',
    newPassword: '',
    confirmPassword: '',
    email: ''
  },
  sys_viType: '',
  sys_viTimeSlider: 1,
  sys_viToggle: false,
  //sys_profilePic: 'https://odesk-prod-portraits.s3.amazonaws.com/Users:morris_huling:PortraitUrl_100?AWSAccessKeyId=AKIAIKIUKM3HBSWUGCNQ&Expires=2147483647&Signature=IbtByTs9FKxwhsFOUBWsGHXAcRc%3D&1533901493724878&1554676114010',
  // sys_reserveBtnTxt: 'Reserve',
  // sys_reserveBtnColor: 'orange',
  // sys_reserveBtnLogo: 'lock-open',
  //sys_reserveBtnTxt: 'Control',
  //sys_reserveBtnColor: myColors.vlt,
  //sys_reserveBtnLogo: 'leak-add',

  //---------- ORDER  ----------//
  ord_itemName: 'Laptop',
  ord_itemPrice: '1000',
  ord_itemSize: '5x5x5',
  ord_secondaryID: '#deepak',

};


export default (state = STATE, action) => {
  switch (action.type) {

  //---------- USR  ----------//
    case UPDATE_USR_FULLNAME:
      return {
        ...state,
        usr_creds_fullName: action.payload
      };
    case UPDATE_USR_EMAIL:
      return {
        ...state,
        usr_creds_email: action.payload
      };
    case UPDATE_USR_USERNAME:
      return {
        ...state,
        usr_creds_username: action.payload
      };
    case UPDATE_USR_PASSWORD:
      return {
        ...state,
        usr_creds_password: action.payload
      };
    case UPDATE_USR_PHONENUMBER:
      return {
        ...state,
        usr_creds_phoneNumber: action.payload
      };
    case UPDATE_USR_LATITUDE:
      return {
        ...state,
        usr_creds_username: action.payload
      };
    case UPDATE_USR_LONGITUDE:
      return {
        ...state,
        usr_creds_password: action.payload
      };

  //----------  VLT   ----------//
    case UPDATE_VLT_TAG:
      return {
        ...state,
          vlt_meta_tag: action.payload
      };
    case UPDATE_VLT_UNIT:
      return {
        ...state,
          vlt_meta_unit: action.payload
      };
    case UPDATE_VLT_SIZE:
      return {
        ...state,
        vlt_meta_size: action.payload
      };
    case UPDATE_VLT_LOCATION:
      return {
        ...state,
        vlt_meta_location: action.payload
      };
    case UPDATE_VLT_ADDRESS:
      return {
        ...state,
        vlt_meta_address: action.payload
      };
    case UPDATE_VLT_ZIP:
      return {
        ...state,
        vlt_meta_zip: action.payload
      };
    case UPDATE_VLT_RATE:
      return {
        ...state,
        vlt_meta_rate: action.payload
      };
    case UPDATE_VLT_DIRECTIONS:
      return {
        ...state,
        vlt_meta_directions: action.payload
      };
    case UPDATE_VLT_INDEX:
      return {
        ...state,
        vlt_meta_index: action.payload
      };
    case UPDATE_VLT_RESTIME:
      return {
        ...state,
        vlt_meta_resTime: action.payload
      };
    case UPDATE_VLT_RESERVED:
      return {
        ...state,
        vlt_creds_reserved: action.payload
      };
    case UPDATE_VLT_MASTERKEY:
      return {
        ...state,
        vlt_creds_masterKey: action.payload
      };
    case UPDATE_VLT_KEY1:
      return {
        ...state,
        vlt_creds_key1: action.payload
      };
    case UPDATE_VLT_KEY2:
      return {
        ...state,
        vlt_creds_key2: action.payload
      };



    case UPDATE_VLT_DOOR:
      return {
        ...state,
        vlt_hw_door: action.payload
      };


  //---------- SYSTEM  ----------//
    case UPDATE_SYS_CACHE1:
      return {
        ...state,
        sys_cache1: action.payload
      };
    case UPDATE_SYS_CONNECTSTATUS:
      return {
        ...state,
        sys_connectStatus: action.payload
      };
    case UPDATE_SYS_PAGELOADED:
      return {
        ...state,
        sys_pageLoaded: action.payload
      };
    case UPDATE_SYS_PAGELOADED1:
      return {
        ...state,
        sys_pageLoaded1: action.payload
      };
    case UPDATE_SYS_WAITSOCKET:
      return {
        ...state,
        sys_waitSocket: action.payload
      };
    case UPDATE_SYS_SEARCHVALUE:
      return {
        ...state,
        sys_searchValue: action.payload
      };
    case UPDATE_SYS_STARCOUNT:
      return {
        ...state,
        sys_starCount: action.payload
      };
    case UPDATE_SYS_ACTIVITYINDICATOR:
      return {
        ...state,
        sys_activityIndicator: action.payload
      };
    case UPDATE_SYS_ACTIVETAB:
      return {
        ...state,
        sys_activeTab: action.payload
      };
    case UPDATE_SYS_EDITPAGE_TARGET:
      return {
        ...state,
        sys_editTarget: action.payload
      };
    case UPDATE_SYS_EDITPAGE_CONTENT:
      return {
        ...state,
        sys_editContent: action.payload
      };
    case UPDATE_SYS_PROFILEPIC:
      return {
        ...state,
        sys_profilePic: action.payload
      };
    case UPDATE_SYS_VITYPE:
      return {
        ...state,
        sys_viType: action.payload
      };
    case UPDATE_SYS_VITIMESLIDER:
      return {
        ...state,
        sys_viTimeSlider: action.payload
      };
    case UPDATE_SYS_VITOGGLE:
      return {
        ...state,
        sys_viToggle: action.payload
      };
    case UPDATE_SYS_VIEXPDATE:
      return {
        ...state,
        sys_viExpDate: action.payload
      };

  //----------  ORDER   ----------//
    case UPDATE_ORD_ITEMNAME:
      return {
        ...state,
        ord_itemName: action.payload
      };

    case UPDATE_ORD_ITEMPRICE:
      return {
        ...state,
        ord_itemPrice: action.payload
      };

    case UPDATE_ORD_ITEMSIZE:
      return {
        ...state,
        ord_itemSize: action.payload
      };

    case UPDATE_ORD_SECONDARYID:
      return {
        ...state,
        ord_secondaryID: action.payload
      };

    case SOCKET_IO:
        socket.emit('emit', action.payload, socket.id)

    default:
      return state;
  }
};
