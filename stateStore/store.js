import { createStore } from 'redux';
//import { reducer as formReducer } from 'redux-form';

import reducers from './reducers';


const store = createStore(reducers);

export default store;
